### Ivanhoe ###
## COMP 3004 Winter 2016 Project ##
## Contributors: Max Goad, Peter Zumbach, Sam Curran ##

Server Instructions
1. Run the "StartIvanhoeServer" jar file
2. Input start command with port, number of human players and number of AI players you wish to accept (Example: "start 5050 2 0")
3. Your server is now running and ready to accept client connections!
4. The server will close if a game is over AND each human player no longer wishes to play.

Client Instructions (Do once per client)
1. Run the "StartIvanhoeClient" jar file
2. Input start command with name, IP and port number of the server you wish to connect to (Example: "start max 127.168.0.1 5050")
3. Once the server reaches the desired number of human players, the GUIs will launch and the game will begin!
4. Once the game has completed, the GUIs will close down and the client will ask you if you wish to play again.
5. The client will close if a game is over AND you choose to not play again.