/*
 * Name: 		IvanhoeClient.java
 * Created by:	Max Goad
 * Description:	The main client class for the Ivanhoe game.
 */

package client;

import java.io.InputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.apache.log4j.Logger;

import UI.UIFacade;
import server.message.Message;
import server.message.Messenger;

public class IvanhoeClient extends Messenger implements Runnable {
	
	private ClientUserInterface userInterface = null;
	
	private Thread thread = null;
	private Socket socket = null;

	private String playerName;
	private String serverIP;
	private int serverPort;
	
	private int UID;
	
	private InputStream userInputStream = null;
	private Scanner inputScanner = null;
	
	private static Logger logger = Logger.getLogger(IvanhoeClient.class.getName());
	
	//Flags
	private boolean doLog;
	private boolean establishedConnection;
	private boolean playingGame;
	
	//////////////
	//Constructors
	//////////////
	public IvanhoeClient() {
		super();

		this.playerName = null;
		this.serverIP = null;
		this.serverPort = -1;
		
		this.UID = -1;
		
		this.userInputStream = ClientConfig.DEFAULT_INPUT_STREAM;
		
		this.doLog = true;
		this.establishedConnection = false;
		this.playingGame = false;
		
		thread = new Thread(this);
	}
	
	/////////////////
	//Main thread fns
	/////////////////
	public void start() {
		thread.start();
	}
	
	public void run() {

		this.inputScanner = new Scanner(userInputStream);
		
		if (!this.isInitialized())
			initializeViaInStream();
		openSocket();
		establishMessageConnection();
		
		//Session loop (handles multiple games on same server connection)
		while (this.isRunning()) {
			
			waitForGameStart();
			
			launchUserInterface();
			
			String winner = gameLoop();
			//Display game end results?
			endGame(winner);
		}
		
		this.stop();
	}
	
	public void stop() {
		//Stop thread running
		thread = null;

		//Shut down everything else
		if (inputScanner != null) {
			if (userInputStream != System.in) {
				inputScanner.close();
			}
			inputScanner = null;
		}
	}
	
	//////////////////
	//Client logic fns
	//////////////////
	private void initializeViaInStream() {
		if (!this.isRunning()) {
			return;
		}
		
		while (!this.isInitialized()) {
			
			if(userInputStream != System.in && !inputScanner.hasNextLine()) {
				logger.error("Error! Detected no new line from input stream!");
				this.stop();
				break;
			}
			
			//Get start command from input stream
			if (doLog) {
				logger.info("Please enter \"start\" followed by your name, the server's IP, and port you wish to connect to!");
				logger.info("Example: \"start max 127.168.0.1 5050\"");
			}
			String rawcmd = inputScanner.nextLine();
			
			//Check array integrity
			if (rawcmd == null) {
				if (doLog)
					logger.warn("Unknown Error! Command string parsed to be null!");
				continue;
			}
			
			//Split string into args
			String[] cmd = rawcmd.split(" ");
			
			//Check start command
			if (!cmd[0].equalsIgnoreCase("start")) {
				if (doLog)
					logger.warn("Error! Command must begin with the word \"start\"!");
				continue;
			}
			
			//Check length of command
			if ((cmd.length - 1) != ClientConfig.EXPECTED_STARTUP_ARGS) {
				if (doLog)
					logger.warn("Error! Incorrect number of arguments input to start command! Expected: " + ClientConfig.EXPECTED_STARTUP_ARGS);
				continue;
			}
			
			//Check validity of args
			String inputPlayerName = cmd[1];
			String inputServerAddr = cmd[2];
			int inputPort;
			
			try {
				inputPort = Integer.valueOf(cmd[3]);
			} catch (NumberFormatException e) {
				if (doLog) 
					logger.warn("Error! Cannot parse port number! Received: " + cmd[3]);
				continue;
			}
			
			//All args are valid! Assign values and end loop
			this.initialize(inputPlayerName, inputServerAddr, inputPort);
		}
	}

	private void openSocket() {
		if (!this.isRunning()) {
			return;
		}
		
		if (!this.hasSocketInitialized()) {
			if (this.serverIP == null || this.serverPort == 0) {
				logger.error("Aborted attempt to open new socket with insufficient information!");
				this.stop();
				return;
			}
			
			try {
				if (doLog) {
					logger.info("Attempting connection to server at IP " + this.serverIP + ":" + this.serverPort + "...");
				}
				
				this.socket = new Socket(this.serverIP, this.serverPort);
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				this.stop();
				return;
			}
		}
		
		this.initializeStreams(this.socket);
	}
	
	private void establishMessageConnection() {
		if (!this.isRunning() || this.hasEstablishedConnection()) {
			return;
		}
		
		//Wait for registration request from server
		Message request = waitForMessage(Message.Type.CLIENT_REGISTRATION_REQUEST);
		if (!isValidMessage(request)) {
			logger.error("establishMessageConnection() failed!"); 
			this.stop();
			return;
		}
		
		//Craft and send response
		Message response = new Message(Message.Type.CLIENT_REGISTRATION_RESPONSE, this.playerName);
		if (!sendMessage(response)) {
			this.stop();
			return;
		}
		
		//Wait for confirmation message
		//TODO: Can we do anything about a possible expected ERROR_RETRY_TYPE?
		Message confirmation = waitForMessage(Message.Type.CLIENT_REGISTRATION_CONFIRMATION);
		if (!isValidMessage(confirmation, true)) {
			logger.error("establishMessageConnection() failed!"); 
			this.stop();
			return;
		}
		
		if (doLog) {
			logger.info("Connection to server successful!");
		}
		
		this.setUID(Integer.parseInt(confirmation.contents));
		this.setEstablishConnectionFlag(true);
	}
	
	private void waitForGameStart() {
		if (!this.isRunning() || this.isPlayingGame()) {
			return;
		}
		
		if (doLog) {
			logger.info("Waiting for server to start the game...");
		}
		
		Message gameStartMessage = null;
		
		do {
			gameStartMessage = waitForMessage(Message.Type.GAME_START);
			if (gameStartMessage == null) {
				this.stop();
				return;
			}
		} while (!isValidMessage(gameStartMessage));	
		
		this.setPlayingGameFlag(true);
	}
	
	private void launchUserInterface() {
		if (!this.isRunning() || !this.isPlayingGame() ) {
			return;
		}
		
		if (this.userInterface == null) {
			this.userInterface = new UIFacade();
		}
		
		if (doLog) {
			logger.info("Launching userInterface!");
		}
		
		userInterface.start(this.UID);
	}
	
	private String gameLoop() {
		if (!this.isRunning()) {
			return null;
		}
		
		String winningPlayer = null;
		
		Set<Message.Type> expectedTypes = new HashSet<>();
		expectedTypes.add(Message.Type.GAME_UPDATE_STATE);
		expectedTypes.add(Message.Type.GAME_USER_ERROR);
		expectedTypes.add(Message.Type.GAME_STRING_REQUEST);
		expectedTypes.add(Message.Type.GAME_TURN_ACTION_REQUEST);
		expectedTypes.add(Message.Type.GAME_END);
		
		//Game loop
		while (this.isPlayingGame()) {
			
			Message receivedMsg  = waitForMessage(expectedTypes);
			if (!isValidMessage(receivedMsg)) {
				logger.error("gameLoop() received message failed validation!"); 
				this.stop();
				this.playingGame = false;
				return null;
			}
			
			switch (receivedMsg.type) {
				case GAME_UPDATE_STATE:
					userInterface.updateState(receivedMsg.contents);
					break;
				case GAME_USER_ERROR:
					userInterface.reportErrorToUser(receivedMsg.contents);
					break;
				case GAME_STRING_REQUEST:
					String stringResponse = userInterface.promptUserForString(receivedMsg.contents);
					Message stringResponseMsg = new Message(Message.Type.GAME_STRING_RESPONSE, stringResponse);
					if (!sendMessage(stringResponseMsg)) {
						logger.error("gameLoop() send message failed!"); 
						this.stop();
						this.playingGame = false;
						return null;
					}
					break;
				case GAME_TURN_ACTION_REQUEST:
					String turnResponse = userInterface.getTurnAction();
					Message turnResponseMsg = new Message(Message.Type.GAME_TURN_ACTION_RESPONSE, turnResponse);
					if (!sendMessage(turnResponseMsg)) {
						logger.error("gameLoop() send message failed!"); 
						this.stop();
						this.playingGame = false;
						return null;						
					}
					break;
				case GAME_END:
					setPlayingGameFlag(false);
					winningPlayer = receivedMsg.contents;
					break;
				default:
					logger.error("Unexpected message type received in game loop! " + receivedMsg.type.toString());
					break;
			}
		}
		
		return winningPlayer;
	}
	
	private void endGame(String winner) {
		if (!this.isRunning()) {
			return;
		}
		
		if (doLog) {
			logger.info("Game has ended!");
			if (winner.equals(this.playerName)) {
				logger.info("Congratulations! You won!");
			}
			else {
				logger.info("You have lost. The winner was: " + winner);
			}
		}
		
		if (this.userInterface != null) {
			userInterface.stop();
		}
		
		Message serverNotification = null;
		
		while (true) {
			
			if(userInputStream != System.in && !inputScanner.hasNextLine()) {
				logger.error("Error! Detected no new line from input stream!");
				this.stop();
				return;
			}
			
			if (doLog) {
				logger.info("Would you like to participate in another game? (Yes or No)");
			}
			String rawcmd = inputScanner.nextLine();
			
			//Check string integrity
			if (rawcmd == null) {
				if (doLog)
					logger.warn("Unknown Error! Command string parsed to be null!");
				continue;
			}
			
			switch(rawcmd.toLowerCase()) {
				case "yes":
				case "y":
					serverNotification = new Message(Message.Type.CLIENT_CONTINUE_PLAYING);
					sendMessage(serverNotification);
					//TODO: Log something to user?
					return;
				case "no":
				case "n":
					serverNotification = new Message(Message.Type.CLIENT_FINISHED_PLAYING);
					sendMessage(serverNotification);
					//TODO: Log something to user?
					this.stop();
					return;
				default:
					if (doLog) {
						logger.warn("Incorrect/Unknown input!");
					}
					continue;
			}
		}
	}
	
	///////////////////////////////////////////
	//Getters, Setters, Other state-related fns
	///////////////////////////////////////////
	
	/*
	 * Getters
	 */
	public String getPlayerName() {
		return this.playerName;
	}
	
	public InputStream getUserInputStream() {
		return this.userInputStream;
	}
	
	public boolean isRunning() {
		if (thread == null)
			return false;
		
		return thread.isAlive();
	}
	
	public boolean isPlayingGame() {
		return this.playingGame;
	}
	
	public boolean isLogging() {
		return this.doLog;
	}
	
	public boolean isInitialized() {
		return (this.playerName != null)
			&& (this.serverIP != null)
			&& (this.serverPort != -1);
	}
	
	public boolean hasSocketInitialized() {
		return (this.socket != null);
	}
	
	public boolean hasEstablishedConnection() {
		return this.establishedConnection;
	}	
	
	/*
	 * Setters
	 */
	public void initialize(String playerName, String serverIP, int serverPort) {
		this.playerName = playerName;
		this.serverIP = serverIP;
		this.serverPort = serverPort;
		
		if (doLog) {
			//logger.info("Player name set to " + this.getPlayerName() + "!");
			//logger.info("Server IP set to " + this.getServerIP() + "!");
			//logger.info("Server Port set to " + this.getServerPort() + "!");
		}
	}

	public void setUID(int UID) {
		this.UID = UID;
	}

	public void setUserInterface(ClientUserInterface userInterface) {
		this.userInterface = userInterface;
	}

	public void setSocket(Socket s) {
		this.socket = s;
	}

	public void setUserInputStream(InputStream is) {
		this.userInputStream = is;
	}
	
	public void setPlayingGameFlag(boolean flag) {
		this.playingGame = flag;
	}
	
	public void setEstablishConnectionFlag(boolean flag) {
		this.establishedConnection = flag;
	}
	
	public void setDoLogFlag(boolean flag) {
		this.doLog = flag;
	}
}
