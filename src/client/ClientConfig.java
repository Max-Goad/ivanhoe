package client;

import java.io.InputStream;

public class ClientConfig {
	public static final InputStream DEFAULT_INPUT_STREAM = System.in;
	
	public static final int EXPECTED_STARTUP_ARGS = 3;
	
	public static final String TEST_SIMPLE_STARTUP = "start test 127.168.0.1 5050";
}
