package client;

//import java.util.Random;

import org.apache.log4j.Logger;

import util.TestUtilFns;

public class StartIvanhoeClient {
	
	private static Logger logger = Logger.getLogger(StartIvanhoeClient.class.getName());
	private static IvanhoeClient client = null;
	
	public static void main(String[] args) {
		//Initialize the client
		logger.info("Starting Ivanhoe Client...");
		client = new IvanhoeClient();
		//Random rng = new Random();
		//client.initialize("TestPlayer" + rng.nextInt(100), "127.168.0.1", 12345);
		client.start();
		
		//Wait a second for the client to start
		TestUtilFns.wait(1000);
		
		//Keep process running while client is running
		while (client.isRunning()) { TestUtilFns.wait(1000); }

		//Once client is no longer running, kill process
		logger.info("Exiting Ivanhoe Client...");
		System.exit(0);
	}
}

