/*
 * Name: 		ClientUserInterface.java
 * Created by:	Max Goad
 * Description:	Interface for all UIs meant to be used by IvanhoeClient to implement.
 */

package client;

public interface ClientUserInterface {
	
	//Control fns
	public void start(int UID);
	public void stop();
	
	//Push fns (Will not wait for user input)
	public void updateState(String state);
	public void reportErrorToUser(String errorMsg);
	
	//Pull fns (Blocking; Waits until user input received)
	public String promptUserForString(String prompt);
	public String getTurnAction();

}
