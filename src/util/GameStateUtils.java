package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import cards.Card;
import engine.GameState;
import engine.Player;

public class GameStateUtils {

	/*
	 * colourSubset
	 * Returns:
	 * 	-A subset of only cards that match the desired colour
	 */
	public static ArrayList<Card> colourSubset(ArrayList<Card> cardList, String desiredColour) {
		ArrayList<Card> subset = new ArrayList<>();
		
		for (Card c : cardList)
			if (c.getColour().equals(desiredColour))
				subset.add(c);
		
		return subset;
	}
	
	/*
	 * getHighestValueCard
	 * Returns:
	 * 	-The highest value card from a list passed in.
	 * 	-NULL if no cards that have value are found.
	 */
	public static Card getHighestValueCard(ArrayList<Card> cardList) {
		Card largestCard = null;
		int largestCardValue = 0;
		for (Card c : cardList) {
			if (c.getValue() > largestCardValue) {
				largestCard = c;
				largestCardValue = c.getValue();
			}
		}
		
		return largestCard;
	}
	
	/*
	 * getHighestValueColour
	 * Returns:
	 * 	-The tournament colour that represents the most value
	 * 	 from the list passed in.
	 * 	-IGNORES WHITE (supporter) CARDS
	 * 	-NULL if no cards of any tournament colour are found.
	 */
	public static String getHighestValueColour(ArrayList<Card> cardList) {
		HashMap<String, Integer> colourValueMap = new HashMap<>();
		
		for (Card c : cardList) {
			Integer currentColourCount = colourValueMap.get(c.getColour());
			if (currentColourCount == null) {
				colourValueMap.put(c.getColour(), c.getValue());
			}
			else {
				int newColourCount = currentColourCount.intValue() + c.getValue();
				colourValueMap.put(c.getColour(), newColourCount);
			}
		}
		
		String highestValueColour = null;
		int highestValue = 0;
		for (Entry<String, Integer> e : colourValueMap.entrySet()) {
			if (e.getKey().equals("white")) continue; //Ignore the supporter cards!
			if (e.getValue() > highestValue) {
				highestValueColour = e.getKey();
				highestValue = e.getValue();
			}
		}
		
		return highestValueColour;
	}
	
	/*
	 * getTotalCardValue
	 * Returns
	 * 	-Total card value
	 */
	public static int getTotalCardValue(ArrayList<Card> cardList) {
		int totalValue = 0;
		for (Card c : cardList) {
			totalValue += c.getValue();
		}
		
		return totalValue;
	}
	
	/*
	 * getLeadTournamentScore
	 * Returns:
	 * 	-The score of the tournament leader
	 * 	-null if no tournament started
	 */
	public static int getLeadTournamentScore() {
		GameState currentState = GameState.getInstance();
		ArrayList<Player> players = currentState.getPlayers();
		
		int largestDisplayScore = 0;
		for (Player p : players) {
			int totalDisplayValue = getTotalCardValue(p.getDisplay());
			if (totalDisplayValue > largestDisplayScore) {
				largestDisplayScore = totalDisplayValue;
			}
		}
		
		return largestDisplayScore;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
