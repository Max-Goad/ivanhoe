package util;

import static org.junit.Assert.fail;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.mockito.Mockito;

import client.ClientUserInterface;

public class TestUtilFns {
	
	public static Socket createEmptyMockSocket() {
		Socket mockSocket = Mockito.mock(Socket.class);
		OutputStream mockOut = Mockito.mock(OutputStream.class);
		InputStream mockIn = Mockito.mock(InputStream.class);
		try {
			Mockito.when(mockSocket.getOutputStream()).thenReturn(mockOut);
			Mockito.when(mockSocket.getInputStream()).thenReturn(mockIn);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		return mockSocket;
	}
	
	public static ClientUserInterface createEmptyMockUI() {
		ClientUserInterface mockUI = Mockito.mock(ClientUserInterface.class);
		
		Mockito.doNothing().when(mockUI).start(Mockito.anyInt());
		Mockito.doNothing().when(mockUI).stop();
		Mockito.doNothing().when(mockUI).updateState(Mockito.anyString());
		Mockito.doNothing().when(mockUI).reportErrorToUser(Mockito.anyString());
		Mockito.when(mockUI.promptUserForString(Mockito.anyString())).thenReturn(null);
		Mockito.when(mockUI.getTurnAction()).thenReturn(null);
		
		return mockUI;
	}
	
	public static void wait(int x) {
		if (x >= 0) {
			try {
				Thread.sleep(x);
			} catch (InterruptedException e) {	}
		}
	}
}
