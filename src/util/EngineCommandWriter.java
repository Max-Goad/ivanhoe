package util;

public class EngineCommandWriter {
	
	/*
	 * Server -> Client Commands
	 */
	//TODO: Write these
	
	/*
	 * Client -> Server Commands
	 */
	public static String endTurnCommand() {
		return "Withdraw";
	}
	
	public static String playCardCommand(int cardID) {
		return "play " + cardID;
	}
	
	/*
	 * Advanced Client -> Server Commands
	 * These are used by the AI to send the entire command through without
	 * needing additional prompting from the engine!
	 */
	public static String playSupporterStartTournamentCommand(int cardID, String tournamentColour){
		return "play " + cardID + " " + tournamentColour;
	}
}
