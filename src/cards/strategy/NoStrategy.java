package cards.strategy;

import engine.GameState;

public class NoStrategy implements CardStrategy {
	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		return true;
	}
}
