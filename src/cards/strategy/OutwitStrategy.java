package cards.strategy;

import java.util.ArrayList;

import cards.Card;
import engine.GameState;
import engine.Player;

public class OutwitStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional Information required
		 * (int) ID of the player who played the card
		 * (int) ID of the player's card to switch
		 * (int) ID of the target player
		 * (int) ID of the target's card
		 */
		
		String[] args = additionalInformation.split(" ");
		if (args == null || args.length != 4) {
			return false;
		}
		int playerID;
		int playerCardID;
		int targetPlayerID;
		int targetCardID;
		try {
			playerID = Integer.parseInt(args[0]);
			playerCardID = Integer.parseInt(args[1]);
			targetPlayerID = Integer.parseInt(args[2]);
			targetCardID = Integer.parseInt(args[3]);
		} catch (NumberFormatException e){
			return false;
		}
		
		Player player = gameState.getPlayerByID(playerID);		
		if (player == null || (player.getDisplay().isEmpty() && player.getSpecialZone().isEmpty()) ) {
			return false;
		}
		Player targetPlayer = gameState.getPlayerByID(targetPlayerID);		
		if (targetPlayer == null || (targetPlayer.getDisplay().isEmpty() && targetPlayer.getSpecialZone().isEmpty()) ) {
			return false;
		}
		
		
		ArrayList<Card> playerDisplay = player.getDisplay();
		if (playerCardID < 0 || playerCardID >= playerDisplay.size()+player.getSpecialZone().size()){
			return false;
		}
		ArrayList<Card> targetDisplay = targetPlayer.getDisplay();
		if (targetCardID < 0 || targetCardID >= targetDisplay.size()+targetPlayer.getSpecialZone().size()){
			return false;
		}
		
		if (playerDisplay.size() <= 1 && targetCardID >= targetDisplay.size()){
			return false;
		}
		if (targetDisplay.size() <= 1 && playerCardID >= playerDisplay.size()){
			return false;
		}
		
		if (playerCardID >= playerDisplay.size()){ 
			playerCardID -= playerDisplay.size();
			playerDisplay = player.getSpecialZone();
		}
		if (targetCardID >= targetDisplay.size()){ 
			targetCardID -= targetDisplay.size();
			targetDisplay = targetPlayer.getSpecialZone();
		}
		
		Card playerCard = playerDisplay.remove(playerCardID);
		if (playerCard == null) {
			return false;
		}
		
		Card targetCard = targetDisplay.remove(targetCardID);
		if (targetCard == null) {
			return false;
		}
		
		if (playerCard.getName().equals("shield") || playerCard.getName().equals("stunned")){
			targetPlayer.getSpecialZone().add(playerCard);
		} else {
			targetDisplay.add(playerCard);
		}
		
		if (targetCard.getName().equals("shield") || targetCard.getName().equals("stunned")){
			player.getSpecialZone().add(targetCard);
		} else {
			playerDisplay.add(targetCard);
		}
		
		return true;
		/*else {
			gameState.addCardToDiscardPile(targetsCard);
			return true;
		}*/
	}

}
