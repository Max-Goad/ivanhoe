package cards.strategy;

import java.util.ArrayList;

import cards.Card;
import engine.GameState;
import engine.Player;

public class DodgeStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional Information required
		 * (int) ID of the target player
		 * (int) ID of the card to discard
		 */
		
		String[] args = additionalInformation.split(" ");
		if (args == null || args.length != 2) {
			return false;
		}
		int targetPlayerID;
		int targetCardID;
		try {
			targetPlayerID = Integer.parseInt(args[0]);
			targetCardID = Integer.parseInt(args[1]);
		} catch (NumberFormatException e){
			return false;
		}
		
		Player targetPlayer = gameState.getPlayerByID(targetPlayerID);		
		if (targetPlayer == null || targetPlayer.getDisplay().isEmpty()) {
			return false;
		}
		
		if (targetPlayer.hasShield()) return false;
		
		ArrayList<Card> affectedDisplay = targetPlayer.getDisplay();
		if (affectedDisplay.size() <= 1) return false;
		if (targetCardID < 0 || targetCardID >= affectedDisplay.size()){
			return false;
		}
		
		Card affectedCard = affectedDisplay.remove(targetCardID);
		
		if (affectedCard == null) {
			return false;
		}
		else {
			gameState.addCardToDiscardPile(affectedCard);
			return true;
		}
	}

}
