package cards.strategy;

import engine.*;

public class ChangeWeaponStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		if (!gameState.getTournamentColour().equals("red")
		 && !gameState.getTournamentColour().equals("blue")
		 && !gameState.getTournamentColour().equals("yellow")) {
			return false;
		}
		
		if (additionalInformation == null){
			return false;
		}
		
		switch (additionalInformation) {
			case "red":
			case "blue":
			case "yellow":
				if (gameState.getTournamentColour().equals(additionalInformation))
					return false;
				
				gameState.setTournamentColour(additionalInformation);
				return true;
			default:
				return false;
		}
	}

}
