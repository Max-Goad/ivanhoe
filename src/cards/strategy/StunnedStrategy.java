package cards.strategy;

import engine.GameState;
import engine.Player;

public class StunnedStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional Information required
		 * (int) ID of the player who played the card
		 * (int) ID of the target player
		 */
		
		int targetPlayerID;
		try {
			targetPlayerID = Integer.parseInt(additionalInformation);
		} catch (NumberFormatException e){
			return false;
		}
		
		Player targetPlayer = gameState.getPlayerByID(targetPlayerID);		
		if (targetPlayer == null) {
			return false;
		}
		
		if (targetPlayer.isWithdrawn()) return false;
		
		return true;
		
		//return false;
	}

}
