package cards.strategy;

import cards.*;
import engine.*;

public class RiposteStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional Information required
		 * (int) ID of the player who played the card
		 * (int) ID of the target player
		 */
		
		String[] tokens = additionalInformation.split(" ");
		Player player;
		Player target;
		try {
			player = gameState.getPlayerByID(Integer.parseInt(tokens[0]));
			target = gameState.getPlayerByID(Integer.parseInt(tokens[1]));
		} catch (NumberFormatException e){
			return false;
		}
		
		if (target.getDisplay().size() <= 1) return false;
		if (target.hasShield()) return false;
		
		Card affectedCard = target.getDisplay().get(target.getDisplay().size()-1);
		player.getDisplay().add(affectedCard);
		target.getDisplay().remove(affectedCard);
		
		return true;
	}

}
