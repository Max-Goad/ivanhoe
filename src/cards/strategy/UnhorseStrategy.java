package cards.strategy;

import engine.*;

public class UnhorseStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		if (!gameState.getTournamentColour().equals("purple")) {
			return false;
		}
		
		switch (additionalInformation) {
			case "red":
			case "blue":
			case "yellow":
				gameState.setTournamentColour(additionalInformation);
				return true;
			default:
				return false;
		}
	}

}
