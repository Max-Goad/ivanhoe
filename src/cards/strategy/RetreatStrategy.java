package cards.strategy;

import java.util.ArrayList;

import cards.Card;
import engine.GameState;
import engine.Player;

public class RetreatStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional Information required
		 * (int) ID of the player who played the card
		 * (int) ID of the card to return from display
		 */
		
		String[] tokens = additionalInformation.split(" ");
		if (tokens.length != 2) {
			return false;
		}
		int playerID;
		int cardID;
		try {
			playerID = Integer.parseInt(tokens[0]);
			cardID = Integer.parseInt(tokens[1]);
		} catch (NumberFormatException e){
			return false;
		}
		
		
		Player player = gameState.getPlayerByID(playerID);
		if (player == null) {
			return false;
		}
		
		ArrayList<Card> targetDisplay = player.getDisplay();
		if (targetDisplay == null) {
			return false;
		}
		if (cardID < 0 || cardID >= targetDisplay.size()){
			return false;
		}
		
		Card targetCard = targetDisplay.get(cardID);
		
		if (targetCard == null) {
			return false;
		} else {
			targetDisplay.remove(targetCard);
			player.getHand().add(targetCard);
			return true;
		}
		
	}

}
