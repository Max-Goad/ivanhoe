package cards.strategy;

import cards.*;
import engine.*;

public class DisgraceStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional Information required
		 * (int) ID of the player who played it
		 */
		
		Player player;
		try {
			player = gameState.getPlayerByID(Integer.parseInt(additionalInformation));
		} catch (NumberFormatException e){
			return false;
		}
		
		boolean cardsDiscarded = false;
		
		for (Player p : gameState.getPlayers()){
			if (p.hasShield() && p != player) continue;
			for (int i = p.getDisplay().size() - 1; i >= 0; --i) {
				if (p.getDisplay().size() <= 1) break;
				Card c = p.getDisplay().get(i);
				if (c.getType().equals("simple") && c.getColour().equals("white")){
					p.getDisplay().remove(c);
					gameState.addCardToDiscardPile(c);
					cardsDiscarded = true;
				}
			}
		}		
		
		return cardsDiscarded;
	}

}
