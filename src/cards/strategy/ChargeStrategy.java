package cards.strategy;

import java.util.ArrayList;

import cards.Card;
import engine.GameState;
import engine.Player;

public class ChargeStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional Information required
		 * (int) ID of the player who played it
		 */
		
		Player player;
		try {
			player = gameState.getPlayerByID(Integer.parseInt(additionalInformation));
		} catch (NumberFormatException e){
			return false;
		}
		
		int lowestValue = Integer.MAX_VALUE;
		boolean cardsExistInDisplay = false;
		
		//Get lowest value
		for (Player p : gameState.getPlayers()) {
			if (!cardsExistInDisplay && p.getDisplay().size() != 0) {
				cardsExistInDisplay = true;
			}
			
			for (Card c : p.getDisplay()) {
				if (c.getValue() < lowestValue) {
					lowestValue = c.getValue();
				}
			}
		}
		
		boolean cardsDiscarded = false;
		
		//Remove any matches from players' displays
		if (cardsExistInDisplay) {
			for (Player p : gameState.getPlayers()) {
				if (p.hasShield() && p != player) continue;
				ArrayList<Card> display = p.getDisplay();
				if (display.size() <= 1) break;
				for (int i = display.size() - 1; i >= 0; --i) {
					if (display.get(i).getValue() == lowestValue) {
						gameState.addCardToDiscardPile(display.get(i));
						display.remove(i);
						cardsDiscarded = true;
					}
				}
			}
		}
		
		return cardsDiscarded;
	}

}
