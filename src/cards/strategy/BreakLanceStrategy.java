package cards.strategy;

import java.util.ArrayList;

import cards.Card;
import engine.GameState;
import engine.Player;

public class BreakLanceStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional Information required
		 * (int) ID of the target player
		 */
		
		boolean cardsDiscarded = false;
		int targetID = -1;
		
		try {
			targetID = Integer.parseInt(additionalInformation);
		} catch (NumberFormatException e) {
			return false;
		}
		
		for (Player p : gameState.getPlayers()) {
			if (p.getID() == targetID) {
				
				if (p.hasShield()) {
					return false;
				}
				
				ArrayList<Card> display = p.getDisplay();
				if (display.size() <= 1) {
					return false;
				}
				
				for (int i = display.size() - 1; i >= 0; --i) {
					if (display.size() <= 1) break;
					if (display.get(i).getColour().equals("purple")) {
						gameState.addCardToDiscardPile(display.get(i));
						display.remove(i);
						cardsDiscarded = true;
					}
				}
			}
		}
		
		return cardsDiscarded;
	}

}
