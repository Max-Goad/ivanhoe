package cards.strategy;

import engine.*;

public class DropWeaponStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		switch (gameState.getTournamentColour()) {
		case "red":
			gameState.setTournamentColour("green");
			return true;
		case "blue":
			gameState.setTournamentColour("green");
			return true;
		case "yellow":
			gameState.setTournamentColour("green");
			return true;
		default:
			return false;
		}
	}

}
