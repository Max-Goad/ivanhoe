package cards.strategy;

import java.util.ArrayList;

import engine.*;
import cards.*;

public class AdaptStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional info:
		 * a list of lists - each list indicating the ID's of the cards that should REMAIN in one player's display;
		 * 		one list for each player
		 */
		
		String[] numberLists = additionalInformation.split(" ");
		ArrayList<ArrayList<Integer>> IDLists = new ArrayList<ArrayList<Integer>>();
		for (int i=0; i<numberLists.length; i++){
			IDLists.add(new ArrayList<Integer>());
			String[] tempList = numberLists[i].split(",");
			for (int k=0; k<tempList.length; k++){
				try {
					IDLists.get(i).add(Integer.parseInt(tempList[k]));
				} catch (NumberFormatException e){
					return false;
				}
			}
			
		}
		
		boolean cardsDiscarded = false;
		
		for (int p=0; p<gameState.getPlayers().size(); p++){
			Player currentPlayer = gameState.getPlayers().get(p);
			ArrayList<Integer> currentIDList = IDLists.get(p);
			
			ArrayList<Card> newDisplay = new ArrayList<Card>();
			
			for (Card c : currentPlayer.getDisplay()){
				// Pull out all cards that should remain in the display
				if (currentIDList.contains(c.getID())){
					newDisplay.add(c);
					currentPlayer.getDisplay().remove(c);
				}
			}
			
			for (Card c : currentPlayer.getDisplay()){
				// Discard all cards left in the original display
				gameState.addCardToDiscardPile(c);
				currentPlayer.getDisplay().remove(c);
				cardsDiscarded = true;
			}
			
			// Update the player's display
			currentPlayer.setDisplay(newDisplay);
			
		}
		
		
		
		return cardsDiscarded;
	}

}
