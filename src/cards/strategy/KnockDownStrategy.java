package cards.strategy;

import java.util.Random;

import cards.Card;
import engine.GameState;
import engine.Player;

public class KnockDownStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional Information required
		 * (int) ID of the player who played the card
		 * (int) ID of the target player
		 */
		String[] tokens = additionalInformation.split(" ");
		if (tokens.length != 2) {
			return false;
		}
		
		Player player = gameState.getPlayerByID(Integer.parseInt(tokens[0]));
		Player target = gameState.getPlayerByID(Integer.parseInt(tokens[1]));
		if (player == null || target == null) {
			return false;
		}
		
		if (target.getHand().size() == 0) {
			return false;
		}
		
		if (target.hasShield()) return false;
		
		Random rng = new Random();
		int randomCardIndex = rng.nextInt(target.getHand().size());
		Card targetCard = target.getHand().remove(randomCardIndex);
		if (targetCard == null) {
			return false;
		}
		else {
			player.getHand().add(targetCard);
			return true;
		}
	}

}
