package cards.strategy;

import engine.*;

public class OutmaneuverStrategy implements CardStrategy {

	@Override
	public boolean performCardStrategy(GameState gameState, String additionalInformation) {
		/* Additional Information required:
		 * (int) ID of the player who played the card
		 */
		
		boolean cardsDiscarded = false;
		int playerID = Integer.parseInt(additionalInformation);
		
		for (Player p : gameState.getPlayers()){
			if (p.getID() == playerID) continue;
			if (p.hasShield()) continue;
			if (p.getDisplay().size() > 1){
				gameState.addCardToDiscardPile(p.getDisplay().get(p.getDisplay().size()-1));
				p.getDisplay().remove(p.getDisplay().size()-1);
				cardsDiscarded = true;
			}
		}
		
		return cardsDiscarded;
	}

}
