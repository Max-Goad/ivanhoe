package cards.strategy;

import engine.*;

public interface CardStrategy {
	public boolean performCardStrategy(GameState gameState, String additionalInformation);
}

/* ===========================
 * ADDING A NEW CARD STRATEGY
 * ---------------------------
 * By: Max Goad
 * ===========================
 * Step 1: 	Create new class in the cards.strategy package
 * Step 2: 	Make this new class implement CardStrategy
 * Step 3: 	Fill out the above function with the proper action card logic
 * Step 4: 	Go to cards > Card.java > assignActionBehaviour(), and add your new class 
 * 			to the appropriate spot on the list.
 * Step 5: 	Go to test.rules > CardStrategyTests, and fill out the appropriate test 
 *          for your new action. See other tests for help. Make sure to hit all potential cases!
 * Step 6:	You're done!
 */

