package cards;

import cards.strategy.*;
import engine.GameState;

public class Card {
	private String name;
	private String colour;
	private String type;
	private String imagePath; // path of the image representing the card
	private int value;
	private int id;
	
	private CardStrategy cardStrategy;
	
	public static String[] card_colours = {"purple", "red", "blue", "yellow", "green"};
	public static String[] card_types = {"action", "simple"};
	/*
	 * include more stuff here later
	 */

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	public Card(String c, int v, int id, String imgPath){
		// Constructor for simple cards
		this.name = "simple";
		this.colour = c;
		this.value = v;
		this.id = id;
		this.imagePath = imgPath;
		
		this.type = "simple";
		
		assignCardStrategy(name);
	}
	
	public Card(String name, int id, String imgPath){
		// Constructor for action cards
		this.name = name;
		this.colour = "none";
		this.id = id;
		this.value = 0;
		this.imagePath = imgPath;

		this.type = "action";
		
		assignCardStrategy(name);
	}
	
	public String code(){
		String code = "";
		
		switch(colour){
		case "red": code += "R"; break;
		case "green": code += "G"; break;
		case "blue": code += "B"; break;
		case "yellow": code += "Y"; break;
		case "purple": code += "P"; break;
		case "white": code += "W"; break;
		}
		code += value+"("+id+")";
		
		return code;
	}
	
	public int getID() { return id; }
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	//Call this to run action stored in cardAction
	public boolean performCardStrategy(GameState state, String additionalInfo) {
		return cardStrategy.performCardStrategy(state, additionalInfo);
	}
	
	//Action card strategy gets assigned here
	private void assignCardStrategy(String name) {
		switch(name) {
		case "dodge":
			this.cardStrategy = new DodgeStrategy();
			break;
		case "disgrace":
			this.cardStrategy = new DisgraceStrategy();
			break;
		case "changeWeapon":
			this.cardStrategy = new ChangeWeaponStrategy();
			break;
		case "unhorse":
			this.cardStrategy = new UnhorseStrategy();
			break;
		case "knockDown":
			this.cardStrategy = new KnockDownStrategy();
			break;
		case "retreat":
			this.cardStrategy = new RetreatStrategy();
			break;
		case "riposte":
			this.cardStrategy = new RiposteStrategy();
			break;
		case "outmaneuver":
			this.cardStrategy = new OutmaneuverStrategy();
			break;
		case "counterCharge":
			this.cardStrategy = new CounterChargeStrategy();
			break;
		case "charge":
			this.cardStrategy = new ChargeStrategy();
			break;
		case "breakLance":
			this.cardStrategy = new BreakLanceStrategy();
			break;
		case "dropWeapon":
			this.cardStrategy = new DropWeaponStrategy();
			break;
			
		//Advanced Action cards
			
		case "outwit":
			this.cardStrategy = new OutwitStrategy();
			break;
		case "adapt":
			//TODO: Replace NoStrategy with new subclass
			this.cardStrategy = new NoStrategy();
			break;
		case "shield":
			this.cardStrategy = new ShieldStrategy();
			break;
		case "stunned":
			this.cardStrategy = new StunnedStrategy();
			break;
		case "ivanhoe":
			this.cardStrategy = new IvanhoeStrategy();
			break;
		case "simple":
			//TODO: Implement SimpleStrategy
			this.cardStrategy = new NoStrategy();
			break;
		default:
			this.cardStrategy = new NoStrategy();
			break;
		}
	}
	
}
