package cards;

public class CardFactory {

	private int id;
	
	public CardFactory(){
		id = 0;
	}
	
	public Card makeCard(String code){
		String[] words = code.split(" ");
		String name = words[0];
		int value = 0;
		
		if (words.length != 1) {
			try {
				value = Integer.parseInt(words[1]);
			} catch (NumberFormatException e){
				return null;
			} catch (ArrayIndexOutOfBoundsException e){
				return null;
			}
		}
		
		switch (name){
		case "white":
			if (value == 2) return new Card(name, value, id++, "/simpleCards.jpg");
			if (value == 3) return new Card(name, value, id++, "/simpleCards1.jpg");;
			if (value == 6) return new Card(name, value, id++, "/simpleCards2.jpg");;
			return null;
		case "purple":
			if (value == 3) return new Card(name, value, id++, "/simpleCards14.jpg");
			if (value == 4) return new Card(name, value, id++, "/simpleCards15.jpg");
			if (value == 5) return new Card(name, value, id++, "/simpleCards16.jpg");
			if (value == 7) return new Card(name, value, id++, "/simpleCards17.jpg");
			return null;
		case "red":
			if (value == 3) return new Card(name, value, id++, "/simpleCards7.jpg");
			if (value == 4) return new Card(name, value, id++, "/simpleCards8.jpg");
			if (value == 5) return new Card(name, value, id++, "/simpleCards9.jpg");
			return null;
		case "blue":
			if (value == 2) return new Card(name, value, id++, "/simpleCards10.jpg");
			if (value == 3) return new Card(name, value, id++, "/simpleCards11.jpg");
			if (value == 4) return new Card(name, value, id++, "/simpleCards12.jpg");
			if (value == 5) return new Card(name, value, id++, "/simpleCards13.jpg");
			return null;
		case "yellow":
			if (value == 2) return new Card(name, value, id++, "/simpleCards4.jpg");
			if (value == 3) return new Card(name, value, id++, "/simpleCards5.jpg");
			if (value == 4) return new Card(name, value, id++, "/simpleCards6.jpg");
			return null;
		case "green":
			if (value == 1) return new Card(name, value, id++, "/simpleCards3.jpg");
			return null;
		case "dodge":
			return new Card(name, id++, "/actionCards.jpg");
		case "disgrace":
			return new Card(name, id++, "/actionCards1.jpg");
		case "changeWeapon":
			return new Card(name, id++, "/actionCards10.jpg");
		case "unhorse":
			return new Card(name, id++, "/actionCards11.jpg");
		case "knockDown":
			return new Card(name, id++, "/actionCards12.jpg");
		case "retreat":
			return new Card(name, id++, "/actionCards2.jpg");
		case "riposte":
			return new Card(name, id++, "/actionCards3.jpg");
		case "outmaneuver":
			return new Card(name, id++, "/actionCards4.jpg");
		case "counterCharge":
			return new Card(name, id++, "/actionCards5.jpg");
		case "charge":
			return new Card(name, id++, "/actionCards6.jpg");
		case "breakLance":
			return new Card(name, id++, "/actionCards7.jpg");
		case "dropWeapon":
			return new Card(name, id++, "/actionCards9.jpg");
		case "outwit":
			return new Card(name, id++, "/actionCards13.jpg");
		case "adapt":
			return new Card(name, id++, "/actionCards8.jpg");
		case "shield":
			return new Card(name, id++, "/actionCards14.jpg");
		case "stunned":
			return new Card(name, id++, "/actionCards15.jpg");
		case "ivanhoe":
			return new Card(name, id++, "/actionCards16.jpg");
		default:
			return null;
		}
		
	}
	
}
