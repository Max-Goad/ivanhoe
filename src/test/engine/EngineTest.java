package test.engine;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestName;
import org.mockito.Mockito;

import cards.Card;
import cards.CardFactory;
import engine.Engine;
import engine.Player;
import server.ServerInterface;

public class EngineTest {
	
	@Rule public TestName name = new TestName();
	private static Logger logger = Logger.getLogger(CardStrategyTests.class.getName());
	
	static Engine e;
	
	@BeforeClass
	public static void setUpClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
		logger.info("Starting " + name.getMethodName() + "...");
	}

	@After
	public void tearDown() throws Exception {
		e = null;
	}
	
	@Test
	public void testPlayCard() {
		e = new Engine(null);
		Player testPlayer = new Player("Bob", 0);
		e.addPlayer(testPlayer);
				
		e.drawCard(testPlayer);
		
		testPlayer.playCard(testPlayer.getHand().get(0).getID());
		
		assertTrue(testPlayer.getDisplay().size() == 1 && testPlayer.getHand().size() == 0);
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}

	@Test
	public void testIsPlayerWinning(){
		e = new Engine(null);
		Player p1 = new Player("Bob", 0);
		Player p2 = new Player("Joe", 0);
		e.addPlayer(p1);
		e.addPlayer(p2);
		
		p1.getDisplay().add(new Card("red", 3, 0, ""));
		p1.getDisplay().add(new Card("red", 2, 0, ""));
		
		p2.getDisplay().add(new Card("red", 6, 0, ""));
		
		e.setTournamentColour("none");
		assertTrue(e.isPlayerWinning(p2));
		e.setTournamentColour("green");
		assertTrue(e.isPlayerWinning(p1));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void testMakeCardList(){
		e = new Engine(null);
		
		ArrayList<Card> list = new ArrayList<Card>();
		
		for (int i=0; i<5; i++){
			list.add(new Card("", 1, i, ""));
		}
		
		assertTrue(e.makeCardList(list).equals("0,1,2,3,4"));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void testStartingTournament(){
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		String testColour = "red";
		Mockito.when(mockServer.promptUserForString(Mockito.anyInt(), Mockito.anyString())).thenReturn(testColour);
		
		e = new Engine(mockServer);
		Player testPlayer = new Player("test", 1);
		testPlayer.getHand().add(new Card("purple",  0, 0, ""));
		testPlayer.getHand().add(new Card("red", 	 0, 1, ""));
		testPlayer.getHand().add(new Card("blue", 	 0, 2, ""));
		testPlayer.getHand().add(new Card("yellow",  0, 3, ""));
		testPlayer.getHand().add(new Card("green", 	 0, 4, ""));
		testPlayer.getHand().add(new Card("white", 	 0, 5, ""));
		testPlayer.getHand().add(new Card("someAction", 6, ""));
		
		e.setTournamentColour("none");
		assertTrue(e.handleTurnAction("play 0", testPlayer, false));
		assertEquals(e.getTournamentColour(), "purple");
		
		e.setTournamentColour("none");
		assertTrue(e.handleTurnAction("play 1", testPlayer, false));
		assertEquals(e.getTournamentColour(), "red");
		
		e.setTournamentColour("none");
		assertTrue(e.handleTurnAction("play 2", testPlayer, false));
		assertEquals(e.getTournamentColour(), "blue");
		
		e.setTournamentColour("none");
		assertTrue(e.handleTurnAction("play 3", testPlayer, false));
		assertEquals(e.getTournamentColour(), "yellow");
		
		e.setTournamentColour("none");
		assertTrue(e.handleTurnAction("play 4", testPlayer, false));
		assertEquals(e.getTournamentColour(), "green");
		
		e.setTournamentColour("none");
		assertTrue(e.handleTurnAction("play 5", testPlayer, false));
		assertEquals(e.getTournamentColour(), testColour);
		
		e.setTournamentColour("none");
		assertFalse(e.handleTurnAction("play 6", testPlayer, false));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void testCanPlayerStart(){
		e = new Engine(null);
		
		Player testPlayer = new Player("test", 1);
		
		testPlayer.getHand().add(new Card("red", 0, 0, ""));
		assertTrue(e.canPlayerStart(testPlayer, false));
		
		testPlayer.getHand().clear();
		testPlayer.getHand().add(new Card("someAction", 0, ""));
		assertFalse(e.canPlayerStart(testPlayer, false));
		
		testPlayer.getHand().clear();
		testPlayer.getHand().add(new Card("purple", 0, 0, ""));
		assertTrue(e.canPlayerStart(testPlayer, false));
		
		testPlayer.getHand().clear();
		testPlayer.getHand().add(new Card("purple", 0, 0, ""));
		assertFalse(e.canPlayerStart(testPlayer, true));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void testMaidenRules(){
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		String testColour = "red";
		Mockito.when(mockServer.promptUserForString(Mockito.anyInt(), Mockito.anyString())).thenReturn(testColour);
		
		e = new Engine(mockServer);
		Player testPlayer = new Player("test", 1);
		testPlayer.getHand().add(new Card("white", 6, 0, ""));
		testPlayer.getHand().add(new Card("white", 6, 1, ""));
		
		assertTrue(e.handleTurnAction("play 0", testPlayer, false));
		assertTrue(testPlayer.getDisplay().size() == 1);
		
		assertFalse(e.handleTurnAction("play 1", testPlayer, false));
		assertTrue(testPlayer.getDisplay().size() == 1);
		
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void testAwardToken(){
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		String testColour = "purple";
		Mockito.when(mockServer.promptUserForString(Mockito.anyInt(), Mockito.anyString())).thenReturn(testColour);
		
		e = new Engine(mockServer);
		Player testPlayer = new Player("testPlayer", 1);
		
		testColour = "green";
		e.setTournamentColour(testColour);
		assertTrue(e.awardToken(testPlayer) == null);
		assertTrue(testPlayer.getTokens().get(testColour));
		
		testColour = "red";
		e.setTournamentColour(testColour);
		assertTrue(e.awardToken(testPlayer) == null);
		assertTrue(testPlayer.getTokens().get(testColour));
		
		testColour = "blue";
		e.setTournamentColour(testColour);
		assertTrue(e.awardToken(testPlayer) == null);
		assertTrue(testPlayer.getTokens().get(testColour));
		
		testColour = "yellow";
		e.setTournamentColour(testColour);
		assertTrue(e.awardToken(testPlayer) == null);
		assertTrue(testPlayer.getTokens().get(testColour));
		
		testColour = "purple";
		e.setTournamentColour(testColour);
		assertTrue(e.awardToken(testPlayer) == testPlayer);
		assertTrue(testPlayer.getTokens().get(testColour));
		
		Mockito.verify(mockServer, Mockito.times(5)).updateAllStates(Mockito.anyString());
		
		testColour = "green";
		e.setTournamentColour(testColour);
		assertTrue(e.awardToken(testPlayer) == testPlayer);
		assertTrue(testPlayer.getTokens().get(testColour));
		// should still have been called 5 times, since the player already has the green token
		Mockito.verify(mockServer, Mockito.times(5)).updateAllStates(Mockito.anyString());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testHandleTurnAction(){
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		Mockito.when(mockServer.promptUserForString(Mockito.anyInt(), Mockito.anyString())).thenReturn("");
		
		e = new Engine(mockServer);
		Player testPlayer = new Player("test", 1);
		
		assertFalse(e.handleTurnAction("Withdraw", testPlayer, false));
		
		e.setTournamentColour("purple");
		testPlayer.getHand().clear();
		testPlayer.getHand().add(new Card("purple", 0, 0, ""));
		testPlayer.getHand().add(new Card("red", 	0, 1, ""));
		testPlayer.getHand().add(new Card("blue", 	0, 2, ""));
		testPlayer.getHand().add(new Card("yellow", 0, 3, ""));
		testPlayer.getHand().add(new Card("green", 	0, 4, ""));
		testPlayer.getHand().add(new Card("white", 	0, 5, ""));
		assertTrue (e.handleTurnAction("play 0", testPlayer, false));
		assertFalse(e.handleTurnAction("play 1", testPlayer, false));
		assertFalse(e.handleTurnAction("play 2", testPlayer, false));
		assertFalse(e.handleTurnAction("play 3", testPlayer, false));
		assertFalse(e.handleTurnAction("play 4", testPlayer, false));
		assertTrue (e.handleTurnAction("play 5", testPlayer, false));
		
		e.setTournamentColour("red");
		testPlayer.getHand().clear();
		testPlayer.getHand().add(new Card("purple", 0, 0, ""));
		testPlayer.getHand().add(new Card("red", 	0, 1, ""));
		testPlayer.getHand().add(new Card("blue", 	0, 2, ""));
		testPlayer.getHand().add(new Card("yellow", 0, 3, ""));
		testPlayer.getHand().add(new Card("green", 	0, 4, ""));
		testPlayer.getHand().add(new Card("white", 	0, 5, ""));
		assertFalse(e.handleTurnAction("play 0", testPlayer, false));
		assertTrue (e.handleTurnAction("play 1", testPlayer, false));
		assertFalse(e.handleTurnAction("play 2", testPlayer, false));
		assertFalse(e.handleTurnAction("play 3", testPlayer, false));
		assertFalse(e.handleTurnAction("play 4", testPlayer, false));
		assertTrue (e.handleTurnAction("play 5", testPlayer, false));
		
		e.setTournamentColour("blue");
		testPlayer.getHand().clear();
		testPlayer.getHand().add(new Card("purple", 0, 0, ""));
		testPlayer.getHand().add(new Card("red", 	0, 1, ""));
		testPlayer.getHand().add(new Card("blue", 	0, 2, ""));
		testPlayer.getHand().add(new Card("yellow", 0, 3, ""));
		testPlayer.getHand().add(new Card("green", 	0, 4, ""));
		testPlayer.getHand().add(new Card("white", 	0, 5, ""));
		assertFalse(e.handleTurnAction("play 0", testPlayer, false));
		assertFalse(e.handleTurnAction("play 1", testPlayer, false));
		assertTrue(e.handleTurnAction("play 2", testPlayer, false));
		assertFalse(e.handleTurnAction("play 3", testPlayer, false));
		assertFalse(e.handleTurnAction("play 4", testPlayer, false));
		assertTrue (e.handleTurnAction("play 5", testPlayer, false));
		
		e.setTournamentColour("yellow");
		testPlayer.getHand().clear();
		testPlayer.getHand().add(new Card("purple", 0, 0, ""));
		testPlayer.getHand().add(new Card("red", 	0, 1, ""));
		testPlayer.getHand().add(new Card("blue", 	0, 2, ""));
		testPlayer.getHand().add(new Card("yellow", 0, 3, ""));
		testPlayer.getHand().add(new Card("green", 	0, 4, ""));
		testPlayer.getHand().add(new Card("white", 	0, 5, ""));
		assertFalse(e.handleTurnAction("play 0", testPlayer, false));
		assertFalse(e.handleTurnAction("play 1", testPlayer, false));
		assertFalse(e.handleTurnAction("play 2", testPlayer, false));
		assertTrue (e.handleTurnAction("play 3", testPlayer, false));
		assertFalse(e.handleTurnAction("play 4", testPlayer, false));
		assertTrue (e.handleTurnAction("play 5", testPlayer, false));
		
		e.setTournamentColour("green");
		testPlayer.getHand().clear();
		testPlayer.getHand().add(new Card("purple", 0, 0, ""));
		testPlayer.getHand().add(new Card("red", 	0, 1, ""));
		testPlayer.getHand().add(new Card("blue", 	0, 2, ""));
		testPlayer.getHand().add(new Card("yellow", 0, 3, ""));
		testPlayer.getHand().add(new Card("green", 	0, 4, ""));
		testPlayer.getHand().add(new Card("white", 	0, 5, ""));
		assertFalse(e.handleTurnAction("play 0", testPlayer, false));
		assertFalse(e.handleTurnAction("play 1", testPlayer, false));
		assertFalse(e.handleTurnAction("play 2", testPlayer, false));
		assertFalse(e.handleTurnAction("play 3", testPlayer, false));
		assertTrue (e.handleTurnAction("play 4", testPlayer, false));
		assertTrue (e.handleTurnAction("play 5", testPlayer, false));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testDeckShuffling(){
		
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		
		Engine e = new Engine(mockServer);
		
		Player testPlayer = new Player("testPlayer", 0);
		Card testCard = new Card("", 0, "");
		
		// Add one card to the discard pile
		e.getDiscardPile().add(testCard);
		
		e.buildTestDeck();
		
		for (int i=0; i<43; i++){
			// testPlayer draws all but one card
			e.drawCard(testPlayer);
		}
		
		// The last card left shouldn't be the one in the discard
		assertEquals(1, e.getDeck().size());
		assertFalse( e.getDeck().get(0).equals(testCard) );
		
		// Draw the final card
		e.drawCard(testPlayer);
		
		// testCard should have been shuffled in from the discard, being the only card in the deck
		assertEquals(1, e.getDeck().size());
		assertTrue( e.getDeck().get(0).equals(testCard) );
		
	}
	
	@Test
	public void testOnePersonPlays(){
		
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		Mockito.when(mockServer.getTurnAction(1)).thenReturn("play 0");
		Mockito.when(mockServer.getTurnAction(2)).thenReturn("withdraw");
		Mockito.when(mockServer.getTurnAction(3)).thenReturn("withdraw");
		Mockito.when(mockServer.getTurnAction(4)).thenReturn("withdraw");
		
		Engine e = new Engine(mockServer);
		e.buildTestDeck();
		
		Player testPlayer1 = new Player("Test Player 1", 1);
		Player testPlayer2 = new Player("Test Player 2", 2);
		Player testPlayer3 = new Player("Test Player 3", 3);
		Player testPlayer4 = new Player("Test Player 4", 4);
		
		e.addPlayer(testPlayer1);
		e.addPlayer(testPlayer2);
		e.addPlayer(testPlayer3);
		e.addPlayer(testPlayer4);
		
		e.gameStart(true);
		
		assertTrue(testPlayer1.getTokens().get("red"));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testTwoPeoplePlay(){
		
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		Mockito.when(mockServer.getTurnAction(1)).thenReturn("play 0");
		Mockito.when(mockServer.getTurnAction(2)).thenReturn("play 1");
		Mockito.when(mockServer.getTurnAction(3)).thenReturn("withdraw");
		Mockito.when(mockServer.getTurnAction(4)).thenReturn("withdraw");
		
		Engine e = new Engine(mockServer);
		e.buildTestDeck();
		
		Player testPlayer1 = new Player("Test Player 1", 1);
		Player testPlayer2 = new Player("Test Player 2", 2);
		Player testPlayer3 = new Player("Test Player 3", 3);
		Player testPlayer4 = new Player("Test Player 4", 4);
		
		e.addPlayer(testPlayer1);
		e.addPlayer(testPlayer2);
		e.addPlayer(testPlayer3);
		e.addPlayer(testPlayer4);
		
		e.gameStart(true);
		
		assertTrue(testPlayer2.getTokens().get("red"));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testSomePeoplePlay(){
		
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		Mockito.when(mockServer.getTurnAction(1)).thenReturn("play 0");
		Mockito.when(mockServer.getTurnAction(2)).thenReturn("play 1");
		Mockito.when(mockServer.getTurnAction(3)).thenReturn("play 2");
		Mockito.when(mockServer.getTurnAction(4)).thenReturn("withdraw");
		
		Engine e = new Engine(mockServer);
		e.buildTestDeck();
		
		Player testPlayer1 = new Player("Test Player 1", 1);
		Player testPlayer2 = new Player("Test Player 2", 2);
		Player testPlayer3 = new Player("Test Player 3", 3);
		Player testPlayer4 = new Player("Test Player 4", 4);
		
		e.addPlayer(testPlayer1);
		e.addPlayer(testPlayer2);
		e.addPlayer(testPlayer3);
		e.addPlayer(testPlayer4);
		
		e.gameStart(true);
		
		assertTrue(testPlayer3.getTokens().get("red"));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testAllPeoplePlay(){
		
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		Mockito.when(mockServer.getTurnAction(1)).thenReturn("play 0");
		Mockito.when(mockServer.getTurnAction(2)).thenReturn("play 1");
		Mockito.when(mockServer.getTurnAction(3)).thenReturn("play 2");
		Mockito.when(mockServer.getTurnAction(4)).thenReturn("play 3");
		
		Engine e = new Engine(mockServer);
		e.buildTestDeck();
		
		Player testPlayer1 = new Player("Test Player 1", 1);
		Player testPlayer2 = new Player("Test Player 2", 2);
		Player testPlayer3 = new Player("Test Player 3", 3);
		Player testPlayer4 = new Player("Test Player 4", 4);
		
		e.addPlayer(testPlayer1);
		e.addPlayer(testPlayer2);
		e.addPlayer(testPlayer3);
		e.addPlayer(testPlayer4);
		
		e.gameStart(true);
		
		assertTrue(testPlayer4.getTokens().get("red"));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testStartingWithSupporter(){
		
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		Mockito.when(mockServer.getTurnAction(1)).thenReturn("play 21 red");
		Mockito.when(mockServer.getTurnAction(2)).thenReturn("play 1");
		Mockito.when(mockServer.getTurnAction(3)).thenReturn("play 2");
		Mockito.when(mockServer.getTurnAction(4)).thenReturn("play 3");
		
		Engine e = new Engine(mockServer);
		e.buildTestDeck();
		
		Player testPlayer1 = new Player("Test Player 1", 1);
		Player testPlayer2 = new Player("Test Player 2", 2);
		Player testPlayer3 = new Player("Test Player 3", 3);
		Player testPlayer4 = new Player("Test Player 4", 4);
		
		e.addPlayer(testPlayer1);
		e.addPlayer(testPlayer2);
		e.addPlayer(testPlayer3);
		e.addPlayer(testPlayer4);
		
		e.gameStart(true);
		
		assertTrue(testPlayer4.getTokens().get("red"));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testLosingTokens(){
		
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		Mockito.when(mockServer.getTurnAction(1)).thenReturn("play 0");
		Mockito.when(mockServer.getTurnAction(2)).thenReturn("play 101");
		Mockito.when(mockServer.getTurnAction(3)).thenReturn("play 100");
		Mockito.when(mockServer.getTurnAction(4)).thenReturn("withdraw");
		
		Mockito.when(mockServer.promptUserForString(Mockito.anyInt(), Mockito.anyString())).thenReturn("red");
		
		Engine e = new Engine(mockServer);
		e.buildTestDeck();
		
		Player testPlayer1 = new Player("Test Player 1", 1);
		Player testPlayer2 = new Player("Test Player 2", 2);
		Player testPlayer3 = new Player("Test Player 3", 3);
		Player testPlayer4 = new Player("Test Player 4", 4);
		
		e.addPlayer(testPlayer1);
		e.addPlayer(testPlayer2);
		e.addPlayer(testPlayer3);
		e.addPlayer(testPlayer4);
		
		testPlayer3.getHand().add(new Card("white", 6, 100, ""));
		testPlayer3.setToken("red", true);
		
		testPlayer2.getHand().add(new Card("red", 7, 101, ""));
		
		e.gameStart(true);
		
		assertTrue(testPlayer2.getTokens().get("red"));
		assertFalse(testPlayer3.getTokens().get("red"));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testWinningTheGame(){
		
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		Mockito.when(mockServer.getTurnAction(1)).thenReturn("play 0");
		Mockito.when(mockServer.getTurnAction(2)).thenReturn("withdraw");
		Mockito.when(mockServer.getTurnAction(3)).thenReturn("withdraw");
		Mockito.when(mockServer.getTurnAction(4)).thenReturn("withdraw");
		
		Engine e = new Engine(mockServer);
		e.buildTestDeck();
		
		Player testPlayer1 = new Player("Test Player 1", 1);
		Player testPlayer2 = new Player("Test Player 2", 2);
		Player testPlayer3 = new Player("Test Player 3", 3);
		Player testPlayer4 = new Player("Test Player 4", 4);
		
		testPlayer1.setToken("purple", true);
		testPlayer1.setToken("green", true);
		testPlayer1.setToken("blue", true);
		testPlayer1.setToken("yellow", true);
		
		e.addPlayer(testPlayer1);
		e.addPlayer(testPlayer2);
		e.addPlayer(testPlayer3);
		e.addPlayer(testPlayer4);
		
		assertEquals("Test Player 1", e.gameStart(true));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
}
