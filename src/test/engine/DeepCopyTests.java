package test.engine;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestName;

import cards.Card;
import cards.CardFactory;
import engine.GameState;
import engine.Player;

public class DeepCopyTests {
	
	@Rule public TestName name = new TestName();
	private static Logger logger = Logger.getLogger(DeepCopyTests.class.getName());
	
	private final CardFactory cardFactory = new CardFactory();
	
	@Before
	public void setUp() throws Exception {
		logger.info("Starting " + name.getMethodName() + "...");
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testGameStateDeepCopy(){
		GameState oldGameState = GameState.getInstance();
		
		//Populate Players
		int testPlayerID = 0;
		Player testPlayer = new Player("TestPlayer", testPlayerID);
		oldGameState.addPlayer(testPlayer);
		
		//Populate Deck (done in constructor)
		int testDeckSize = oldGameState.getDeck().size();
		
		//Populate Discard Pile
		Card testCard = cardFactory.makeCard("ivanhoe");
		oldGameState.addCardToDiscardPile(testCard);
		
		//Set additional members
		int testTurn = 100;
		String testColour = "red";
		oldGameState.setTurn(testTurn);
		oldGameState.setTournamentColour(testColour);
		
		//Make copy of GameState
		GameState newGameState = GameState.getCopy();
		
		//Modify old GameState
		oldGameState.removePlayer(testPlayerID);
		oldGameState.getDeck().clear();
		oldGameState.getDiscardPile().clear();
		oldGameState.setTurn(0);
		oldGameState.setTournamentColour("none");

		//Check to see if new GameState has stayed the same
		assertEquals(1, newGameState.getPlayers().size());
		assertEquals(testDeckSize, newGameState.getDeck().size());
		assertEquals(testCard, newGameState.getDiscardPile().get(0));
		assertEquals(testTurn, newGameState.getTurn());
		assertEquals(testColour, newGameState.getTournamentColour());
		
		//Finally, compare old and new together to ensure changes haven't transferred
		assertNotEquals(oldGameState.getPlayers(), newGameState.getPlayers());
		assertNotEquals(oldGameState.getDeck(), newGameState.getDeck());
		assertNotEquals(oldGameState.getDiscardPile(), newGameState.getDiscardPile());
		assertNotEquals(oldGameState.getTurn(), newGameState.getTurn());
		assertNotEquals(oldGameState.getTournamentColour(), newGameState.getTournamentColour());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testPlayerDeepCopy(){
		Player oldPlayer = new Player("TestPlayer01", 0);
		
		//Make copy of Player
		Player newPlayer = Player.getCopy(oldPlayer);
		
		//Populate Tokens
		oldPlayer.setToken("purple", true);
		
		//Populate Hand
		Card testCard = cardFactory.makeCard("ivanhoe");
		oldPlayer.getHand().add(testCard);
		
		//Populate Display
		oldPlayer.getDisplay().add(testCard);
		
		//Populate Special Zone
		oldPlayer.getSpecialZone().add(testCard);
		
		//Set other flags
		oldPlayer.setName("TestPlayer02");
		oldPlayer.setWithdrawn(true);
		oldPlayer.setAddedToDisplay(true);
		
		//Compare old and new together to ensure changes haven't transferred
		assertNotEquals(oldPlayer.getTokens(), newPlayer.getTokens());
		assertNotEquals(oldPlayer.getHand(), newPlayer.getHand());
		assertNotEquals(oldPlayer.getDisplay(), newPlayer.getDisplay());
		assertNotEquals(oldPlayer.getSpecialZone(), newPlayer.getSpecialZone());
		assertNotEquals(oldPlayer.getName(), newPlayer.getName());
		assertNotEquals(oldPlayer.isWithdrawn(), newPlayer.isWithdrawn());
		assertNotEquals(oldPlayer.hasAddedToDisplay(), newPlayer.hasAddedToDisplay());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
}
