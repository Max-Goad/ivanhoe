/*
 * Name: 		ActionCardTests.java
 * Created by:	Max Goad
 * Description:	Tests for all Action Cards and their behaviours. 
 * 				Uses ActionCardHandler and GameStates for easy testing.
 */

package test.engine;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mockito.Mockito;

import cards.Card;
import engine.Engine;
import engine.GameState;
import engine.Player;
import server.ServerInterface;

public class CardStrategyTests {
	
	@Rule public TestName name = new TestName();
	private static Logger logger = Logger.getLogger(CardStrategyTests.class.getName());
	
	private static GameState testGameState;
	
	@BeforeClass
	public static void setUpClass() throws Exception {
		testGameState = GameState.getInstance();
	}
	
	@Before
	public void setUp() throws Exception {
		testGameState.resetGameState(true);
		logger.info("Starting " + name.getMethodName() + "...");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void dodgeTest() {
		
		//Setup testGameState
		String[] playerNames = { "player0", "player1", "player2", "player3" };
		int[] playerIDs = { 0, 1, 2, 3 };
		Player[] players = new Player[4];
		for (int i = 0; i < 3; ++i)
			players[i] = new Player(playerNames[i], playerIDs[i]);
		
		int testCardID = 12345;
		int incorrectCardID = 23456;
		Card testDisplayCard = new Card("testCard", 0, testCardID, "");
		players[1].getDisplay().add(testDisplayCard);
		
		for (Player p : players) {
			testGameState.addPlayer(p);
		}
		
		//Setup dodge test
		String correctArgs = playerIDs[1] + " 0";
		String incorrectArgs = correctArgs + " test";
		String incorrectTargetID = playerIDs[0] + " " + testCardID;
		String incorrectTargetCardID = playerIDs[1] + " " + incorrectCardID;
		Card dodgeCard = new Card("dodge", 0, "");
		boolean testBool = true;

		//Test incorrect argument length
		testBool = dodgeCard.performCardStrategy(testGameState, incorrectArgs);
		
		assertFalse(testBool);
		assertFalse(players[1].getDisplay().isEmpty());
		
		//Test empty display
		testBool = dodgeCard.performCardStrategy(testGameState, incorrectTargetID);
		
		assertFalse(testBool);
		assertFalse(players[1].getDisplay().isEmpty());
		
		//Test incorrect card ID
		testBool = dodgeCard.performCardStrategy(testGameState, incorrectTargetCardID);
		
		assertFalse(testBool);
		assertFalse(players[1].getDisplay().isEmpty());
		
		//Test one card in display
		testBool = dodgeCard.performCardStrategy(testGameState, correctArgs);
		
		assertFalse(testBool);
		assertFalse(players[1].getDisplay().isEmpty());
		
		//Test correct action
		players[1].getDisplay().add(testDisplayCard);
		testBool = dodgeCard.performCardStrategy(testGameState, correctArgs);
		
		assertTrue(testBool);
		assertEquals(1, players[1].getDisplay().size());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void disgraceTest() {
		Card whiteCard1 = new Card("white", 0, 0, "");
		Card whiteCard2 = new Card("white", 0, 1, "");
		Card greenCard = new Card("green", 0, 2, "");
		int testID = 0;
		Player testPlayer = new Player("testPlayer", testID);
		String testIDString = String.valueOf(testID);

		testGameState.addPlayer(testPlayer);
		
		Card disgraceCard = new Card("disgrace", 0, "");
		boolean testBool = true;
		
		//First test on empty display
		testBool = disgraceCard.performCardStrategy(testGameState, testIDString);
		
		assertFalse(testBool);
		assertEquals(0, testGameState.getDiscardPile().size());
		assertEquals(0, testPlayer.getDisplay().size());
		
		//Then test on display with no supporters
		testPlayer.getDisplay().add(greenCard);
		testBool = disgraceCard.performCardStrategy(testGameState, testIDString);
		
		assertFalse(testBool);
		assertEquals(0, testGameState.getDiscardPile().size());
		assertEquals(1, testPlayer.getDisplay().size());
		
		//Test one display with just one card (and it is a supporter)
		testPlayer.getDisplay().clear();
		testPlayer.getDisplay().add(whiteCard1);
		testBool = disgraceCard.performCardStrategy(testGameState, testIDString);
		
		assertFalse(testBool);
		assertEquals(0, testGameState.getDiscardPile().size());
		assertEquals(1, testPlayer.getDisplay().size());
		
		//Finally test on display with supporters
		testPlayer.getDisplay().add(greenCard);
		testPlayer.getDisplay().add(whiteCard2);
		testBool = disgraceCard.performCardStrategy(testGameState, testIDString);
		
		assertTrue(testBool);
		assertEquals(2, testGameState.getDiscardPile().size());
		assertEquals(1, testPlayer.getDisplay().size());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void changeWeaponTest() {
		String testStartColour = "red";
		String testEndColour = "blue";
		String testIncorrectStartColour = "green";
		String testIncorrectEndColour = "red";
		Card changeWeaponCard = new Card("changeWeapon", 0, "");
		boolean testBool = true;
		
		//First test incorrect tournament start colour 
		testGameState.setTournamentColour(testIncorrectStartColour);
		testBool = changeWeaponCard.performCardStrategy(testGameState, testEndColour);
		
		assertFalse(testBool);
		assertEquals(testIncorrectStartColour, testGameState.getTournamentColour());
		
		//Now test incorrect end colour
		testGameState.setTournamentColour(testStartColour);
		testBool = changeWeaponCard.performCardStrategy(testGameState, testIncorrectEndColour);
		
		assertFalse(testBool);
		assertEquals(testStartColour, testGameState.getTournamentColour());
		
		//Finally, test correct start and end colours
		testGameState.setTournamentColour(testStartColour);
		testBool = changeWeaponCard.performCardStrategy(testGameState, testEndColour);
		
		assertTrue(testBool);
		assertEquals(testEndColour, testGameState.getTournamentColour());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void unhorseTest() {
		String testStartColour = "purple";
		String testEndColour = "blue";
		String testIncorrectStartColour = "red";
		String testIncorrectEndColour = "green";
		Card unhorseCard = new Card("unhorse", 0, "");
		boolean testBool = true;
		
		//First test incorrect tournament start colour 
		testGameState.setTournamentColour(testIncorrectStartColour);
		testBool = unhorseCard.performCardStrategy(testGameState, testEndColour);
		
		assertFalse(testBool);
		assertEquals(testIncorrectStartColour, testGameState.getTournamentColour());
		
		//Now test incorrect end colour
		testGameState.setTournamentColour(testStartColour);
		testBool = unhorseCard.performCardStrategy(testGameState, testIncorrectEndColour);
		
		assertFalse(testBool);
		assertEquals(testStartColour, testGameState.getTournamentColour());
		
		//Finally, test correct start and end colours
		testGameState.setTournamentColour(testStartColour);
		testBool = unhorseCard.performCardStrategy(testGameState, testEndColour);
		
		assertTrue(testBool);
		assertEquals(testEndColour, testGameState.getTournamentColour());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void knockDownTest() {
		
		//Setup testGameState
		Player testPlayer = new Player("testPlayer", 0);
		Player targetPlayer = new Player("targetPlayer", 1);
		Player emptyPlayer = new Player("emptyPlayer", 2);
		Card testCard = new Card("testCard", 0, 12345, "");
		targetPlayer.getHand().add(testCard);
		
		testGameState.addPlayer(testPlayer);
		testGameState.addPlayer(targetPlayer);
		testGameState.addPlayer(emptyPlayer);
		
		//Setup knock down test
		String correctArgs = "0 1";
		String incorrectArgs = correctArgs + " test";
		String emptyHand = "0 2";
		Card knockDownCard = new Card("knockDown", 0, "");
		boolean testBool = true;
		
		//Test incorrect argument length
		testBool = knockDownCard.performCardStrategy(testGameState, incorrectArgs);
		
		assertFalse(testBool);
		assertTrue(testPlayer.getHand().isEmpty());
		assertFalse(targetPlayer.getHand().isEmpty());
		
		//Test empty target hand
		testBool = knockDownCard.performCardStrategy(testGameState, emptyHand);
		
		assertFalse(testBool);
		assertTrue(testPlayer.getHand().isEmpty());
		assertFalse(targetPlayer.getHand().isEmpty());
		
		//Test correct action
		testBool = knockDownCard.performCardStrategy(testGameState, correctArgs);
		
		assertTrue(testBool);
		assertFalse(testPlayer.getHand().isEmpty());
		assertTrue(targetPlayer.getHand().isEmpty());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	
	@Test
	public void retreatTest() {
		//Setup testGameState
		Player testPlayer = new Player("testPlayer", 0);
		Card testCard = new Card("testCard", 0, 12345, "");
		testPlayer.getDisplay().add(testCard);
		
		testGameState.addPlayer(testPlayer);
		
		//Setup knock down test
		String correctArgs = "0 0";
		String incorrectArgs = correctArgs + " test";
		Card retreatCard = new Card("retreat", 0, "");
		boolean testBool = true;
		
		//Test incorrect argument length
		testBool = retreatCard.performCardStrategy(testGameState, incorrectArgs);
		
		assertFalse(testBool);
		assertTrue(testPlayer.getHand().isEmpty());
		assertFalse(testPlayer.getDisplay().isEmpty());
		
		//Test empty display
		testGameState.getPlayerByID(0).getDisplay().clear();
		testBool = retreatCard.performCardStrategy(testGameState, correctArgs);
		
		assertFalse(testBool);
		assertTrue(testPlayer.getHand().isEmpty());
		assertTrue(testPlayer.getDisplay().isEmpty());
		
		testGameState.getPlayerByID(0).getDisplay().add(testCard);
		
		//Test correct action
		testBool = retreatCard.performCardStrategy(testGameState, correctArgs);
		
		assertTrue(testBool);
		assertFalse(testPlayer.getHand().isEmpty());
		assertTrue(testPlayer.getDisplay().isEmpty());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void riposteTest() {
		Player testPlayer = new Player("testPlayer", 0);
		Player targetPlayer = new Player("targetPlayer", 1);
		
		Card testCard1 = new Card("white", 0, 1, "");
		Card testCard2 = new Card("green", 0, 2, "");
		targetPlayer.getDisplay().add(testCard1);
		
		testGameState.addPlayer(testPlayer);
		testGameState.addPlayer(targetPlayer);
		
		Card riposteCard = new Card("riposte", 0, "");
		boolean testBool = true;
		
		//Test on empty display
		testBool = riposteCard.performCardStrategy(testGameState, "0 0");
		
		assertFalse(testBool);
		assertEquals(0, testPlayer.getDisplay().size());
		assertEquals(1, targetPlayer.getDisplay().size());
		
		//Test on a display with 1 card
		testBool = riposteCard.performCardStrategy(testGameState, "0 1");
		assertFalse(testBool);
		assertEquals(0, testPlayer.getDisplay().size());
		assertEquals(1, targetPlayer.getDisplay().size());
		
		//Test on non-empty display
		targetPlayer.getDisplay().add(testCard2);
		
		testBool = riposteCard.performCardStrategy(testGameState, "0 1");
		
		assertTrue(testBool);
		assertEquals(1, testPlayer.getDisplay().size());
		assertEquals("green", testPlayer.getDisplay().get(0).getColour());
		assertEquals(1, targetPlayer.getDisplay().size());
		assertEquals("white", targetPlayer.getDisplay().get(0).getColour());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void outmaneuverTest() {
		Player testPlayer01 = new Player("testPlayer01", 1); //Not affected
		Player testPlayer02 = new Player("testPlayer02", 2); //No display
		Player testPlayer03 = new Player("testPlayer03", 3); //1 card display
		Player testPlayer04 = new Player("testPlayer04", 4); //2 card display
		
		testGameState.addPlayer(testPlayer01);
		testGameState.addPlayer(testPlayer02);
		testGameState.addPlayer(testPlayer03);
		testGameState.addPlayer(testPlayer04);
		
		Card outmaneuverCard = new Card("outmaneuver", 0, "");
		boolean testBool = true;
		
		//Test with no cards in any displays
		testBool = outmaneuverCard.performCardStrategy(testGameState, "1");
		
		assertFalse(testBool);
		assertEquals(0, testPlayer01.getDisplay().size());
		assertEquals(0, testPlayer02.getDisplay().size());
		assertEquals(0, testPlayer03.getDisplay().size());
		assertEquals(0, testPlayer04.getDisplay().size());
		
		//Now test with cards in displays
		Card testCard01 = new Card("test01", 0, "");
		Card testCard02 = new Card("test02", 0, "");
		Card testCard03 = new Card("test03", 0, "");
		
		testPlayer03.getDisplay().add(testCard01);
		testPlayer04.getDisplay().add(testCard02);
		testPlayer04.getDisplay().add(testCard03);

		testBool = outmaneuverCard.performCardStrategy(testGameState, "1");
		
		assertTrue(testBool);
		assertEquals(0, testPlayer01.getDisplay().size());
		assertEquals(0, testPlayer02.getDisplay().size());
		assertEquals(1, testPlayer03.getDisplay().size());
		assertEquals(1, testPlayer04.getDisplay().size());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}

	@Test
	public void chargeTest() {
		Player testPlayer01 = new Player("testPlayer01", 1); //3 cards, one low
		Player testPlayer02 = new Player("testPlayer02", 2); //2 cards, no low
		Player testPlayer03 = new Player("testPlayer03", 3); //2 cards, one low
		Player testPlayer04 = new Player("testPlayer03", 3); //1 card, one low
		
		testGameState.addPlayer(testPlayer01);
		testGameState.addPlayer(testPlayer02);
		testGameState.addPlayer(testPlayer03);
		testGameState.addPlayer(testPlayer04);
		
		Card chargeCard = new Card("charge", 0, "");
		boolean testBool = true;
		
		//First test with empty displays
		testBool = chargeCard.performCardStrategy(testGameState, "1");
		
		assertFalse(testBool);
		
		//Now test with filled displays
		Card testCardLow = new Card("testCardLow", 1, 0, "");
		Card testCardMid = new Card("testCardMid", 3, 1, "");
		Card testCardHigh = new Card("testCardHigh", 5, 2, "");
		
		testPlayer01.getDisplay().add(testCardLow);
		testPlayer03.getDisplay().add(testCardLow);
		testPlayer04.getDisplay().add(testCardLow);
		testPlayer01.getDisplay().add(testCardMid);
		testPlayer02.getDisplay().add(testCardMid);
		testPlayer03.getDisplay().add(testCardMid);
		testPlayer01.getDisplay().add(testCardHigh);
		testPlayer02.getDisplay().add(testCardHigh);
		
		testBool = chargeCard.performCardStrategy(testGameState, "1");
		
		assertTrue(testBool);
		assertEquals(2, testPlayer01.getDisplay().size());
		assertEquals(2, testPlayer02.getDisplay().size());
		assertEquals(1, testPlayer03.getDisplay().size());
		assertEquals(1, testPlayer04.getDisplay().size());
		assertEquals(2, testGameState.getDiscardPile().size());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void counterChargeTest() {
		Player testPlayer01 = new Player("testPlayer01", 1); //3 cards, one high
		Player testPlayer02 = new Player("testPlayer02", 2); //2 cards, no high
		Player testPlayer03 = new Player("testPlayer03", 3); //2 cards, one high
		Player testPlayer04 = new Player("testPlayer04", 4); //1 card, one high
		
		testGameState.addPlayer(testPlayer01);
		testGameState.addPlayer(testPlayer02);
		testGameState.addPlayer(testPlayer03);
		testGameState.addPlayer(testPlayer04);
		
		Card counterChargeCard = new Card("counterCharge", 0, "");
		boolean testBool = true;
		
		//First test with empty displays
		testBool = counterChargeCard.performCardStrategy(testGameState, "1");
		
		assertFalse(testBool);
		
		//Now test with filled displays
		Card testCardLow = new Card("testCardLow", 1, 0, "");
		Card testCardMid = new Card("testCardMid", 3, 1, "");
		Card testCardHigh = new Card("testCardHigh", 5, 2, "");
		
		testPlayer01.getDisplay().add(testCardLow);
		testPlayer02.getDisplay().add(testCardLow);
		testPlayer01.getDisplay().add(testCardMid);
		testPlayer02.getDisplay().add(testCardMid);
		testPlayer03.getDisplay().add(testCardMid);
		testPlayer01.getDisplay().add(testCardHigh);
		testPlayer03.getDisplay().add(testCardHigh);
		testPlayer04.getDisplay().add(testCardHigh);
		
		testBool = counterChargeCard.performCardStrategy(testGameState, "1");
		
		assertTrue(testBool);
		assertEquals(2, testPlayer01.getDisplay().size());
		assertEquals(2, testPlayer02.getDisplay().size());
		assertEquals(1, testPlayer03.getDisplay().size());
		assertEquals(1, testPlayer04.getDisplay().size());
		assertEquals(2, testGameState.getDiscardPile().size());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void breakLanceTest() {
		Player testPlayer = new Player("testPlayer", 0);
		
		Card breakLanceCard = new Card("breakLance", 0, "");
		boolean testBool = true;
		
		testGameState.addPlayer(testPlayer);
		
		Card testCard01 = new Card("red", 0, 1, "");
		Card testCard02 = new Card("blue", 0, 2, "");
		Card testCard03 = new Card("green", 0, 3, "");
		Card testCard04 = new Card("purple", 0, 4, "");
		
		//First test empty target display
		testBool = breakLanceCard.performCardStrategy(testGameState, "0");
		
		assertFalse(testBool);
		
		//Test on a display with 1 card (shouldn't discard any)
		testPlayer.getDisplay().add(testCard04);
		
		testBool = breakLanceCard.performCardStrategy(testGameState, "0");
		
		assertFalse(testBool);
		assertEquals(1, testPlayer.getDisplay().size());
		
		//Then test full display
		testPlayer.getDisplay().add(testCard01);
		testPlayer.getDisplay().add(testCard02);
		testPlayer.getDisplay().add(testCard03);
		
		testBool = breakLanceCard.performCardStrategy(testGameState, "0");
		
		assertTrue(testBool);
		assertEquals(3, testPlayer.getDisplay().size());
		assertFalse(testPlayer.getDisplay().contains(testCard04));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void dropWeaponTest() {
		String testStartColour = "red";
		String testIncorrectStartColour = "purple";
		String testEndColour = "green";
		Card dropWeaponCard = new Card("dropWeapon", 0, "");
		boolean testBool = true;
		
		//First test incorrect tournament start colour 
		testGameState.setTournamentColour(testIncorrectStartColour);
		testBool = dropWeaponCard.performCardStrategy(testGameState, "");
		
		assertFalse(testBool);
		assertEquals(testIncorrectStartColour, testGameState.getTournamentColour());
		
		//Finally, test correct start colour
		testGameState.setTournamentColour(testStartColour);
		testBool = dropWeaponCard.performCardStrategy(testGameState, "");
		
		assertTrue(testBool);
		assertEquals(testEndColour, testGameState.getTournamentColour());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	/* Advanced action cards */
	@Test
	public void outwitTest() {
		
		Player p1 = new Player("p1", 1);
		Player p2 = new Player("p2", 2);
		
		testGameState.addPlayer(p1);
		testGameState.addPlayer(p2);
		
		Card outwitCard = new Card("outwit", 0, "");
		Card testRed3Card = new Card("red", 3, 100, "");
		Card testRed4Card = new Card("red", 4, 101, "");
		Card testShieldCard = new Card("shield", 102, "");
		Card testStunnedCard = new Card("stunned", 103, "");
		
		p1.getDisplay().add(testRed3Card);
		p1.getDisplay().add(testRed3Card);
		p2.getDisplay().add(testRed4Card);
		p2.getDisplay().add(testRed4Card);
		p1.getSpecialZone().add(testShieldCard);
		p2.getSpecialZone().add(testStunnedCard);
		
		// Swapping 2 simple cards
		assertTrue(outwitCard.performCardStrategy(testGameState, "1 0 2 0"));
		assertTrue(p1.getDisplay().contains(testRed4Card));
		assertTrue(p2.getDisplay().contains(testRed3Card));

		// Swapping 2 special cards
		assertTrue(outwitCard.performCardStrategy(testGameState, "1 2 2 2"));
		assertTrue(p1.getSpecialZone().contains(testStunnedCard));
		assertTrue(p2.getSpecialZone().contains(testShieldCard));
		
		// Swapping a simple and a special card
		assertTrue(outwitCard.performCardStrategy(testGameState, "1 2 2 0"));
		assertTrue(p1.getDisplay().contains(testRed4Card));
		assertTrue(p2.getSpecialZone().contains(testStunnedCard));
		
		// Attempting to swap with the only simple card in a display
		assertFalse(outwitCard.performCardStrategy(testGameState, "1 2 2 0"));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}

	@Test
	public void shieldTest() {
		
		Player testPlayer = new Player("testPlayer", 1);
		Player testPlayer2 = new Player("testPlayer2", 2);
		
		testGameState.addPlayer(testPlayer);
		testGameState.addPlayer(testPlayer2);
		
		Card shieldCard = new Card("shield", 1, "");
		Card purpleCard = new Card("purple", 1, 100, "");
		Card redCard = new Card("red", 1, 101, "");
		Card blueCard = new Card("blue", 1, 102, "");
		Card yellowCard = new Card("purple", 1, 103, "");
		Card greenCard = new Card("green", 1, 104, "");
		Card supporterCard = new Card("white", 1, 100, "");
		
		// Give the player a Shield card and several other cards 
		// so the every action card is contextually valid to play
		testPlayer.getSpecialZone().add(shieldCard);
		testPlayer.getDisplay().add(purpleCard);
		testPlayer.getDisplay().add(redCard);
		testPlayer.getDisplay().add(blueCard);
		testPlayer.getDisplay().add(yellowCard);
		testPlayer.getDisplay().add(greenCard);
		testPlayer.getDisplay().add(supporterCard);
		testPlayer.getHand().add(purpleCard);
		
		assertTrue(testPlayer.hasShield());
		
		Card dodgeCard = new Card("dodge", 2, "");
		Card disgraceCard = new Card("disgrace", 3, "");
		Card riposteCard = new Card("riposte", 4, "");
		Card outmaneuverCard = new Card("outmaneuver", 5, "");
		Card counterChargeCard = new Card("counterCharge", 6, "");
		Card chargeCard = new Card("charge", 7, "");
		Card breakLanceCard = new Card("breakLance", 2, "");
		Card knockDownCard = new Card("knockDown", 2, "");
		
		assertFalse(dodgeCard.performCardStrategy(testGameState, "1 0"));
		assertFalse(disgraceCard.performCardStrategy(testGameState, "2"));
		assertFalse(riposteCard.performCardStrategy(testGameState, "2 1"));
		assertFalse(outmaneuverCard.performCardStrategy(testGameState, "2"));
		assertFalse(counterChargeCard.performCardStrategy(testGameState, "2"));
		assertFalse(chargeCard.performCardStrategy(testGameState, "2"));
		assertFalse(breakLanceCard.performCardStrategy(testGameState, "1"));
		assertFalse(knockDownCard.performCardStrategy(testGameState, "2 1"));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void stunnedTest() {
		
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		Mockito.when(mockServer.promptUserForString(Mockito.anyInt(), Mockito.anyString())).thenReturn("p1");
		
		Engine e = new Engine(mockServer);
		
		Player p1 = new Player("p1", 1);
		Player p2 = new Player("p2", 2);
		
		e.addPlayer(p1);
		e.addPlayer(p2);
		
		p1.getHand().add(new Card("red", 3, 100, ""));
		p1.getHand().add(new Card("red", 4, 101, ""));
		p1.getHand().add(new Card("riposte", 102, ""));
		p2.getHand().add(new Card("stunned", 1, ""));

		e.setTournamentColour("red");
		
		assertTrue(e.handleTurnAction("play 1", p2, false));
		
		assertTrue(e.handleTurnAction("play 100", p1, false));
		assertFalse(e.handleTurnAction("play 101", p1, false));
		assertFalse(e.handleTurnAction("play 102", p1, false));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void ivanhoeTest() {
		
		ServerInterface mockServer = Mockito.mock(ServerInterface.class);
		Mockito.when(mockServer.promptUserForString(Mockito.anyInt(), Mockito.anyString())).thenReturn("yes");
		
		Engine e = new Engine(mockServer);
		
		Player p1 = new Player("p1", 1);
		Player p2 = new Player("p2", 2);
		
		e.addPlayer(p1);
		e.addPlayer(p2);
		
		p1.getHand().add(new Card("dropWeapon", 0, ""));
		p2.getHand().add(new Card("ivanhoe", 1, ""));
		
		e.setTournamentColour("red");
		
		assertFalse(e.handleTurnAction("play 1", p2, false));
		assertFalse(e.handleTurnAction("play 0", p1, false));
		
		Mockito.when(mockServer.promptUserForString(Mockito.anyInt(), Mockito.anyString())).thenReturn("no");
		
		p1.getHand().add(new Card("dropWeapon", 0, ""));
		assertTrue(e.handleTurnAction("play 0", p1, false));
		
		assertTrue(e.getTournamentColour().equals("green"));
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
}
