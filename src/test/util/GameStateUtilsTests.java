package test.util;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import cards.Card;
import cards.CardFactory;
import engine.GameState;
import engine.Player;
import util.GameStateUtils;

public class GameStateUtilsTests {

	@Rule public TestName name = new TestName();
	private static Logger logger = Logger.getLogger(GameStateUtilsTests.class.getName());
	
	private ArrayList<Card> testHand = new ArrayList<>();
	private CardFactory testCardFactory = new CardFactory();
	
	@BeforeClass
	public static void setUpClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
		testHand.clear();
		logger.info("Starting " + name.getMethodName() + "...");
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void colourSubsetTest() {
		ArrayList<Card> returnSubset = new ArrayList<>();
		ArrayList<Card> expectedReturn = new ArrayList<>();
		String testColour = "red";
		
		//No Cards
		returnSubset = GameStateUtils.colourSubset(testHand, testColour);
		
		assertTrue(returnSubset.isEmpty());
		assertEquals(expectedReturn, returnSubset);
		
		//Action Cards Only
		testHand.add(testCardFactory.makeCard("dodge"));
		testHand.add(testCardFactory.makeCard("shield"));
		testHand.add(testCardFactory.makeCard("ivanhoe"));
		returnSubset = GameStateUtils.colourSubset(testHand, testColour);

		assertTrue(returnSubset.isEmpty());
		assertEquals(expectedReturn, returnSubset);
		
		//Coloured Cards (None matching testColour)
		testHand.add(testCardFactory.makeCard("purple 5"));
		testHand.add(testCardFactory.makeCard("green 1"));
		testHand.add(testCardFactory.makeCard("yellow 2"));
		testHand.add(testCardFactory.makeCard("blue 4"));
		testHand.add(testCardFactory.makeCard("white 6"));
		returnSubset = GameStateUtils.colourSubset(testHand, testColour);

		assertTrue(returnSubset.isEmpty());
		assertEquals(expectedReturn, returnSubset);
		
		//Coloured Cards (Matching testColour)
		Card testCard01 = testCardFactory.makeCard("red 3");
		Card testCard02 = testCardFactory.makeCard("red 4");
		Card testCard03 = testCardFactory.makeCard("red 5");
		
		expectedReturn.add(testCard01);
		expectedReturn.add(testCard02);
		expectedReturn.add(testCard03);
		testHand.add(testCard01);
		testHand.add(testCard02);
		testHand.add(testCard03);
		returnSubset = GameStateUtils.colourSubset(testHand, testColour);
		
		assertFalse(returnSubset.isEmpty());
		assertEquals(expectedReturn, returnSubset);
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}

	@Test
	public void getHighestValueCardTest() {
		Card returnCard = null;
		Card expectedHighestCard = null;
		
		//First, we'll test with no cards
		returnCard = GameStateUtils.getHighestValueCard(testHand);
		
		assertNull(returnCard);
		
		//Then, we'll test with action cards only
		testHand.add(testCardFactory.makeCard("dodge"));
		testHand.add(testCardFactory.makeCard("shield"));
		testHand.add(testCardFactory.makeCard("ivanhoe"));
		returnCard = GameStateUtils.getHighestValueCard(testHand);
		
		assertNull(returnCard);
		
		//Now we'll add in some coloured cards
		expectedHighestCard = testCardFactory.makeCard("purple 5");
		testHand.add(expectedHighestCard);
		testHand.add(testCardFactory.makeCard("green 1"));
		testHand.add(testCardFactory.makeCard("yellow 2"));
		testHand.add(testCardFactory.makeCard("red 3"));
		testHand.add(testCardFactory.makeCard("blue 4"));
		testHand.add(testCardFactory.makeCard("purple 4"));
		returnCard = GameStateUtils.getHighestValueCard(testHand);
		
		assertEquals(expectedHighestCard, returnCard);
		
		//And then some supporter cards
		expectedHighestCard = testCardFactory.makeCard("white 6");
		testHand.add(expectedHighestCard);
		testHand.add(testCardFactory.makeCard("white 2"));
		returnCard = GameStateUtils.getHighestValueCard(testHand);
		
		assertEquals(expectedHighestCard, returnCard);

		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void getHighestValueColourTest() {
		String returnColour = null;
		String expectedColour = null;
		
		//First, we'll test with no cards
		returnColour = GameStateUtils.getHighestValueColour(testHand);
		
		assertNull(returnColour);
		
		//Then, we'll test with action cards only
		testHand.add(testCardFactory.makeCard("dodge"));
		testHand.add(testCardFactory.makeCard("shield"));
		testHand.add(testCardFactory.makeCard("ivanhoe"));
		returnColour = GameStateUtils.getHighestValueColour(testHand);
		
		assertNull(returnColour);
		
		//Now we'll add some coloured cards
		expectedColour = "purple";
		testHand.add(testCardFactory.makeCard("purple 5"));
		testHand.add(testCardFactory.makeCard("green 1"));
		testHand.add(testCardFactory.makeCard("yellow 2"));
		testHand.add(testCardFactory.makeCard("red 3"));
		testHand.add(testCardFactory.makeCard("blue 4"));
		testHand.add(testCardFactory.makeCard("purple 4"));
		returnColour = GameStateUtils.getHighestValueColour(testHand);
		
		assertEquals(expectedColour, returnColour);
		
		//And then some supporter cards (this function should ignore supporters)
		expectedColour = "purple";
		testHand.add(testCardFactory.makeCard("white 6"));
		testHand.add(testCardFactory.makeCard("white 2"));
		testHand.add(testCardFactory.makeCard("white 3"));
		returnColour = GameStateUtils.getHighestValueColour(testHand);
		
		assertEquals(expectedColour, returnColour);

		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void getTotalCardValueTest() {
		int expectedReturn, actualReturn;
		
		//First, we'll test with no cards
		expectedReturn = 0;
		actualReturn = GameStateUtils.getTotalCardValue(testHand);
		
		assertEquals(expectedReturn, actualReturn);
		
		//Then, we'll test with action cards only
		expectedReturn = 0;
		testHand.add(testCardFactory.makeCard("dodge"));
		testHand.add(testCardFactory.makeCard("shield"));
		testHand.add(testCardFactory.makeCard("ivanhoe"));
		actualReturn = GameStateUtils.getTotalCardValue(testHand);
		
		assertEquals(expectedReturn, actualReturn);
		
		//Now we'll add some coloured cards
		expectedReturn = 15;
		testHand.add(testCardFactory.makeCard("green 1"));
		testHand.add(testCardFactory.makeCard("yellow 2"));
		testHand.add(testCardFactory.makeCard("red 3"));
		testHand.add(testCardFactory.makeCard("blue 4"));
		testHand.add(testCardFactory.makeCard("purple 5"));
		actualReturn = GameStateUtils.getTotalCardValue(testHand);
		
		assertEquals(expectedReturn, actualReturn);
		
		//And then some supporter cards
		expectedReturn = 26;
		testHand.add(testCardFactory.makeCard("white 6"));
		testHand.add(testCardFactory.makeCard("white 2"));
		testHand.add(testCardFactory.makeCard("white 3"));
		actualReturn = GameStateUtils.getTotalCardValue(testHand);
		
		assertEquals(expectedReturn, actualReturn);
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void getLeadTournamentScoreTest() {
		GameState testGameState = GameState.getInstance();
		int expectedReturn, actualReturn;
		
		//Empty displays
		expectedReturn = 0;
		actualReturn = GameStateUtils.getLeadTournamentScore();
		
		assertEquals(expectedReturn, actualReturn);
		
		//One display has cards
		Player testPlayer01 = new Player("TestPlayer01", 1);
		ArrayList<Card> display01 = new ArrayList<>();
		display01.add(testCardFactory.makeCard("green 1"));
		testPlayer01.setDisplay(display01);
		testGameState.addPlayer(testPlayer01);
		
		expectedReturn = 1;
		actualReturn = GameStateUtils.getLeadTournamentScore();
		
		assertEquals(expectedReturn, actualReturn);
		
		//Five displays have cards
		Player testPlayer02 = new Player("TestPlayer02", 2);
		ArrayList<Card> display02 = new ArrayList<>();
		display02.add(testCardFactory.makeCard("yellow 2"));
		testPlayer02.setDisplay(display02);
		testGameState.addPlayer(testPlayer02);

		Player testPlayer03 = new Player("TestPlayer03", 3);
		ArrayList<Card> display03 = new ArrayList<>();
		display03.add(testCardFactory.makeCard("red 3"));
		testPlayer03.setDisplay(display03);
		testGameState.addPlayer(testPlayer03);
		
		Player testPlayer04 = new Player("TestPlayer04", 4);
		ArrayList<Card> display04 = new ArrayList<>();
		display04.add(testCardFactory.makeCard("blue 4"));
		testPlayer04.setDisplay(display04);
		testGameState.addPlayer(testPlayer04);
		
		Player testPlayer05 = new Player("TestPlayer05", 5);
		ArrayList<Card> display05 = new ArrayList<>();
		display05.add(testCardFactory.makeCard("purple 5"));
		testPlayer05.setDisplay(display05);
		testGameState.addPlayer(testPlayer05);
		
		expectedReturn = 5;
		actualReturn = GameStateUtils.getLeadTournamentScore();
		
		assertEquals(expectedReturn, actualReturn);
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
}
