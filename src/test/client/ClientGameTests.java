/*
 * Name: 		ClientGameTests.java
 * Created by:	Max Goad
 * Description:	Tests for IvanhoeClient's game functionality (start, end, game loop)
 */

package test.client;

import static org.junit.Assert.*;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mockito.Mockito;

import client.ClientUserInterface;
import client.IvanhoeClient;
import server.message.Message;
import util.TestUtilFns;

public class ClientGameTests {

	@Rule public TestName name = new TestName();
	private static Logger logger = Logger.getLogger(ClientGameTests.class.getName());
	
	private IvanhoeClient testClient;
	
	@Before
	public void setUp() throws Exception {
		logger.info("Starting " + name.getMethodName() + "...");
	}

	@After
	public void tearDown() throws Exception {
		testClient.stop();
		testClient = null;
	}
	
	@Test
	public void testGameStart() {
		Message gameStartTestMessage = null;
		String testName = name.getMethodName();
		
		Socket mockSocket = Mockito.mock(Socket.class);
		ClientUserInterface mockUI = TestUtilFns.createEmptyMockUI();
		
		//Begin testing
		try {
			PipedOutputStream outputPipe = new PipedOutputStream();	//This writes what we want to pass to the client to the input stream below
			PipedInputStream networkInStream = new PipedInputStream(outputPipe);	//This is passed to the NetworkRelay so it can read what we send it
			OutputStream mockOut = Mockito.mock(OutputStream.class);
			
			//Socket mocking
			Mockito.when(mockSocket.getInputStream()).thenReturn(networkInStream);
			Mockito.when(mockSocket.getOutputStream()).thenReturn(mockOut);

			//Creation of the testClient
			testClient = new IvanhoeClient();
			testClient.setDoLogFlag(false);
			//Skip startup steps
			testClient.initialize(testName, "testIP", 1);
			testClient.setSocket(mockSocket);
			testClient.setEstablishConnectionFlag(true);
			testClient.setUserInterface(mockUI);
			testClient.start();
			
			//Streams we're using in the test
			ObjectOutputStream testOutStream = new ObjectOutputStream(outputPipe);	//Write to this in test
			testOutStream.flush();
			
			//Now we need to communicate with IvanhoeClient to do the test
			gameStartTestMessage = new Message(Message.Type.GAME_START);
			testOutStream.writeObject(gameStartTestMessage);
			
			//These stopped working on when run with other tests for some reason...
			//Mockito.verify(mockSocket, Mockito.times(1)).getOutputStream();
			//Mockito.verify(mockSocket, Mockito.times(1)).getInputStream();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		TestUtilFns.wait(10); //Wait for IvanhoeClient to finish
		
		//Asserts
		assertNotNull(gameStartTestMessage);
		assertTrue(testClient.isInitialized());
		assertTrue(testClient.hasSocketInitialized());
		assertTrue(testClient.isRunning());
		
		//See above for comment reason
		//Mockito.verifyNoMoreInteractions(mockSocket);
				
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}

	@Test
	public void testGameLoop() {
		ArrayList<Message> gameLoopMsgs = new ArrayList<>();
		ArrayList<Message> gameLoopResponses = new ArrayList<>();
		Set<Integer> needResponsesFrom = new HashSet<Integer>();
		
		String testName = name.getMethodName();
		String testStringPrompt = "TEST STRING PROMPT";
		String testTurnAction = "TEST TURN ACTION";
		
		Socket mockSocket = Mockito.mock(Socket.class);
		ClientUserInterface mockUI = Mockito.mock(ClientUserInterface.class);
		
		//Begin testing
		try {
			//Pipes used for streams
			PipedOutputStream outputPipe = new PipedOutputStream();	//This writes what we want to pass to the NetworkRelay to the input stream below
			outputPipe.flush();
			PipedInputStream inputPipe = new PipedInputStream();	//This reads what is written to the output stream below
			
			//Used in IvanhoeClient
			PipedInputStream networkInStream = new PipedInputStream(outputPipe);	//This is passed to the NetworkRelay so it can read what we send it
			PipedOutputStream networkOutStream = new PipedOutputStream(inputPipe);	//This is passed to the NetworkRelay so it can write to it
			
			//Socket mocking
			Mockito.when(mockSocket.getInputStream()).thenReturn(networkInStream);
			Mockito.when(mockSocket.getOutputStream()).thenReturn(networkOutStream);
			
			//UI mocking
			Mockito.doNothing().when(mockUI).start(Mockito.anyInt());
			Mockito.doNothing().when(mockUI).stop();
			Mockito.doNothing().when(mockUI).updateState(Mockito.anyString());
			Mockito.doNothing().when(mockUI).reportErrorToUser(Mockito.anyString());
			Mockito.when(mockUI.promptUserForString(Mockito.anyString())).thenReturn(testStringPrompt);
			Mockito.when(mockUI.getTurnAction()).thenReturn(testTurnAction);

			//Creation of the testClient
			testClient = new IvanhoeClient();
			testClient.setDoLogFlag(false);
			//Skip startup steps
			testClient.initialize(testName, "testIP", 1);
			testClient.setSocket(mockSocket);
			testClient.setEstablishConnectionFlag(true);
			testClient.setUserInterface(mockUI);
			testClient.setPlayingGameFlag(true);
			testClient.start();
			
			//Streams we're using in the test
			ObjectOutputStream testOutStream = new ObjectOutputStream(outputPipe);	//Write to this in test
			testOutStream.flush();
			ObjectInputStream testInStream = new ObjectInputStream(inputPipe);
			
			//Now we need to communicate with IvanhoeClient to do the test
			gameLoopMsgs.add(new Message(Message.Type.GAME_UPDATE_STATE, ""));
			gameLoopMsgs.add(new Message(Message.Type.GAME_USER_ERROR, ""));
			gameLoopMsgs.add(new Message(Message.Type.GAME_STRING_REQUEST, ""));
			gameLoopMsgs.add(new Message(Message.Type.GAME_TURN_ACTION_REQUEST, ""));
			gameLoopMsgs.add(new Message(Message.Type.GAME_END, ""));
			
			//Track which of these requests needs responses to wait for
			needResponsesFrom.add(2);
			needResponsesFrom.add(3);
			
			logger.info("Entering test game loop...");
			
			for (int i = 0; i < gameLoopMsgs.size(); ++i) {
				logger.info("Loop " + i + " started...");
				testOutStream.writeObject(gameLoopMsgs.get(i));
				if (needResponsesFrom.contains(i)) {
					logger.info("Loop " + i + " reading from InStream...");
					gameLoopResponses.add((Message)testInStream.readObject());
					logger.info("Loop " + i + " done reading from InStream...");
				}
				else {
					gameLoopResponses.add(null); //Fill out arraylist evenly with gameLoopMsgs
				}
				logger.info("Loop " + i + " finished...");
			}
			
			logger.info("Exiting test game loop...");
			
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		TestUtilFns.wait(1000); //Wait for IvanhoeClient to finish (pipes take a long time on this test)

		//Verify fns were called
		//Mockito.verify(mockUI, Mockito.times(1)).start(); //TODO: Re-add this when implementation done
		Mockito.verify(mockUI, Mockito.times(1)).updateState(Mockito.anyString());
		Mockito.verify(mockUI, Mockito.times(1)).reportErrorToUser(Mockito.anyString());
		Mockito.verify(mockUI, Mockito.times(1)).promptUserForString(Mockito.anyString());
		Mockito.verify(mockUI, Mockito.times(1)).getTurnAction();
		//Mockito.verifyNoMoreInteractions(mockUI);
		
		//Asserts
		assertEquals(testStringPrompt, gameLoopResponses.get(2).contents);
		assertEquals(testTurnAction, gameLoopResponses.get(3).contents);
		assertTrue(testClient.isInitialized());
		assertTrue(testClient.hasSocketInitialized());
		assertTrue(testClient.isRunning());
		assertFalse(testClient.isPlayingGame());
		
		//See above for comment reason
		//Mockito.verifyNoMoreInteractions(mockSocket);
				
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testGameEnd() {
		Message gameEndMessage = null;
		Message gameEndResponse = null;
		
		String testName = name.getMethodName();

		Socket mockSocket = Mockito.mock(Socket.class);
		ClientUserInterface mockUI = TestUtilFns.createEmptyMockUI();
		
		String testEndResponse = "no";
		ByteArrayInputStream testStartInStream = new ByteArrayInputStream(testEndResponse.getBytes());

		//Begin testing
		try {
			//Pipes used for streams
			PipedOutputStream outputPipe = new PipedOutputStream();	//This writes what we want to pass to the NetworkRelay to the input stream below
			outputPipe.flush();
			PipedInputStream inputPipe = new PipedInputStream();	//This reads what is written to the output stream below
			
			//Used in IvanhoeClient
			PipedInputStream networkInStream = new PipedInputStream(outputPipe);	//This is passed to the NetworkRelay so it can read what we send it
			PipedOutputStream networkOutStream = new PipedOutputStream(inputPipe);	//This is passed to the NetworkRelay so it can write to it
			
			//Socket mocking
			Mockito.when(mockSocket.getInputStream()).thenReturn(networkInStream);
			Mockito.when(mockSocket.getOutputStream()).thenReturn(networkOutStream);

			//Socket mocking
			Mockito.when(mockSocket.getInputStream()).thenReturn(networkInStream);
			Mockito.when(mockSocket.getOutputStream()).thenReturn(networkOutStream);

			//Creation of the testClient
			testClient = new IvanhoeClient();
			testClient.setUserInputStream(testStartInStream);
			testClient.setDoLogFlag(false);
			//Skip startup steps
			testClient.initialize(testName, "testIP", 1);
			testClient.setSocket(mockSocket);
			testClient.setEstablishConnectionFlag(true);
			testClient.setPlayingGameFlag(true);
			testClient.setUserInterface(mockUI);
			testClient.start();
			
			//Wait for IvanhoeClient to get to game loop
			TestUtilFns.wait(10); 
			//Make sure it's in a game
			assertTrue(testClient.isPlayingGame());

			//Streams we're using in the test
			ObjectOutputStream testOutStream = new ObjectOutputStream(outputPipe);	//Write to this in test
			testOutStream.flush();
			ObjectInputStream testInStream = new ObjectInputStream(inputPipe);		//Read from this in test
			
			
			//Now we need to communicate with IvanhoeClient to do the test
			gameEndMessage = new Message(Message.Type.GAME_END);
			testOutStream.writeObject(gameEndMessage);
			gameEndResponse = (Message)testInStream.readObject();

			//These stopped working on when run with other tests for some reason...
			Mockito.verify(mockSocket, Mockito.times(1)).getOutputStream();
			Mockito.verify(mockSocket, Mockito.times(1)).getInputStream();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}

		TestUtilFns.wait(10);		

		//Asserts
		assertNotNull(gameEndResponse);
		assertEquals(Message.Type.CLIENT_FINISHED_PLAYING, gameEndResponse.type);
		assertFalse(testClient.isRunning());
		assertFalse(testClient.isPlayingGame());

		//See above for comment reason
		Mockito.verifyNoMoreInteractions(mockSocket);

		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testGameEndAndRestart() {
		Message gameEndMessage = null;
		Message gameEndResponse = null;
		
		String testName = name.getMethodName();

		Socket mockSocket = Mockito.mock(Socket.class);
		ClientUserInterface mockUI = TestUtilFns.createEmptyMockUI();
		
		String testEndResponse = "yes";
		ByteArrayInputStream testStartInStream = new ByteArrayInputStream(testEndResponse.getBytes());

		//Begin testing
		try {
			//Pipes used for streams
			PipedOutputStream outputPipe = new PipedOutputStream();	//This writes what we want to pass to the NetworkRelay to the input stream below
			outputPipe.flush();
			PipedInputStream inputPipe = new PipedInputStream();	//This reads what is written to the output stream below
			
			//Used in IvanhoeClient
			PipedInputStream networkInStream = new PipedInputStream(outputPipe);	//This is passed to the NetworkRelay so it can read what we send it
			PipedOutputStream networkOutStream = new PipedOutputStream(inputPipe);	//This is passed to the NetworkRelay so it can write to it
			
			//Socket mocking
			Mockito.when(mockSocket.getInputStream()).thenReturn(networkInStream);
			Mockito.when(mockSocket.getOutputStream()).thenReturn(networkOutStream);

			//Socket mocking
			Mockito.when(mockSocket.getInputStream()).thenReturn(networkInStream);
			Mockito.when(mockSocket.getOutputStream()).thenReturn(networkOutStream);

			//Creation of the testClient
			testClient = new IvanhoeClient();
			testClient.setUserInputStream(testStartInStream);
			testClient.setDoLogFlag(false);
			//Skip startup steps
			testClient.initialize(testName, "testIP", 1);
			testClient.setSocket(mockSocket);
			testClient.setEstablishConnectionFlag(true);
			testClient.setPlayingGameFlag(true);
			testClient.setUserInterface(mockUI);
			testClient.start();
			
			//Wait for IvanhoeClient to get to game loop
			TestUtilFns.wait(10); 
			//Make sure it's in a game
			assertTrue(testClient.isPlayingGame());

			//Streams we're using in the test
			ObjectOutputStream testOutStream = new ObjectOutputStream(outputPipe);	//Write to this in test
			testOutStream.flush();
			ObjectInputStream testInStream = new ObjectInputStream(inputPipe);		//Read from this in test
			
			
			//Now we need to communicate with IvanhoeClient to do the test
			gameEndMessage = new Message(Message.Type.GAME_END);
			testOutStream.writeObject(gameEndMessage);
			gameEndResponse = (Message)testInStream.readObject();

			//These stopped working on when run with other tests for some reason...
			Mockito.verify(mockSocket, Mockito.times(1)).getOutputStream();
			Mockito.verify(mockSocket, Mockito.times(1)).getInputStream();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}

		TestUtilFns.wait(10);		

		//Asserts
		assertNotNull(gameEndResponse);
		assertEquals(Message.Type.CLIENT_CONTINUE_PLAYING, gameEndResponse.type);
		assertTrue(testClient.isRunning());
		assertFalse(testClient.isPlayingGame());

		//See above for comment reason
		Mockito.verifyNoMoreInteractions(mockSocket);

		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
}
