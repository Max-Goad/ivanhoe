/*
 * Name: 		ClientBasicTests.java
 * Created by:	Max Goad
 * Description:	Tests for basic IvanhoeClient functionality, such as startup and running.
 */

package test.client;

import static org.junit.Assert.*;

import java.io.*;
import java.net.Socket;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mockito.Mockito;

import client.ClientConfig;
import client.IvanhoeClient;
import server.message.Message;
import util.TestUtilFns;

public class ClientBasicTests {

	@Rule public TestName name = new TestName();
	private static Logger logger = Logger.getLogger(ClientBasicTests.class.getName());
	
	private IvanhoeClient testClient;
	
	@Before
	public void setUp() throws Exception {
		logger.info("Starting " + name.getMethodName() + "...");
	}

	@After
	public void tearDown() throws Exception {
		testClient.stop();
		testClient = null;
	}

	@Test
	public void testClientStartup() {
		testClient = new IvanhoeClient();
		testClient.setDoLogFlag(false);
		testClient.start();
		
		//We need this wait block in order to allow the client thread time to start (shouldn't need more than 50 ms)
		TestUtilFns.wait(50);
		
		assertTrue(testClient.isRunning());

		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testClientStartupWithArgs() {
		InputStream testInStream = System.in;
		boolean testDoLog = false;
		
		testClient = new IvanhoeClient();
		testClient.setUserInputStream(testInStream);
		testClient.setDoLogFlag(false);
		
		//Mock out a socket to avoid exceptions
		Socket mockSocket = TestUtilFns.createEmptyMockSocket();
		testClient.setSocket(mockSocket);
		
		//Start client
		testClient.start();
		
		//We need this wait block in order to allow the client thread time to start (shouldn't need more than 50 ms)
		TestUtilFns.wait(50);
		
		assertTrue(testClient.isRunning());
		assertEquals(testInStream, testClient.getUserInputStream());
		assertEquals(testDoLog, testClient.isLogging());

		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testClientStartupInput() {
		String testOptions = ClientConfig.TEST_SIMPLE_STARTUP;
		ByteArrayInputStream testInStream = new ByteArrayInputStream(testOptions.getBytes());
		
		testClient = new IvanhoeClient();
		testClient.setUserInputStream(testInStream);
		testClient.setDoLogFlag(false);
		
		
		//Mock out a socket to avoid exceptions
		Socket mockSocket = TestUtilFns.createEmptyMockSocket();
		testClient.setSocket(mockSocket);
		
		//Start client
		testClient.start();
		
		//We need this wait block in order to allow the client thread time to start (shouldn't need more than 50 ms)
		TestUtilFns.wait(50);
		
		assertTrue(testClient.isRunning());
		assertEquals("test", testClient.getPlayerName());

		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testClientShutdown() {
		String testOptions = ClientConfig.TEST_SIMPLE_STARTUP;
		ByteArrayInputStream testInStream = new ByteArrayInputStream(testOptions.getBytes());
		
		testClient = new IvanhoeClient();
		testClient.setUserInputStream(testInStream);
		testClient.setDoLogFlag(false);
		
		//Mock out a socket to avoid exceptions
		Socket mockSocket = TestUtilFns.createEmptyMockSocket();
		testClient.setSocket(mockSocket);
		
		//Start client
		testClient.start();
		
		//We need this wait block in order to allow the server thread time to start (shouldn't need more than 50 ms)
		TestUtilFns.wait(50);
		
		//First make sure client is running
		assertTrue(testClient.isRunning());

		//Then call stop
		testClient.stop();

		//Check again to see if client still running
		assertFalse(testClient.isRunning());

		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test 
	public void testEstablishConnection() {
		Message requestMsg = null;
		Message responseMsg = null;
		Message confirmMsg = null;
		String testName = name.getMethodName();
		
		Socket mockSocket = Mockito.mock(Socket.class);
		
		//Begin testing
		try {
			//Pipes used for streams
			PipedOutputStream outputPipe = new PipedOutputStream();	//This writes what we want to pass to the NetworkRelay to the input stream below
			outputPipe.flush();
			PipedInputStream inputPipe = new PipedInputStream();	//This reads what is written to the output stream below
			
			//Used in IvanhoeClient
			PipedInputStream networkInStream = new PipedInputStream(outputPipe);	//This is passed to the NetworkRelay so it can read what we send it
			PipedOutputStream networkOutStream = new PipedOutputStream(inputPipe);	//This is passed to the NetworkRelay so it can write to it
			
			//Socket mocking
			Mockito.when(mockSocket.getInputStream()).thenReturn(networkInStream);
			Mockito.when(mockSocket.getOutputStream()).thenReturn(networkOutStream);
			
			//Input stream mocking

			//Creation of the testClient
			testClient = new IvanhoeClient();
			testClient.setDoLogFlag(false);
			//Skip startup/socket exchanges using initialize() and setSocket() before start()
			testClient.initialize(testName, "testIP", 0);
			testClient.setSocket(mockSocket);
			testClient.start();
			
			//Streams we're using in the test
			ObjectOutputStream testOutStream = new ObjectOutputStream(outputPipe);	//Write to this in test
			testOutStream.flush();
			ObjectInputStream testInStream = new ObjectInputStream(inputPipe);		//Read from this in test
			
			//Now we need to communicate with IvanhoeClient to do the test
			requestMsg = new Message(Message.Type.CLIENT_REGISTRATION_REQUEST);
			testOutStream.writeObject(requestMsg);
			responseMsg = (Message)testInStream.readObject();
			confirmMsg = new Message(Message.Type.CLIENT_REGISTRATION_CONFIRMATION);
			
			Mockito.verify(mockSocket, Mockito.times(1)).getInputStream();
			Mockito.verify(mockSocket, Mockito.times(1)).getOutputStream();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		TestUtilFns.wait(10); //Wait for IvanhoeClient to finish
		
		//Asserts
		assertNotNull(requestMsg);
		assertNotNull(responseMsg);
		assertNotNull(confirmMsg);
		assertEquals(Message.Type.CLIENT_REGISTRATION_RESPONSE, responseMsg.type);
		assertEquals(testName, responseMsg.contents);
		assertTrue(testClient.isInitialized());
		assertTrue(testClient.hasSocketInitialized());
		assertTrue(testClient.isRunning());
		
		Mockito.verifyNoMoreInteractions(mockSocket);
				
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
}
