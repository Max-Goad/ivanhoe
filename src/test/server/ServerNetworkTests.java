/*
 * Name: 		ServerNetworkTests.java
 * Created by:	Max Goad
 * Description:	Tests for server network functionality classes, such as NetworkMediator and NetworkRelay.
 */

package test.server;

import static org.junit.Assert.*;

import java.io.*;
import java.net.*;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mockito.Mockito;

import server.IvanhoeServer;
import server.NetworkMediator;
import server.NetworkRelay;
import server.ServerConfig;
import server.message.Message;
import util.TestUtilFns;

public class ServerNetworkTests {

	@Rule public TestName name = new TestName();
	private static Logger logger = Logger.getLogger(ServerNetworkTests.class.getName());
	
	private NetworkMediator testNetworkMediator;
	
	@Before
	public void setUp() throws Exception {
		IvanhoeServer mockServer = Mockito.mock(IvanhoeServer.class);
		testNetworkMediator = new NetworkMediator(mockServer);
		logger.info("Starting " + name.getMethodName() + "...");
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testCreateNewRelay() {
		Message requestMsg = null;
		Message responseMsg = null;
		Message confirmMsg = null;
		String testName = name.getMethodName();

		Socket mockSocket = Mockito.mock(Socket.class);
		
		try {
			//Pipes used for streams
			PipedOutputStream outputPipe = new PipedOutputStream();	//This writes what we want to pass to the NetworkRelay to the input stream below
			PipedInputStream inputPipe = new PipedInputStream();	//This reads what is written to the output stream below
			//Used in NetworkRelay
			PipedInputStream networkInStream = new PipedInputStream(outputPipe);	//This is passed to the NetworkRelay so it can read what we send it
			PipedOutputStream networkOutStream = new PipedOutputStream(inputPipe);	//This is passed to the NetworkRelay so it can write to it
			
			//Socket mocking
			Mockito.when(mockSocket.getInputStream()).thenReturn(networkInStream);
			Mockito.when(mockSocket.getOutputStream()).thenReturn(networkOutStream);

			//Creation of the NetworkRelay (Will spin up new thread)
			testNetworkMediator.createNewRelay(mockSocket);
			
			//Streams we're using in the test
			ObjectOutputStream testOutStream = new ObjectOutputStream(outputPipe);	//Write to this in test
			testOutStream.flush();
			ObjectInputStream testInStream = new ObjectInputStream(inputPipe);		//Read from this in test
			
			//Now we need to communicate with the NetworkRelay to complete the setup process
			requestMsg = (Message)testInStream.readObject();
			responseMsg = new Message(Message.Type.CLIENT_REGISTRATION_RESPONSE, testName);
			testOutStream.writeObject(responseMsg);
			confirmMsg  = (Message)testInStream.readObject();
			
			Mockito.verify(mockSocket).getInputStream();
			Mockito.verify(mockSocket).getOutputStream();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}

		TestUtilFns.wait(10); //Wait for NetworkRelay to finish
		
		//Message Asserts
		assertNotNull(requestMsg);
		assertNotNull(responseMsg);
		assertNotNull(confirmMsg);
		assertEquals(requestMsg.type, Message.Type.CLIENT_REGISTRATION_REQUEST);
		assertEquals(requestMsg.contents, null);
		assertEquals(confirmMsg.type, Message.Type.CLIENT_REGISTRATION_CONFIRMATION);
		assertNotEquals(confirmMsg.contents, null);
		
		//Data Integrity Asserts
		assertEquals(testNetworkMediator.getNumRelays(), 1);
		
		for (int uid : testNetworkMediator.getRelayUIDs()) //Should run only once
			assertEquals(testNetworkMediator.getNameFromUID(uid), testName);
		
		Mockito.verifyNoMoreInteractions(mockSocket);
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}

	@Test
	public void testAddAndRemoveRelays() {

		Set<Integer> expectedRelayUIDs = new HashSet<>();
		
		//Add arbitrary number of Relays in succession
		for (int i = 0; i < ServerConfig.MAX_PLAYER_NUM; ++i) {
			//Mock NetworkRelay
			NetworkRelay mockRelay = Mockito.mock(NetworkRelay.class);
			
			//Keep valid calls to UID getter/setter working
			Mockito.when(mockRelay.getUID()).thenCallRealMethod();
			Mockito.doCallRealMethod().when(mockRelay).setUID(Mockito.anyInt());
			
			//Add mock to Mediator and UID to tracking list
			testNetworkMediator.addRelay(mockRelay);
			expectedRelayUIDs.add(mockRelay.getUID());
		}
		
		//Assert the relays were added properly
		assertEquals(testNetworkMediator.getRelayUIDs(), expectedRelayUIDs);
		
		//Remove each Relay from Mediator list, checking along the way that it's being properly removed
		for (int UID : expectedRelayUIDs) {
			testNetworkMediator.removeRelay(UID);
			expectedRelayUIDs.remove(UID);
			assertEquals(testNetworkMediator.getRelayUIDs(), expectedRelayUIDs);
		}
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}

}
