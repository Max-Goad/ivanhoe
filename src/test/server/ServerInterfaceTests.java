/*
 * Name: 		ServerInterfaceTests.java
 * Created by:	Max Goad
 * Description:	Tests for the ServerInterface portions of IvanhoeServer
 * 				such as updating state, and prompting client for various things
 */

package test.server;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mockito.Mockito;

import server.IvanhoeServer;
import server.NetworkMediator;
import server.ServerConfig;
import server.message.Message;

public class ServerInterfaceTests {
	
	@Rule public TestName name = new TestName();
	private static Logger logger = Logger.getLogger(ServerInterfaceTests.class.getName());
	
	private IvanhoeServer testServer;
	
	@Before
	public void setUp() throws Exception {
		logger.info("Starting " + name.getMethodName() + "...");
	}

	@After
	public void tearDown() throws Exception {
		testServer.stop();
		testServer = null;
	}
	
	@Test
	public void testUpdateFunctions() {
		//Test values
		int testUID = 123456;
		Message testMessage = new Message(Message.Type.TEST_MESSAGE);
		
		//Mocking
		NetworkMediator mockMediator = Mockito.mock(NetworkMediator.class);
		Mockito.doNothing().when(mockMediator).broadcastMessage(Mockito.any());
		Mockito.doNothing().when(mockMediator).sendMessage(testUID, testMessage);
		
		//Initialize server
		testServer = new IvanhoeServer();
		testServer.initialize(100, ServerConfig.MIN_PLAYER_NUM, ServerConfig.MIN_AI_NUM);
		testServer.setNetworkMediator(mockMediator);
		testServer.setDoLogFlag(false);
		
		//Test function calls
		testServer.updateAllStates("TEST STATE");
		testServer.updateState(testUID, "TEST STATE");
		testServer.reportErrorToUser(testUID, "TEST ERROR MSG");
		
		Mockito.verify(mockMediator, Mockito.times(1)).setDoLogFlag(Mockito.anyBoolean());
		Mockito.verify(mockMediator, Mockito.times(1)).broadcastMessage(Mockito.any());
		Mockito.verify(mockMediator, Mockito.times(2)).sendMessage(Mockito.eq(testUID), Mockito.any());
		Mockito.verifyNoMoreInteractions(mockMediator);
		
		logger.info("Finished " + name.getMethodName() + " successfully!");			
	}
	
	@Test
	public void testRequestFunctions() {
		
		//Test values
		int testUID = 123456;
		String testString 		 = "TEST STRING";
		String testTAString 	 = "TEST TURN ACTION";
		Message testStringResponse		= new Message(Message.Type.GAME_STRING_RESPONSE, testString);
		Message testTurnActionResponse  = new Message(Message.Type.GAME_TURN_ACTION_RESPONSE, testTAString);
		
		//Mocking
		NetworkMediator mockMediator = Mockito.mock(NetworkMediator.class);
		Mockito.doNothing().when(mockMediator).broadcastMessage(Mockito.any());
		Mockito.doNothing().when(mockMediator).sendMessage(Mockito.eq(testUID), Mockito.any());
		Mockito.when(mockMediator.getResponseFrom(testUID, Message.Type.GAME_STRING_RESPONSE)).thenReturn(testStringResponse);
		Mockito.when(mockMediator.getResponseFrom(testUID, Message.Type.GAME_TURN_ACTION_RESPONSE)).thenReturn(testTurnActionResponse);

		//Initialize server
		testServer = new IvanhoeServer();
		testServer.initialize(200, ServerConfig.MIN_PLAYER_NUM, ServerConfig.MIN_AI_NUM);
		testServer.setNetworkMediator(mockMediator);
		testServer.setDoLogFlag(false);

		//Test function calls
		String stringReturn = testServer.promptUserForString(testUID, "TEST STRING PROMPT");
		String turnReturn   = testServer.getTurnAction(testUID);

		Mockito.verify(mockMediator, Mockito.times(1)).setDoLogFlag(Mockito.anyBoolean());
		Mockito.verify(mockMediator, Mockito.times(2)).sendMessage(Mockito.eq(testUID), Mockito.any());
		Mockito.verify(mockMediator, Mockito.times(1)).getResponseFrom(testUID, Message.Type.GAME_STRING_RESPONSE);
		Mockito.verify(mockMediator, Mockito.times(1)).getResponseFrom(testUID, Message.Type.GAME_TURN_ACTION_RESPONSE);
		
		Mockito.verifyNoMoreInteractions(mockMediator);
		
		assertEquals(stringReturn, testString);
		assertEquals(turnReturn, testTAString);
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
}
