/*
 * Name: 		ServerBasicTests.java
 * Created by:	Max Goad
 * Description:	Tests for basic IvanhoeServer functionality, such as startup and running.
 */

package test.server;

import static org.junit.Assert.*;

import java.io.*;
import java.net.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mockito.Mockito;

import server.IvanhoeServer;
import server.NetworkMediator;
import server.ServerConfig;
import util.TestUtilFns;

public class ServerBasicTests {
	
	@Rule public TestName name = new TestName();
	private static Logger logger = Logger.getLogger(ServerBasicTests.class.getName());
	
	private IvanhoeServer testServer;
	
	@Before
	public void setUp() throws Exception {
		logger.info("Starting " + name.getMethodName() + "...");
	}

	@After
	public void tearDown() throws Exception {
		testServer.stop();
		testServer = null;
	}
	
	@Test
	public void testStartup() {
		testServer = new IvanhoeServer();
		testServer.setDoLogFlag(false);
		testServer.start();
		
		//We need this wait block in order to allow the server thread time to start (shouldn't need more than 50 ms)
		TestUtilFns.wait(50);
		
		assertTrue(testServer.isRunning());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");
	}
	
	@Test
	public void testStartupWithArgs() {
		InputStream testInputStream = System.in;
		boolean testDoLog = false;
		
		testServer = new IvanhoeServer();
		testServer.setInputStream(testInputStream);
		testServer.setDoLogFlag(testDoLog);
		testServer.start();
		
		//We need this wait block in order to allow the server thread time to start (shouldn't need more than 50 ms)
		TestUtilFns.wait(50);
		
		assertTrue(testServer.isRunning());
		assertEquals(testInputStream, testServer.getInputStream());
		assertEquals(testDoLog, testServer.isLogging());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void testStartupInput() {
		int testPort = 11000;
		for (int testPlayerNum = (ServerConfig.MIN_PLAYER_NUM - 1); testPlayerNum <= (ServerConfig.MAX_PLAYER_NUM + 1); testPlayerNum++) {
			for (int testCompNum = (ServerConfig.MIN_AI_NUM - 1); testCompNum <= ServerConfig.MAX_PLAYER_NUM + 1; testCompNum++) {
				String testOptions = "start " + testPort + " " + testPlayerNum + " " + testCompNum + "\n";
				ByteArrayInputStream testInStream = new ByteArrayInputStream(testOptions.getBytes());
				
				testServer = new IvanhoeServer();
				testServer.setInputStream(testInStream);
				testServer.setDoLogFlag(false);
				testServer.start();
				
				//We need this wait block in order to allow the server thread time to start (shouldn't need more than 50 ms)
				TestUtilFns.wait(10);
				
				//Tests for incorrect player numbers
				if (testPlayerNum < ServerConfig.MIN_PLAYER_NUM || testPlayerNum > ServerConfig.MAX_PLAYER_NUM
				 || testCompNum < ServerConfig.MIN_AI_NUM || testCompNum > (ServerConfig.MAX_PLAYER_NUM - testPlayerNum)
				 || (testPlayerNum + testCompNum) < ServerConfig.MIN_TOTAL_NUM) {
					assertFalse(testServer.isRunning());
					assertNotEquals(testPlayerNum, testServer.getExpectedNumPlayers());
					assertNotEquals(testCompNum, testServer.getComputerPlayers());
				}
				//Tests for correct player numbers
				else {
					assertTrue(testServer.isRunning());
					assertEquals(testPort, testServer.getAssociatedPort());
					assertEquals(testPlayerNum, testServer.getExpectedNumPlayers());
					assertEquals(testCompNum, testServer.getComputerPlayers());
				}
				testPort++;
			}
			testPort++;
		}
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void testShutdown() {
		int testPort = 12000;
		String testOptions = "start " + testPort + " " + ServerConfig.MIN_TOTAL_NUM + " " + ServerConfig.MIN_AI_NUM +"\n";
		ByteArrayInputStream testInStream = new ByteArrayInputStream(testOptions.getBytes());
		testServer = new IvanhoeServer();
		testServer.setInputStream(testInStream);
		testServer.setDoLogFlag(false);
		testServer.start();
		
		//We need this wait block in order to allow the server thread time to start (shouldn't need more than 50 ms)
		TestUtilFns.wait(50);
		
		//First make sure server is running (and port isn't closed somehow)
		assertTrue(testServer.isRunning());
		assertFalse(testServer.isClosed());

		//Then call stop
		testServer.stop();
	
		//Check again to see if server still running
		assertFalse(testServer.isRunning());
		assertTrue(testServer.isClosed());
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}

	@Test
	public void testAcceptConnections() {
		int testExpectedNumPlayers = 1;
		int testPortNumber = ServerConfig.DEFAULT_PORT;
		String testHost = ServerConfig.LOCAL_HOST;

		//Mock out the NetworkMediator
		NetworkMediator mockMediator = Mockito.mock(NetworkMediator.class);
		Mockito.doNothing().when(mockMediator).createNewRelay(Mockito.any());
		Mockito.when(mockMediator.getNumRelays()).thenReturn(0);
		
		testServer = new IvanhoeServer();
		testServer.initialize(testPortNumber, testExpectedNumPlayers, ServerConfig.MIN_AI_NUM);
		testServer.setNetworkMediator(mockMediator);
		testServer.setDoLogFlag(false);
		
		//Start server thread
		testServer.start();
		
		try {
			Socket testSocket = new Socket(testHost, testPortNumber);
			testSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		Mockito.verify(mockMediator, Mockito.times(1)).setDoLogFlag(Mockito.anyBoolean());
		Mockito.verify(mockMediator, Mockito.times(testExpectedNumPlayers)).getNumRelays();
		Mockito.verify(mockMediator).createNewRelay(Mockito.any());
		
		Mockito.verifyNoMoreInteractions(mockMediator);

		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
}
