package test.ai;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import ai.AIFactory;
import ai.AIPlayer;
import ai.strategy.SimpleOnlyStrategy;
import cards.Card;
import cards.CardFactory;
import engine.GameState;
import engine.Player;
import test.client.ClientBasicTests;
import util.EngineCommandWriter;

public class SimpleOnlyStrategyTests {

	@Rule public TestName name = new TestName();
	private static Logger logger = Logger.getLogger(ClientBasicTests.class.getName());
	
	private static GameState testGameState;
	private static AIFactory aiFactory;
	private static CardFactory cardFactory;
	private static ArrayList<Card> testHand;
	
	private AIPlayer testAI;
	private Player testPlayer;
	
	@BeforeClass
	public static void setUpClass() throws Exception {
		testGameState = GameState.getInstance();
		aiFactory = new AIFactory();
		cardFactory = new CardFactory();
		testHand = new ArrayList<>();
	}
	
	@Before
	public void setUp() throws Exception {
		testGameState.resetGameState(true);
		testHand.clear();
		
		testAI = aiFactory.createAIPlayer(SimpleOnlyStrategy.class.getSimpleName());
		testPlayer = new Player(testAI.getName(), testAI.getUID());
		testGameState.addPlayer(testPlayer);
		logger.info("Starting " + name.getMethodName() + "...");
	}

	@After
	public void tearDown() throws Exception {
		testAI = null;
		testPlayer = null;
	}

	@Test
	public void testNoActiveTournament() {	
		testGameState.setTournamentColour("none");
		
		//NOTE: Unless otherwise explicitly "clear()"ed, testHand will not be empty at 
		//		the start of each of these "subtests". This is intentional.
		
		//Test: Null hand
		testPlayer.setHand(testHand);
		
		String result = testAI.getTurnAction();
		String expectedResult = EngineCommandWriter.endTurnCommand();
		assertEquals(expectedResult, result);
		
		//Test: No Simple Cards
		testHand.add(cardFactory.makeCard("dodge"));
		testHand.add(cardFactory.makeCard("disgrace"));
		testHand.add(cardFactory.makeCard("ivanhoe"));
		testPlayer.setHand(testHand);
		
		result = testAI.getTurnAction();
		expectedResult = EngineCommandWriter.endTurnCommand();
		assertEquals(expectedResult, result);
		
		//Test: Simple Cards
		Card expectedHighestCard = cardFactory.makeCard("purple 5");
		testHand.add(cardFactory.makeCard("red 3"));
		testHand.add(cardFactory.makeCard("red 3"));
		testHand.add(cardFactory.makeCard("blue 3"));
		testHand.add(cardFactory.makeCard("yellow 3"));
		testHand.add(cardFactory.makeCard("green 1"));
		testHand.add(expectedHighestCard);
		testPlayer.setHand(testHand);

		result = testAI.getTurnAction();
		expectedResult = EngineCommandWriter.playCardCommand(expectedHighestCard.getID());
		assertEquals(expectedResult, result);
		
		//Test: Supporter w/other cards
		expectedHighestCard = cardFactory.makeCard("white 6");
		testHand.add(cardFactory.makeCard("white 2"));
		testHand.add(cardFactory.makeCard("white 3"));
		testHand.add(expectedHighestCard);
		testPlayer.setHand(testHand);
		
		result = testAI.getTurnAction();
		expectedResult = EngineCommandWriter.playSupporterStartTournamentCommand(expectedHighestCard.getID(), "red");
		assertEquals(expectedResult, result);
		
		//Test: Supporters ONLY
		testHand.clear();
		expectedHighestCard = cardFactory.makeCard("white 6");
		testHand.add(cardFactory.makeCard("white 2"));
		testHand.add(cardFactory.makeCard("white 3"));
		testHand.add(expectedHighestCard);
		testPlayer.setHand(testHand);
		
		result = testAI.getTurnAction();
		expectedResult = EngineCommandWriter.playSupporterStartTournamentCommand(expectedHighestCard.getID(), "purple");
		assertEquals(expectedResult, result);
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}
	
	@Test
	public void testActiveTournament() {	
		testGameState.setTournamentColour("red");
		
		//NOTE: Unless otherwise explicitly "clear()"ed, testHand will not be empty at 
		//		the start of each of these "subtests". This is intentional.
		
		//Test: Null hand
		testPlayer.setHand(testHand);
		
		String result = testAI.getTurnAction();
		String expectedResult = EngineCommandWriter.endTurnCommand();
		assertEquals(expectedResult, result);
		
		//Test: No Simple Cards
		testHand.add(cardFactory.makeCard("dodge"));
		testHand.add(cardFactory.makeCard("disgrace"));
		testHand.add(cardFactory.makeCard("ivanhoe"));
		testPlayer.setHand(testHand);
		
		result = testAI.getTurnAction();
		expectedResult = EngineCommandWriter.endTurnCommand();
		assertEquals(expectedResult, result);
		
		//Test: Supporters
		Card expectedHighestCard = cardFactory.makeCard("white 3");
		testHand.add(cardFactory.makeCard("white 2"));
		testHand.add(expectedHighestCard);
		testPlayer.setHand(testHand);
		
		result = testAI.getTurnAction();
		expectedResult = EngineCommandWriter.playCardCommand(expectedHighestCard.getID());
		assertEquals(expectedResult, result);
		
		//Test: Simple Cards (No Matching Colour)
		testHand.add(cardFactory.makeCard("purple 5"));
		testHand.add(cardFactory.makeCard("blue 3"));
		testHand.add(cardFactory.makeCard("yellow 3"));
		testHand.add(cardFactory.makeCard("green 1"));
		testPlayer.setHand(testHand);

		result = testAI.getTurnAction();
		expectedResult = EngineCommandWriter.playCardCommand(expectedHighestCard.getID());
		assertEquals(expectedResult, result);
		
		//Test: Simple Cards (Matching Colour)
		expectedHighestCard = cardFactory.makeCard("red 4");
		testHand.add(cardFactory.makeCard("red 3"));
		testHand.add(expectedHighestCard);
		testPlayer.setHand(testHand);

		result = testAI.getTurnAction();
		expectedResult = EngineCommandWriter.playCardCommand(expectedHighestCard.getID());
		assertEquals(expectedResult, result);
		
		logger.info("Finished " + name.getMethodName() + " successfully!");	
	}

}
