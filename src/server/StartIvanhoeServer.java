package server;

import org.apache.log4j.Logger;

import util.TestUtilFns;

public class StartIvanhoeServer {
	
	private static Logger logger = Logger.getLogger(StartIvanhoeServer.class.getName());
	private static IvanhoeServer server = null;
	
	public static void main(String[] args) {
		//Initialize the server
		logger.info("Starting Ivanhoe Server...");
		server = new IvanhoeServer();
		
		//TODO: Remove next few lines if not testing!
		//server.initialize(12345, 1, 1);
		server.setDoLogFlag(true);

		server.start();
		
		//Wait a second for the server to start
		TestUtilFns.wait(1000);
		
		//Keep process running while server is running
		while (server.isRunning()) { TestUtilFns.wait(1000); }

		//Once server is no longer running, kill process
		logger.info("Exiting Ivanhoe Server...");
		System.exit(0);
	}
}
