/*
 * Name: 		Messenger.java
 * Created by:	Max Goad
 * Description:	Superclass meant to be extended upon for any class that wishes to send/receive messages
 */

package server.message;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

public class Messenger {

	private ObjectOutputStream outStream;
	private ObjectInputStream inStream;
	
	private static Logger logger = Logger.getLogger(Messenger.class.getName());
	
	public Messenger(){
	}
	
	public boolean initializeStreams(Socket s) {
		try {
			outStream = new ObjectOutputStream(s.getOutputStream());
			outStream.flush();
			inStream = new ObjectInputStream(s.getInputStream());
			return true;
		} catch (IOException e) {
			logger.error("initializeStreams ioe - " + e.getMessage());
			return false;
		}
	}
	
	public boolean sendMessage(Message msg) {		
		if (outStream == null) {
			logger.error("OutStream not initialized!");
			return false;
		}
		
		try {
			outStream.writeObject(msg);
			return true;
		} catch (IOException e) {
			logger.error("sendMessage ioe - " + e.getMessage());
			return false;
		}
	}
	
	public Message waitForMessage(Message.Type expectedType) {
		Set<Message.Type> typeSet = new HashSet<>();
		typeSet.add(expectedType);
		return waitForMessage(typeSet);
	}
	
	public Message waitForMessage(Set<Message.Type> expectedTypes) {
		if (inStream == null) {
			logger.error("InStream not initialized!");
			return null;
		}
		
		if (expectedTypes.isEmpty()) {
			logger.error("Expected type set is empty!");
			return null;
		}
		
		Message receivedMsg = null;
		
		try {
			boolean receiveSuccess = false;
			while (!receiveSuccess) {
				receivedMsg = (Message)inStream.readObject();
				
				//We return either correct type messages OR error messages
				if (expectedTypes.contains(receivedMsg.type) || receivedMsg.type.isError) {
					receiveSuccess = true;
				} 
				//We reject messages that do not match what we are expecting
				//But are not error messages
				else {
					String expectedTypeString = "Expected:";
					for (Message.Type type : expectedTypes) {
						expectedTypeString += " ";
						expectedTypeString += type.toString();
					}
					
					Message incorrectTypeMsg = new Message(Message.Type.ERROR_INCORRECT_MSG_SENT, expectedTypeString);
					sendMessage(incorrectTypeMsg);
				}
			}
		} catch (SocketException e) {
			receivedMsg = null;
			logger.error("waitForMessage encountered SocketException... shutting down!");
		} catch (Exception e) { //IOException or ClassNotFoundException
			receivedMsg = null;
			logger.error("waitForMessage - " + e.getMessage());
		}
		
		return receivedMsg;
	}

	protected boolean isValidMessage(Message msg) {
		return this.isValidMessage(msg, false);
	}
	
	protected boolean isValidMessage(Message msg, boolean checkContents) {
		if (msg == null) {
			logger.error("Message to be validated was null!");
			return false;
		}
		else if (msg.type.isError) {
			logger.error("Message to be validated is of type " + msg.type);
			return false;
		}
		else if (checkContents && msg.contents == null) {
			logger.error("Message to be validated expected contents, found null!");
			return false;
		}
		return true;
		
	}
	
	protected boolean isOutStreamValid() {
		return outStream != null;
	}
	
	protected boolean isInStreamValid() {
		return inStream != null;
	}
}
