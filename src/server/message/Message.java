/*
 * Name: 		Message.java
 * Created by:	Max Goad
 * Description:	Generic client-server message to be sent via Messengers
 */

package server.message;

import java.io.Serializable;

public class Message implements Serializable {

	private static final long serialVersionUID = 6829563192222292837L;

	///////////////////////
	//Type enum declaration
	///////////////////////
	public enum Type {
		//NetworkRelay setup messages
		CLIENT_REGISTRATION_REQUEST,		//Server -> Client
		CLIENT_REGISTRATION_RESPONSE,		//Client -> Server; Player info
		CLIENT_REGISTRATION_CONFIRMATION,	//Server -> Client
		CLIENT_FINISHED_PLAYING,			//Client -> Server
		CLIENT_CONTINUE_PLAYING,			//Client -> Server
		//Game messages
		GAME_START,					//Server -> Client
		GAME_UPDATE_STATE,			//Server -> Client; Full state info
		GAME_USER_ERROR,			//Server -> Client;
		GAME_STRING_REQUEST,		//Server -> Client
		GAME_STRING_RESPONSE,		//Client -> Server
		GAME_TURN_ACTION_REQUEST,	//Server -> Client
		GAME_TURN_ACTION_RESPONSE,	//Client -> Server; Playing cards, ending turn, withdrawing
		GAME_IVANHOE_REQUEST,		//Server -> Client
		GAME_IVANHOE_RESPONSE,		//Client -> Server; Yes/No
		GAME_END,					//Server -> Client
		//Test messages
		TEST_MESSAGE,
		TEST_ERROR(true),
		//Error messages (always set to true)
		ERROR_INCORRECT_MSG_SENT(true),
		ERROR_RETRY_INPUT(true),
		ERROR_UNEXPECTED(true);
		
		public boolean isError;
		
		Type() {
			isError = false;
		}
		
		Type(boolean isError) {
			this.isError = isError;
		}
	}
	
	public Type type;
	public String contents;
	
	public Message(Type type) {
		this.type = type;
		this.contents = null;
	}
	
	public Message(Type type, String contents) {
		this.type = type;
		this.contents = contents;
	}
}
