/*
 * Name: 		IvanhoeServer.java
 * Created by:	Max Goad
 * Description:	The main server class for the Ivanhoe game.
 */

package server;

import java.io.*;
import java.net.*;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.apache.log4j.Logger;

import ai.AIMediator;
import engine.Engine;
import engine.EngineInterface;
import server.message.Message;

public class IvanhoeServer implements Runnable, ServerInterface {

	private static Logger logger = Logger.getLogger(IvanhoeServer.class.getName());
	
	private Thread thread = null;		//Server main thread
	private ServerSocket server = null; //Server main socket
	
	private NetworkMediator networkMediator = null; //Mediator for client communications
	private AIMediator aiMediator = null;			//Mediator for AI communications
	private EngineInterface gameEngine = null;  //Game Engine for running game control flow

	private InputStream inputStream;	//Stream for inputting settings/commands
	private int expectedNumPlayers;		//Number of players needed to start game
	private int aiPlayerNum;			//Number of ai players
	private boolean doLog;				//Flag for logging

	//////////////
	//Constructors
	//////////////
	public IvanhoeServer() {
		this.networkMediator = new NetworkMediator(this);
		this.aiMediator = new AIMediator();
		
		this.inputStream = ServerConfig.DEFAULT_INPUT_STREAM;
		this.expectedNumPlayers = -1;
		this.aiPlayerNum = -2;
		this.setDoLogFlag(doLog);
		
		thread = new Thread(this);
	}
	
	/////////////////
	//Main thread fns
	/////////////////
	public void start() {
		thread.start();
	}
	
	public void run() {

		initializeViaInStream();
		
		while (this.isRunning()) {			
			waitForSocketConnections();
			
			if (!this.isRunning()) 
				return;
			
			engineStartup();
			
			sendClientStartupMessages();
			
			String winningPlayer = gameEngine.gameStart(); //Pass control to engine; Will pass back control once game is over
			
			endGame(winningPlayer);
		}
	}
	
 	public void stop() {
		//Stop thread running
		thread = null;
		
		//Close server
		if (!isClosed()) {
			try {
			server.close();
			} catch (Exception e) {
				logger.error("Error stopping Ivanhoe server!", e);
			}
		}
	}
 	
	//////////////////
	//Server logic fns
	//////////////////
	private void initializeViaInStream() {

		while (!this.isInitialized()) {

			Scanner inputScanner = new Scanner(inputStream);
			
			if (inputStream != System.in && !inputScanner.hasNextLine()) {
				if (doLog)
					logger.error("Error! Detected no new line from input stream!");
				this.stop();
				break;
			}

			//Get start command from input stream
			if (doLog) {
				logger.info("Please enter \"start\" followed by port number, the number of players you wish to support and finally the number of AI players! (Game supports 2-5 human and/or AI players)");
				logger.info("Example: \"start 5050 2 1\"");
			}
			String rawcmd = inputScanner.nextLine();
			
			//Check array integrity
			if (rawcmd == null) {
				if (doLog)
					logger.warn("Unknown Error! Command string parsed to be null!");
				continue;
			}
			
			//Split string into args
			String[] cmd = rawcmd.split(" ");
			
			//Check start command
			if (!cmd[0].equalsIgnoreCase("start")) {
				if (doLog)
					logger.warn("Error! Command must begin with the word \"start\"!");
				continue;
			}
			
			//Check length of command
			if ((cmd.length - 1) != ServerConfig.EXPECTED_STARTUP_ARGS) {
				if (doLog)
					logger.warn("Error! Incorrect number of arguments input to start command! Expected: " + ServerConfig.EXPECTED_STARTUP_ARGS);
				continue;
			}
			
			//Check validity of args
			
			//Port Num
			int inputPortNum;
			try {
				inputPortNum = Integer.parseInt(cmd[1]);
			} catch (NumberFormatException e) {
				if (doLog)
					logger.warn("Error! Could not parse port number from string provided: " + cmd[1]);
				continue;
			}
			
			if (inputPortNum < ServerConfig.MIN_PORT_NUM || inputPortNum > ServerConfig.MAX_PORT_NUM) {
				if (doLog)
					logger.warn("Error! Incorrect port number specified! Expected: Between " + ServerConfig.MIN_PORT_NUM + " and " + ServerConfig.MAX_PORT_NUM + "!");
				continue;
			}
			
			//Player Num
			int inputPlayerNum;
			try { 
				inputPlayerNum = Integer.parseInt(cmd[2]);
			} catch (NumberFormatException e) {
				if (doLog)
					logger.warn("Error! Could not parse number of players from string provided: " + cmd[2]);
				continue;
			}
			
			if (inputPlayerNum < ServerConfig.MIN_PLAYER_NUM || inputPlayerNum > ServerConfig.MAX_PLAYER_NUM) {
				if (doLog)
					logger.warn("Error! Incorrect number of players specified! Expected: Between " + ServerConfig.MIN_PLAYER_NUM + " and " + ServerConfig.MAX_PLAYER_NUM + "!");
				continue;
			}
			
			//AI Player Num
			int inputAINum;
			int aiMax = ServerConfig.MAX_PLAYER_NUM - inputPlayerNum;
			
			try {
				inputAINum = Integer.parseInt(cmd[3]);
			} catch (NumberFormatException e) {
				if (doLog)
					logger.warn("Error! Could not parse number of AI players from string provided: " + cmd[3]);
				continue;
			}
			
			if (inputAINum < ServerConfig.MIN_AI_NUM || inputAINum > aiMax) {
				if (doLog)
					logger.warn("Error! Incorrect number of AI players specified! Expected: Between " + ServerConfig.MIN_AI_NUM + " and " + aiMax + "!");
				continue;
			}
			
			if (inputPlayerNum + inputAINum < ServerConfig.MIN_TOTAL_NUM) {
				if (doLog)
					logger.warn("Error! Need at least " + ServerConfig.MIN_TOTAL_NUM +  " human/ai players total!");
				continue;
			}
			
			
			//All args are valid! Assign values and end loop
			this.initialize(inputPortNum, inputPlayerNum, inputAINum);
			
			inputScanner.close();
		}

	}
	
	private void waitForSocketConnections() {
		
		if (doLog) {
			logger.info("Waiting for clients to connect...");
		}
		
		while (this.isRunning() && !this.isClosed() && networkMediator.getNumRelays() != this.getExpectedNumPlayers()) {
			
			Socket newClient = null;
			
			try {
				newClient = server.accept();
			} catch (SocketException e) {
				this.stop();
				return;
			} catch (IOException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				continue;
			}
			
			if (newClient != null) {
				if (doLog) {
					logger.info("Client connection request detected! Verifying...");
				}
				networkMediator.createNewRelay(newClient);
				
				try { Thread.sleep(1000); } catch (Exception e) { break; }
			}
		}
	}
	
	private void engineStartup() {
		//Init engine if not already init'd
		if (gameEngine == null) {
			gameEngine = new Engine(this); //TODO: Make less hardcoded?
		}

		gameEngine.resetGameState();
		
		//Register players from list in NetworkMediator
		Set<Integer> playerUIDs = networkMediator.getRelayUIDs();
		for (int UID : playerUIDs) {
			gameEngine.addPlayer(UID, networkMediator.getNameFromUID(UID));
			
			if (doLog) {
				System.out.println("Registered human player \"" + networkMediator.getNameFromUID(UID) + "\" with UID " + UID);
			}
		}
		
		//Register any AI players as well
		Set<Integer> aiUIDs = aiMediator.getUIDs();
		for (int UID : aiUIDs) {
			gameEngine.addPlayer(UID, aiMediator.getNameFromUID(UID));
			
			if (doLog) {
				System.out.println("Registered AI player \"" + aiMediator.getNameFromUID(UID) + "\" with UID " + UID);
			}
		}
	}
	
	private void sendClientStartupMessages() {
		if (this.isRunning()) {
			Message startMsg = new Message(Message.Type.GAME_START);
			networkMediator.broadcastMessage(startMsg);
		}
	}
	
	private void endGame(String winningPlayer) {
		logger.info("Game has ended! The winner is: " + winningPlayer);
		
		//Check with players to see if they want to keep playing
		Message gameEndMsg = new Message (Message.Type.GAME_END, winningPlayer);
		networkMediator.broadcastMessage(gameEndMsg);

		Set<Integer> playersLeaving = new HashSet<>();
		Set<Message.Type> expectedTypes = new HashSet<>();
		expectedTypes.add(Message.Type.CLIENT_CONTINUE_PLAYING);
		expectedTypes.add(Message.Type.CLIENT_FINISHED_PLAYING);
		
		//Prompt user for play again response
		Set<Integer> playerUIDs = networkMediator.getRelayUIDs();
		for (int UID : playerUIDs) {
			Message response = networkMediator.getResponseFrom(UID, expectedTypes);
			if (response == null || response.type != Message.Type.CLIENT_CONTINUE_PLAYING) {
				playersLeaving.add(UID);
			}
			
			//While we're here, remove all players from gameEngine 
			//(we'll add them back at beginning of loop if they want to play again)
			gameEngine.removePlayer(UID);
		}
		
		//Remove all players that do not want to play again from NetworkMediator
		for (int UID : playersLeaving) {
			networkMediator.removeRelay(UID);
		}
		
		//If no players are playing anymore, stop the server!
		if (networkMediator.getNumRelays() == 0) {
			this.stop();
		}
		
		//Don't forget to remove all AI players too
		for (int UID : aiMediator.getUIDs()) {
			gameEngine.removePlayer(UID);
		}
	}
	
	public void reportRelayFailure(int UID) {
		gameEngine.removePlayer(UID);
	}
 	/////////////////////////
 	//ClientUserInterface fns
 	/////////////////////////
 	@Override
	public void updateAllStates(String state) {
 		Message msg = new Message(Message.Type.GAME_UPDATE_STATE, state);
		networkMediator.broadcastMessage(msg);
	}

	@Override
	public void updateState(int UID, String state) {
 		if (aiMediator.isAIPlayer(UID)) {
 			return;
 		}
 		
 		Message msg = new Message(Message.Type.GAME_UPDATE_STATE, state);
		networkMediator.sendMessage(UID, msg);
	}
	
	@Override
	public void reportErrorToUser(int UID, String errorMsg) {
		if (aiMediator.isAIPlayer(UID)) {
			aiMediator.reportError(UID);
		} else {
			Message msg = new Message(Message.Type.GAME_USER_ERROR, errorMsg);
			networkMediator.sendMessage(UID, msg);
		}
	}

	@Override
	public String promptUserForString(int UID, String prompt) {
		if (aiMediator.isAIPlayer(UID)) {
			if (doLog) {
				logger.warn("Warning! Attempted to prompt string from AI Player! Returning null...");
			}
			return null;
		}
		else {
			Message request = new Message(Message.Type.GAME_STRING_REQUEST, prompt);
			networkMediator.sendMessage(UID, request);
			
			Message response = networkMediator.getResponseFrom(UID, Message.Type.GAME_STRING_RESPONSE);
			if (response != null && response.type == Message.Type.GAME_STRING_RESPONSE) {
				return response.contents;
			}
			else {
				return null;
			}
		}
	}

	@Override
	public String getTurnAction(int UID) {
		if (aiMediator.isAIPlayer(UID)) {
			return aiMediator.getTurnAction(UID);
		}
		else {
			//logger.warn("GETTING TURN ACTION!");
			
			Message request = new Message(Message.Type.GAME_TURN_ACTION_REQUEST);
			networkMediator.sendMessage(UID, request);
			
			//logger.warn("SENT REQUEST MESSAGE!");
			
			Message response = networkMediator.getResponseFrom(UID, Message.Type.GAME_TURN_ACTION_RESPONSE);
			if (response != null && response.type == Message.Type.GAME_TURN_ACTION_RESPONSE) {
				//logger.warn("RECEIVED RESPONSE MESSAGE! " + response.contents);
				return response.contents;
			}
			else {
				//logger.warn("ERROR! DIDN'T RECEIVE RESPONSE MESSAGE!");
				return null;
			}
		}
	}
 	
 	///////////////////////////////////////////
 	//Getters, Setters, Other state-related fns
 	///////////////////////////////////////////
	
	/*
	 * Getters
	 */
	public int getAssociatedPort() {
		if (server == null || !server.isBound())
			return -1;
		
		return server.getLocalPort();
	}
	
	public int getExpectedNumPlayers() {
		return this.expectedNumPlayers;
	}
	
	public int getComputerPlayers() {
		return this.aiPlayerNum;
	}
 	
	public InputStream getInputStream() {
		return this.inputStream;
	}
	
	public boolean isRunning() {
		if (thread == null)
			return false;
		
		return thread.isAlive();
	}
	
	public boolean isClosed() {
		if (server == null)
			return true;
		
		return server.isClosed();
	}
	
	public boolean isInitialized() {
		return (this.server != null)
			&& (this.expectedNumPlayers != -1);
	}
	
	public boolean isLogging() {
		return doLog;
	}
	
	/*
	 * Setters
	 */
	public void setInputStream(InputStream is) {
		this.inputStream = is;
	}
	
	public void setNetworkMediator(NetworkMediator mediator) {
		this.networkMediator = mediator;
	}
	
	public void setAIMediator(AIMediator mediator) {
		this.aiMediator = mediator;
	}
	
	public void setDoLogFlag(boolean doLog) {
		this.doLog = doLog;
		networkMediator.setDoLogFlag(doLog);
		aiMediator.setDoLogFlag(doLog);
	}
	
	public void initialize(int portNumber, int expectedNumPlayers, int computerPlayers) {
		ServerSocket newSocket = null;
		try {
			newSocket = new ServerSocket(portNumber);
			newSocket.setReuseAddress(true);
		} catch (Exception e) {
			logger.error("Error initializing Ivanhoe server!", e);
			this.stop();
			return;
		}
		
		this.initialize(newSocket, expectedNumPlayers, computerPlayers);
	}
	
	public void initialize(ServerSocket server, int expectedNumPlayers, int computerPlayers) {
		this.server = server;
		this.expectedNumPlayers = expectedNumPlayers;
		this.aiPlayerNum = computerPlayers;
		
		if (aiPlayerNum > 0) {
			aiMediator.initializeAIPlayers(aiPlayerNum);
		}
	}
	
}
