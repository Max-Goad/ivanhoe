package server;

import java.io.InputStream;

public class ServerConfig {
	public static final InputStream DEFAULT_INPUT_STREAM = System.in;
	public static final String LOCAL_HOST = "127.168.0.1";
	
	public static final int MIN_PLAYER_NUM = 1;
	public static final int MAX_PLAYER_NUM = 5;
	public static final int MIN_AI_NUM = 0;
	public static final int MIN_TOTAL_NUM = 2;
	
	public static final int DEFAULT_PORT = 5050;
	public static final int MIN_PORT_NUM = 256;
	public static final int MAX_PORT_NUM = 65535;
	
	public static final int EXPECTED_STARTUP_ARGS = 3;
}
