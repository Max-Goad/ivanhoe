/*
 * Name: 		NetworkFacade.java
 * Created by:	Max Goad
 * Description:	Facade class for NetworkRelays. Creates and manages NetworkRelays.
 */

package server;

import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import server.message.Message;
import util.UIDGenerator;

public class NetworkMediator {
	private static Logger logger = Logger.getLogger(NetworkMediator.class.getName());
	
	private IvanhoeServer server;
	
	private Map<Integer, NetworkRelay> relayMap = null;
	private boolean doLog;
	
	//////////////
	//Constructors
	//////////////
	public NetworkMediator(IvanhoeServer server) {
		relayMap = new HashMap<>();
		this.server = server;
	}
	
	///////////////////
	//Main Mediator Fns
	///////////////////
	public void sendMessage(int UID, Message msg) {
		if (relayMap.get(UID).sendMessage(msg) == false) {
			this.reportRelayFailure(UID, "sendMessage failed!");
		}
	}
	
	public void broadcastMessage(Message msg) {
		for(int UID : getRelayUIDs()) {
			sendMessage(UID, msg);
		}
	}
	
	public Message getResponseFrom(int UID, Message.Type expectedType) {
		Set<Message.Type> expectedTypes = new HashSet<>();
		expectedTypes.add(expectedType);
		return this.getResponseFrom(UID, expectedTypes);
	}
	
	public Message getResponseFrom(int UID, Set<Message.Type> expectedTypes) {
		Message receivedMessage = relayMap.get(UID).waitForMessage(expectedTypes);
		
		if (receivedMessage == null) {
			this.reportRelayFailure(UID, "waitForMessage failed!");
		}
		
		return receivedMessage;
	}
	
	///////////
	//Relay Fns
	///////////
	public void createNewRelay(Socket clientSocket) {
		int newUID = UIDGenerator.generate();
		//logger.info("New ID generated! " + newUID);
		NetworkRelay newRelay = new NetworkRelay(this, clientSocket, newUID);
		newRelay.start();
	}
	
	public synchronized void addRelay(NetworkRelay relay) {
		relayMap.put(relay.getUID(), relay);
		
		if (doLog) {
			logger.info("Successfully added client \"" + relay.getClientName() + "\"!");
		}
	}
	
	public synchronized boolean removeRelay(int UID) {
		if (relayMap.containsKey(UID)) {
			relayMap.remove(UID);
			return true;
		}
		else {
			//Key wasn't present in map... something went wrong.
			//One potential cause of this is that the relay hasn't been added yet
			//And failed during initialization
			return false;
		}
	}
	
	public synchronized void reportRelayFailure(int UID, String failureMessage) {
		logger.error("Relay with UID " + UID + " failed with message \"" + failureMessage + "\"");
		removeRelay(UID);
		server.reportRelayFailure(UID);
	}
 	
	///////////////////////////////////////////
	//Getters, Setters, Other state-related fns
	///////////////////////////////////////////
	public Set<Integer> getRelayUIDs() {
		return relayMap.keySet();
	}

	public int getNumRelays() {
		return this.relayMap.size();
	}
	
	public NetworkRelay getRelay(int UID) {
		return relayMap.get(UID);
	}
	
	public String getNameFromUID(int UID) {
		NetworkRelay matchingRelay = getRelay(UID);
		if (matchingRelay != null) {
			return matchingRelay.getClientName();
		}
		else {
			//Log error?
			return null;
		}
	}
	
	public void setDoLogFlag(boolean doLog) {
		this.doLog = doLog;
	}
}
