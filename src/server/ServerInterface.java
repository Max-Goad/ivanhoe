/*
 * Name: 		ServerRequestInterface.java
 * Created by:	Max Goad
 * Description:	Interface for servers to inherit if they wish to connect with Game Engines
 */

package server;

public interface ServerInterface {
	
	//Push fns (Will not wait for response)
	public void updateAllStates(String state);
	public void updateState(int UID, String state);
	public void reportErrorToUser(int UID, String errorMessage);
	
	//Pull fns (Blocking; Waits until response received)
	public String promptUserForString(int UID, String prompt);
	public String getTurnAction(int UID);
}
