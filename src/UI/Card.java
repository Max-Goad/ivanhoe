package UI;
/*
 * Name: 		Card.java
 * Created by:	Peter Zumbach
 * Description:	Stores all Image state data related to the card JLabels as
 *  well some implementations to keep track of ownership,if shown to the 
 *  particular user, or if discarded, etc.
 */

import javax.swing.ImageIcon;
import javax.swing.JLabel;
public class Card extends JLabel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int cardID,positionx,positiony;
	private boolean isItUp = false;
	private ImageIcon faceUp;
	private ImageIcon faceDown; 
	private boolean isItSpecial = false;

	/*
	 * Card Constructor
	 * */
	Card(int passedCardID,ImageRef tempIR){
			//Position and cardID setup
		cardID = passedCardID;
		setPositionx(tempIR.deckx);
		setPositiony(tempIR.decky);
		
			//Image and settings setup
		faceDown = new ImageIcon(this.getClass().getResource(tempIR.b0));
		setIcon(faceDown);	
		setBounds(0, 0, tempIR.cardx, tempIR.cardy);
		setVisible(true);
	}
	/*
	 * Flips Icon of the card when it is being flipped
	 * */
	public boolean flipCard(){
		if(isItUp){ // If the card is facing up
			setIcon(faceDown);
			return(isItUp = false);
		}else{ //If the card is facing down
			setIcon(faceUp);
			return(isItUp = true);
		}
	}
		/*
		 * 
		 * GETTER'S AND SETTER'S 
		 * 
		 * */
	public void setCardIcon(ImageIcon cardIcon){
		this.faceUp = cardIcon;
	}
	public int getPositionx() {
		return positionx;
	}
	public void setPositionx(int positionx) {
		this.positionx = positionx;
	}
	public int getPositiony() {
		return positiony;
	}
	public void setPositiony(int positiony) {
		this.positiony = positiony;
	}
	public int getCardID(){
		return cardID;
	}
	public boolean getisItUp(){
		return isItUp;
	}
	public boolean getisItSpecial() {
		return isItSpecial;
	}
	public void setisItSpecial(boolean isItSpecial) {
		this.isItSpecial = isItSpecial;
	}

}
