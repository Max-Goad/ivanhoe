package UI;
/*
 * Name: 		UIMain.java
 * Created by:	Peter Zumbach
 * Description:	Stores ALL information related to the UI and manages all objects used in the JFrame
 */

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import java.awt.Font;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.awt.Color;

public class UIMain extends JFrame implements MouseListener,ActionListener {
	/**
	 * Automatically generated serialVersionUID to get rid of class warning
	 */
	private static final long serialVersionUID = -4538028296795149035L;

	//Initializes Reference Object
	private ImageRef IR = new ImageRef();
	private ImageIcon largeImage[];
		//Player info/hands
	private int maxPnum = 5,pnum =0,
				cardNumberTemp=-1,
				tempID;
	private int cardDirection;
	private ArrayList<Integer> specialCards = new ArrayList<Integer>();
	private PlayersHandandPlay hands[];
	private PlayersHandandPlay plays[];
	private Card cards[];
	private Token Tokens[];
		//Turn phases
	private boolean myTurnDraw = false;
	private boolean myTurnPlay = false;
	private boolean myTurnWithdraw = false;
		//Setting up deck, discard, Overlay(Largecard) and Content Pane
	private DeckandDiscard deck = new DeckandDiscard(IR.deckx,IR.decky);
	private DeckandDiscard discard = new DeckandDiscard(IR.discardx,IR.discardy);
	private JLayeredPane LayeredPane;
	private JLabel largeCard;
	private JLabel tournamentDisplay = new JLabel("Tournament Color: White",SwingConstants.CENTER);
	private JLabel BackgroundLabel;
	private JLabel playerNameDisplay[];
	private JButton EndTurnLabel = new JButton("End Turn/Withdraw");
		// DATA SENDING BACK
	private String cardPlay = null;
		// HASHMAP FOR PLAYER IDS TO SERVER IDS
	private Map<Integer, Integer> playerIDMap = null;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIMain frame = new UIMain();
					frame.setResizable(false);
						// CODE FOR FUTURE FEATUER (FULL SCREEN)
						//frame.setUndecorated(true);
						//frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
						//frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UIMain() {
			/*
			 * 
			 * WINDOW SETTINGS AND LAYOUT MANAGER
			 * 
			 * */
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Ivanhoe");
		setBounds(0, 0, IR.windowx, IR.windowy);
		LayeredPane = new JLayeredPane();
		LayeredPane.setLayout(null);
		setSize(IR.windowx+5, IR.windowy+29);
		setResizable(false);
			/*
			 * 
			 * BACKGROUND
			 * 
			 * */
		BackgroundLabel = new JLabel("");
		BackgroundLabel.setBounds(0, 0, IR.windowx, IR.windowy);
		BackgroundLabel.setIcon(new ImageIcon(this.getClass().getResource(IR.background)));
		BackgroundLabel.setVisible(true);
		LayeredPane.add(BackgroundLabel,1,0);
			/*
			 * 
			 * TOURNAMENT DISPLAY
			 * 
			 * */
		tournamentDisplay.setBounds(0, 0, IR.playx, IR.playy);
		tournamentDisplay.setLocation(IR.displayx, IR.displayy);
		tournamentDisplay.setOpaque(true);
		tournamentDisplay.setFont(new Font("Serif", Font.PLAIN, 14));
		tournamentDisplay.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.BLACK));
		tournamentDisplay.setVisible(true);
		LayeredPane.add(tournamentDisplay,2,0);
		
			/*
			 * 
			 * EndTurn DISPLAY
			 * 
			 * */
		EndTurnLabel.setBounds(0, 0, IR.playx, IR.playy);
		EndTurnLabel.setLocation(IR.windowx-IR.playx, IR.windowy-IR.playy);
		EndTurnLabel.setOpaque(true);
		EndTurnLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		EndTurnLabel.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.BLACK));
		EndTurnLabel.setVisible(true);
		EndTurnLabel.addActionListener(this);
		LayeredPane.add(EndTurnLabel,2,0);

			/*
			 * 
			 * TOKEN SETUP
			 * 
			 * */
		int color = 0;
		Tokens = new Token[(maxPnum*5)];
		for(int i = 0; i<(maxPnum*5); i++){
			if(i%5 ==0){color++;}
			Tokens[i] = new Token(i,IR,color);
			LayeredPane.add(Tokens[i],IR.tokenlayer,0);
			discard.addtoken(Tokens[i]);
		}
			/*
			 * 
			 * OVERLAY CARD
			 * 
			 * */
		largeCard = new JLabel("");
		largeCard.setBounds(0, 0, IR.largecardx, IR.largecardy);
		largeCard.setLocation(0, 0);
		largeCard.setVisible(false);
		LayeredPane.add(largeCard,IR.top,0);
		LayeredPane.moveToFront(largeCard);
			/*
			 * 
			 * HANDS SETUP
			 * 
			 * */

		hands = new PlayersHandandPlay[maxPnum];
		plays = new PlayersHandandPlay[maxPnum];
		cardDirection = 0;
		for(int r = 0;r<hands.length;r++){
			if(r == 1){cardDirection = 1;}
			else if(r == 4) {cardDirection = 2;}
			else{cardDirection = 0;}
			hands[r] = new PlayersHandandPlay(IR.Phandx[r],IR.Phandy[r],r,cardDirection);
			if(cardDirection >0){
				hands[r].setBounds(IR.Phandx[r], IR.Phandy[r], IR.handy, IR.handx);
			}else{
				hands[r].setBounds(IR.Phandx[r], IR.Phandy[r], IR.handx, IR.handy);
			}
			hands[r].setOpaque(false);
			hands[r].setBorder(BorderFactory.createMatteBorder(
                    1, 1, 1, 1, IR.getPlayColor(r)));
			plays[r] = new PlayersHandandPlay(IR.Pplayx[r]+75,IR.Pplayy[r],r,cardDirection);
			plays[r].setBounds(IR.Pplayx[r], IR.Pplayy[r], IR.playx, IR.playy);
			plays[r].setOpaque(false);
			plays[r].setBorder(BorderFactory.createMatteBorder(
                    1, 1, 1, 1, IR.getPlayColor(r)));
			LayeredPane.add(hands[r],IR.middle,0);
			LayeredPane.add(plays[r],IR.middle,0);
			
		}
		setContentPane(LayeredPane);
			//Player ID map
		playerIDMap = new HashMap<>();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(myTurnPlay){
			cardPlay = "Withdraw";
		}
	}
	/*
	 * 
	 * MOUSE LISTENER
	 * 
	 * */

	public void mouseClicked(MouseEvent e) {
		tempID = ((Card) e.getSource()).getCardID();
		if(myTurnDraw){				//Drawing phase clicking
			if(deck.contains(tempID)){
				deck.removecard(cardNumberTemp);
				hands[pnum].addCard(cards[cardNumberTemp]);
				cards[cardNumberTemp].flipCard();
				cardNumberTemp = -1;
				handreorder(pnum);
				endmyturndraw();
				return;
			}
		}else if(myTurnPlay){		//Drawing phase clicking
			if(hands[pnum].contains(tempID)){
				cardPlay = "play "+tempID;	// SETS STRING FOR SERVER TO RETRIEVE 
				endmyturnplay();
				}
		}else if(myTurnWithdraw){	//Drawing phase clicking
			endmyturnwithdraw();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if(((Card) e.getSource()).getisItUp()){
			largeCard.setIcon(largeImage[((Card) e.getSource()).getCardID()]);
		}else{
			largeCard.setIcon(largeImage[IR.cardcount]);
		}
		int positionx = (int) ((Card) e.getSource()).getLocationOnScreen().getX();
		int positiony = (int) ((Card) e.getSource()).getLocationOnScreen().getY();
		if(positionx <IR.windowx-IR.largecardx && positiony < IR.windowy-IR.largecardy){ //Checks if not in the BOTTOM RIGHT
				//FAR BOTTOM RIGHT
			largeCard.setLocation(IR.windowx-IR.largecardx,IR.windowy-IR.largecardy);
		}else{
				//FAR TOP LEFT
			largeCard.setLocation(IR.windowx-IR.largecardx,0);
		}
		largeCard.setVisible(true);
	}
	@Override
	public void mouseExited(MouseEvent e) {
		largeCard.setVisible(false);
	}
	@Override
	public void mousePressed(MouseEvent e) {		
	}
	@Override
	public void mouseReleased(MouseEvent e) {		
	}

		/*
		 * 
		 * TURN LIMITERS/ TURN BOOLEANS
		 * 
		 * */
	public void startmyturndraw(){
		displayturn();
		myTurnDraw = true;
	}	
	public void endmyturndraw(){
		myTurnDraw = false;
	}
	public void startmyturnplay(){
		myTurnPlay = true;
	}
	public void endmyturnplay(){
		myTurnPlay = false;
	}
	public void startmyturnwithdraw(){
		myTurnWithdraw = true;
	}
	public void endmyturnwithdraw(){
		myTurnWithdraw = false;
	}
		/*
		 * 
		 * Movement/Play/Withdraw Functions (Including reassigning or prep for MainPlayer draw)
		 * 
		 * */

			/*
			 * 
			 * RE ORDERS DISPLAYS
			 * 
			 * */

	// RE orders cards in deck
	@SuppressWarnings("deprecation")
	public void deckreorder(){
		ArrayList<Integer> newdeck = deck.getcardPile();
		int size = newdeck.size();
		int deckposx = deck.getPositionx();
		int deckposy = deck.getPositiony();
		for(int r = 0; r<size;r++){
			if (cards[newdeck.get(r)].getisItUp()){
				cards[newdeck.get(r)].flipCard();
			}
			cards[newdeck.get(r)].resize(IR.cardx, IR.cardy);
			cards[newdeck.get(r)].setLocation(deckposx+(r/20), deckposy-(r/20));
			LayeredPane.setLayer(cards[newdeck.get(r)],IR.cardlayer+r);
		}
	}
	// RE orders cards in discard
	@SuppressWarnings("deprecation")
	public void discardreorder(){
		ArrayList<Integer> newdiscard = discard.getcardPile();
		int size = newdiscard.size();
		int discardposx = discard.getPositionx();
		int discardposy = discard.getPositiony();
		for(int r = 0; r<size;r++){
			if (!cards[newdiscard.get(r)].getisItUp()){
				cards[newdiscard.get(r)].flipCard();
			}
			cards[newdiscard.get(r)].resize(IR.cardx, IR.cardy);
			cards[newdiscard.get(r)].setLocation(discardposx+(r/20), discardposy-(r/20));
			LayeredPane.setLayer(cards[newdiscard.get(r)],IR.cardlayer+r);
		}
	}
		// RE orders cards in hand
	@SuppressWarnings("deprecation")
	public void handreorder(int tempnum){
		ArrayList<Integer> newhand = hands[tempnum].getCardPile();
		int size = newhand.size();
		int handposx = hands[tempnum].getPositionx();
		int handposy = hands[tempnum].getPositiony();
		int vertical = hands[tempnum].getVertical();
		int horizontal = hands[tempnum].getHorizontal();
		for(int r = 0; r<size;r++){
			if(tempnum ==1){
				cards[newhand.get(r)].resize(IR.cardy, IR.cardx);
				cards[newhand.get(r)].setIcon(new ImageIcon(this.getClass().getResource(IR.b1)));
			}else if(tempnum ==4){
				cards[newhand.get(r)].resize(IR.cardy, IR.cardx);
				cards[newhand.get(r)].setIcon(new ImageIcon(this.getClass().getResource(IR.b2)));
			}else if(tempnum == 2 || tempnum == 3){
				cards[newhand.get(r)].resize(IR.cardx, IR.cardy);
				cards[newhand.get(r)].setIcon(new ImageIcon(this.getClass().getResource(IR.b3)));
			}else{
				cards[newhand.get(r)].resize(IR.cardx, IR.cardy);
			}
			if (tempnum == 0 && !(cards[newhand.get(r)].getisItUp())){
				cards[newhand.get(r)].flipCard();
			}
			cards[newhand.get(r)].setLocation(handposx+(horizontal*(r*15)), handposy+(vertical*(r*15)));
			LayeredPane.setLayer(cards[newhand.get(r)],IR.cardlayer+r);
		}
	}
	// RE orders cards in play
	@SuppressWarnings("deprecation")
	public void playreorder(int tempnum){
		ArrayList<Integer> newplay = plays[tempnum].getCardPile();
		int size = newplay.size();
		int playposx = plays[tempnum].getPositionx();
		int playposy = plays[tempnum].getPositiony();

			/*
			 * 
			 * HANDLES NORMAL CARDS
			 * 
			 * */
		size = newplay.size();
		for(int r = 0; r<size;r++){
			if (!(cards[newplay.get(r)].getisItUp())){
				cards[newplay.get(r)].flipCard();
			}

			cards[newplay.get(r)].resize(IR.cardx, IR.cardy);
			cards[newplay.get(r)].setLocation(playposx+((r*15))-73, playposy+2);
			LayeredPane.setLayer(cards[newplay.get(r)],IR.cardlayer+r);
		}
		/*
		 * 
		 * HANDLES SPECIAL CARDS
		 * 
		 * */
		int specialCardCounter = 0;
		for(int r = 0;r<size;r++){
			if(specialCards.contains(newplay.get(r))){			
				if (!(cards[newplay.get(r)].getisItUp())){
					cards[newplay.get(r)].flipCard();
				}
				cards[newplay.get(r)].resize(IR.cardx, IR.cardy);
				cards[newplay.get(r)].setLocation(playposx+(IR.playx)-(((++specialCardCounter)*IR.cardx))-73, playposy+2);
				LayeredPane.setLayer(cards[newplay.get(r)],IR.cardlayer+r);
			}
		}
	}

	//plays card
	public void Playscard(int playernum,int id){
		hands[playernum].removeCard(cards[id]);
		plays[playernum].addCard(cards[id]);
		handreorder(playernum);
		playreorder(playernum);
	}
	public void addcardtohand(int UID, int cardnum){
		int handnum = this.getPlayerNumFromID(UID);
		if(handnum == pnum){
				cardNumberTemp = cardnum;
				startmyturndraw();
				return;
			}
			handreorder(handnum);
			deck.removecard(cardnum);
			hands[handnum].addCard(cards[cardnum]);
	}
	//Playing the selected cards	
		//Removes card from play
	public void discardcard(int cardnum, int playnum,boolean hand){
		if(!cards[cardnum].getisItUp()){cards[cardnum].flipCard();}
		if(hand){
			hands[playnum].removeCard(cards[cardnum]);
			discard.addcard(cards[cardnum]);
		}else{
			plays[playnum].removeCard(cards[cardnum]);
			discard.addcard(cards[cardnum]);
		}
	}
		//Player Withdrawing
	public void withdraw(int playnum){
		ArrayList<Integer> templist = plays[playnum].getCardPile();
		int x = 0;
		for(int r = 0;r<templist.size();r++){
			x = templist.get(r);
			discardcard(x,playnum,false);
		}
	}
		//Resets player action called after validating
			/*
			 * 
			 * GETTERS AND SETTERS
			 * 
			 * */
	public void setplaynull(){
		cardPlay = null;
	}
		//Sets Tournament Color
	public void setTournamentColor(int color){
		tournamentDisplay.setText("Tournament Color: "+IR.getPlayColorString(color));
		tournamentDisplay.setBackground(IR.getPlayColor(color));
	}
	public int getColorStringId(String color){
		int r = 0;
		while(!color.equals(IR.getPlayColorString(r).toLowerCase())){
			r++;
		}
		return r;
	}
	public ArrayList<Integer> getHand(int id){
		return hands[id].getCardPile();
	}
	public ArrayList<Integer> getPlay(int id){
		return plays[id].getCardPile();
	}
	public void setHand(int playerID ,ArrayList<Integer> temp){
		hands[playerID].setCardPile(temp);
		hands[playerID].setCardCount(temp.size());
		handreorder(playerID);
	}
	public void setPlay(int playerID ,ArrayList<Integer> temp){
		plays[playerID].setCardPile(temp);
		plays[playerID].setCardCount(temp.size());
		playreorder(playerID);
	}
	public ArrayList<Integer> getDeck(){
		return deck.getcardPile();
	}
	public ArrayList<Integer> getDiscard(){
		return discard.getcardPile();
	}
	public void setDeck(ArrayList<Integer> temp){
		deck.setcardPile(temp);
		deck.setCardCount(temp.size());
		deckreorder();
	}
	public void setDiscard(ArrayList<Integer> temp){
		discard.setcardPile(temp);
		discard.setCardCount(temp.size());
		discardreorder();
	}
	public void displayerror(String errorMsg){
		JOptionPane.showMessageDialog(this,errorMsg, "Ivanhoe: Input Error",JOptionPane.ERROR_MESSAGE);
	}	
	public void displayturn(){
		JOptionPane.showMessageDialog(this,"Your Turn!", "Ivanhoe: Notification",JOptionPane.INFORMATION_MESSAGE);
	}	
	public String requeststring(String displayMsg){
		String temp = null;
		while(temp == null){
			temp =JOptionPane.showInputDialog(displayMsg);
		}
		return temp;
	}
	public boolean addToken(int pnum, int color){
		for(int r = 0; r<Tokens.length;r++){
			if((Tokens[r].getColor() == color)&&(!Tokens[r].getIsTaken())){
				hands[pnum].addToken(Tokens[r]);
				return true;
			}
		}
		return false;
	}
	public boolean removeToken(int pnum, int color){
		for(int r = 0; r<Tokens.length;r++){
			if((Tokens[r].getColor() == color)&&(Tokens[r].getIsTaken())){
				hands[pnum].removeToken(Tokens[r]);
				discard.addtoken(Tokens[r]);
				return true;
			}
		}
		return false;
	}
		/*
		 * 
		 * HANDLES PLAYER SERVER'S NUMBER WITH PLAYER'S UI NUMBER
		 * 
		 * */
	public void addPlayerID(int playerIndex, int UID) {
		playerIDMap.put(playerIndex, UID);
	}

	public Map<Integer, Integer> getPlayerIDMap() {
		return playerIDMap;
	}
	
	public int getPlayerNumFromID(int UID) {
		for (Map.Entry<Integer, Integer> entry : playerIDMap.entrySet()) {
			if (entry.getValue() == UID) {
				return entry.getKey();
			}
		}
		return -1;
	}

	public String getCardPlay() {
		return cardPlay;
	}

	public void setCardPlay(String cardPlay) {
		this.cardPlay = cardPlay;
	}
	public void setNames(String names[]){
		playerNameDisplay = new JLabel[names.length];
		for(int r = 0;r<names.length;r++){
			playerNameDisplay[r] = new JLabel((r+1)+". "+names[r]);
			playerNameDisplay[r].setForeground(IR.getPlayColor(r));
			playerNameDisplay[r].setFont(new Font("Verdana", Font.BOLD, 12));
			playerNameDisplay[r].setBounds(0, 0+(r*15), IR.Namesx,IR.Namesy);
			playerNameDisplay[r].setVisible(true);
			LayeredPane.add(playerNameDisplay[r], IR.cardlayer-1,0);
		}
	}

	public void createCardsWithIcons(String[] deckAddresses) {
		IR.cardcount = deckAddresses.length;
		/*
		 * 
		 * CARD SETUP
		 * 
		 * */
		largeImage = new ImageIcon[IR.cardcount+1] ;
		cards = new Card[IR.cardcount];
		for(int i = 0; i<IR.cardcount; i++){
			cards[i] = new Card(i,IR);
			cards[i].addMouseListener(this);
			LayeredPane.add(cards[i],IR.cardlayer,0);
			deck.addcard(cards[i]);
				/*
				 *CREATING LARGE CARD IMAGES
				 * */
			largeImage[i] = (new ImageIcon(this.getClass().getResource(deckAddresses[i])));
				/*
				 * CREATING THE SMALL CARD IMAGES
				 * */
			String temp[] = deckAddresses[i].split("[.]");
			deckAddresses[i] = temp[0] + "R."+temp[1];
			cards[i].setCardIcon(new ImageIcon(this.getClass().getResource(deckAddresses[i])));
			if(specialCards.contains(i)){
				cards[i].setisItSpecial(true);
			}
		}
		/*
		 * SETTING THE BACKGROUND LARGE IMAGE 
		 * INTO THE LARGE IMAGE CARD ARRAY
		 * */
		largeImage[IR.cardcount] = (new ImageIcon(this.getClass().getResource(IR.lb0))) ;
		largeCard.setIcon(largeImage[IR.cardcount]);
	}



	public void setSpecialcards(String[] specials) {
		for(int r = 0;r<specials.length;r++){
			specialCards.add(Integer.parseInt(specials[r]));
		}
	}
}