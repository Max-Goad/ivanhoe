package UI;
/*
 * Name: 		PlayersHand.java
 * Created by:	Peter Zumbach
 * Description:	Stores a list of Integers to keep track of which cards are in what hands in this display for state checking
 * Stores location of the panel itself as well as positioned cards in proper position corresponding to the hands position
 */
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class PlayersHandandPlay extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Integer> cardPile = new ArrayList<Integer>();
	private ArrayList<Integer> tokenpile = new ArrayList<Integer>();
	private ImageRef tempIR = new ImageRef();
	private int positionx = 0;
	private int positiony = 0;
	private int playnum = 0;
	private int cardCount = 0;
	private int tokenCount = 0;
		//Store state or orientation
	private int horizontal = 0;
	private int vertical = 0;
	private int direction = 0;
	
	PlayersHandandPlay(int x, int y, int num,int dir){
		if(dir > 0){vertical = 1;}
		else{horizontal = 1;}
		direction = dir;
		positionx = x;
		positiony = y;
		playnum = num;
		setLocation(positionx,positiony);
		setVisible(true);

	}
	// Adds a card a resizes it to the new image rotation so that the cards appear in the proper rotation on the board based on the players position
	@SuppressWarnings("deprecation")
	public boolean addCard(Card tempCard){
		if(direction ==1){
			tempCard.resize(tempIR.cardy, tempIR.cardx);
			tempCard.setIcon(new ImageIcon(this.getClass().getResource(tempIR.b1)));
		}else if(direction == 2){
			tempCard.resize(tempIR.cardy, tempIR.cardx);
			tempCard.setIcon(new ImageIcon(this.getClass().getResource(tempIR.b2)));
		}
		cardPile.add(new Integer(tempCard.getCardID()));
		tempCard.setLocation(positionx+(horizontal*(cardCount*15)), positiony+(vertical*(cardCount*15)+5));
		cardCount++;
		return true;
	}
	//Removes a card and resizies it as need be
	@SuppressWarnings("deprecation")
	public void removeCard(Card tempCard){
		cardCount--;
		if(direction >0){
			tempCard.resize(tempIR.cardx, tempIR.cardy);
			tempCard.setIcon(new ImageIcon(this.getClass().getResource(tempIR.b0)));
		}
		cardPile.remove(new Integer(tempCard.getCardID()));
	}
	//Checks if a card is contained in the array list
	public boolean contains(int cardID) {
		return (cardPile.contains(new Integer(cardID)));
	}

	// adds a token to the players hand in the proper position
	public boolean addToken(Token tempCard){
		tokenCount++;
		tokenpile.add(new Integer(tempCard.getTokenID()));
		tempCard.possesed(playnum);
		int upordown = 1;
		if(playnum == 0 || playnum ==4){upordown = -1;}
		tempCard.setLocation(positionx+(horizontal*(tokenCount*30))+(vertical*(50)*upordown), positiony+(vertical*(tokenCount*30)+5)+(horizontal*(50)*upordown));
		return true;
	}
	//Getters and setters
	public ArrayList<Integer> getCardPile(){
		return cardPile;
	}
	public ArrayList<Integer> setCardPile(ArrayList<Integer> cardPile){
		return this.cardPile = cardPile;
	}
	public void removeToken(Token tempCard){
		tokenCount--;
		tokenpile.remove(new Integer(tempCard.getTokenID()));
	}
	public int getPositionx() {
		return positionx;
	}
	public void setPositionx(int positionx) {
		this.positionx = positionx;
	}
	public int getPositiony() {
		return positiony;
	}
	public void setPositiony(int positiony) {
		this.positiony = positiony;
	}
	public int getVertical() {
		return vertical;
	}
	public int getHorizontal() {
		return horizontal;
	}
	public int getCardCount() {
		return cardCount;
	}
	public void setCardCount(int cardCount) {
		this.cardCount = cardCount;
	}
}