package UI;
/*
 * Name: 		DeckandDiscard.java
 * Created by:	Peter Zumbach
 * Description:	Stores visual state of deck and discard for recording integers of the id vales for the cards in the deck or discard for
 * state checking, adding ,removing and checking cards.
 */
import java.util.ArrayList;

public class DeckandDiscard {
	private ArrayList<Integer> cardPile = new ArrayList<Integer>();
	private int positionx = 0;
	private int positiony = 0;
	private int cardCount = 0;
	//Constructor which holds its frame position
	DeckandDiscard(int x, int y){
		positionx = x;
		positiony = y;
	}
	// Adds a card to the pile
	public void addcard(Card tempCard){
		cardCount++;
		cardPile.add(new Integer(tempCard.getCardID()));
		tempCard.setLocation(positionx+(cardCount/20), positiony-(cardCount/20));
	}
	//Removes a card from the pile
	public void removecard(int cardID){
		cardCount--;
		cardPile.remove(new Integer(cardID));
	}
	//Checks if the arraylist contains a card
	public Boolean contains(int cardID){
		return cardPile.contains(new Integer(cardID));
	}
	//Gets the array list for checking if it accurate
	public ArrayList<Integer> getcardPile(){
		return cardPile;
	}
	//IF a game updates state the new arraylist is assigned 
	public ArrayList<Integer> setcardPile( ArrayList<Integer> cardPile){
		return this.cardPile = cardPile;
	}
	//An area to move tokens when they are disabled
	public void addtoken(Token token) {
		token.possesed(-1);
		token.setLocation(positionx, positiony);
	}
		/*
		 * 
		 * GETTER'S AND SETTER'S 
		 * 
		 * */
	public int getPositionx() {
		return positionx;
	}
	public void setPositionx(int positionx) {
		this.positionx = positionx;
	}
	public int getPositiony() {
		return positiony;
	}
	public void setPositiony(int positiony) {
		this.positiony = positiony;
	}
	public int getCardCount() {
		return cardCount;
	}
	public void setCardCount(int cardCount) {
		this.cardCount = cardCount;
	}
}
