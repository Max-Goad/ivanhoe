package UI;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.concurrent.TimeUnit;
import client.ClientUserInterface;

public class UIFacade implements ClientUserInterface {
	 	/*
	 	 * 
	 	 * Decleration of Variables
	 	 * 
	 	 * */
	private static UIMain mainWindow;
	private int MainUID=0;
	 	/*
	 	 * 
	 	 * Starts the UI
	 	 * 
	 	 * */
	 @Override
	 public void start(int UID){
		 mainWindow  = new UIMain();
	     mainWindow.setVisible(true);
	     MainUID = UID;
	     ///mainWindow.addPlayerID(0, UID);////////////////////////////////////////
	 }
	 	/*
	 	 * 
	 	 * Stops the UI
	 	 * 
	 	 * */
	 @Override
	 public void stop(){
	     if (mainWindow != null) {
			 mainWindow.setVisible(false);
			 mainWindow = null;
	     }
	 }
	 	/*
	 	 * 
	 	 * Reports an error to the User
	 	 * 
	 	 * */
	 @Override
	 public void reportErrorToUser(String errorMsg){
		if(mainWindow == null){return;}
		 mainWindow.displayerror(errorMsg);
	 }	

	 	/*
	 	 * 
	 	 * Prompt User for String
	 	 * 
	 	 * */

	 @Override
	 public String promptUserForString(String prompt){
		if(mainWindow == null){return null;}
		return (mainWindow.requeststring(prompt)); 
	 }
	 	/*
	 	 * 
	 	 * Get Turn Action
	 	 * 
	 	 * */
	 @Override
	 public String getTurnAction(){
			if(mainWindow == null){return null;}
		mainWindow.startmyturnplay();
		while(mainWindow.getCardPlay() == null){
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		String returnstring = mainWindow.getCardPlay();
		mainWindow.setplaynull();
		return returnstring;	 
	 }
	
	 	/*
	 	 * 
	 	 * Updates State
	 	 * 
	 	 * */
	 @Override
	 public void updateState(String state){
		 if (mainWindow == null) {
			 return;
		 }
		 
		 String[] actiontype = state.split(" ");
		 
		 	/// HANDLES DRAWING OF CARDS
		 if(actiontype[0].equals("Draw")){			 
			 mainWindow.addcardtohand( Integer.parseInt(actiontype[1]),  Integer.parseInt(actiontype[2]));	 
		 }
		 
		 	/// HANDLES UPDATING GAMESTATE
		 else if(actiontype[0].equals("GameState")){
			doStateUpdate(actiontype[1]);
		 }
		 
		 	/// HANDLES WITHDRAWS
		 else if(actiontype[0].equals("Withdraw")){
			 mainWindow.withdraw(mainWindow.getPlayerNumFromID(Integer.parseInt(actiontype[1])));
		 }
		 
		 	/// HANDLES NEW GAME
		 else if(actiontype[0].equals("NewGame")){
			 setSpecials(actiontype[4]);
			 doDeckAssigning(actiontype[2]);
			 doPlayerNumberInsertion(actiontype[1],actiontype[3]);
			 doStateUpdate(actiontype[1]);
		 }
		 
		 	/// HANDLES GIVING OF TOKENS
		 else if(actiontype[0].equals("Token")){
			 int playid = mainWindow.getPlayerNumFromID(Integer.parseInt(actiontype[1]));
			 if(actiontype[3].equals("Won")){
				 if(actiontype[2].equals("purple")){
					 mainWindow.addToken(playid,1);} 
				 else if(actiontype[2].equals("red")){
					 mainWindow.addToken(playid,2);}
				 else if(actiontype[2].equals("blue")){
					 mainWindow.addToken(playid,3);}
				 else if(actiontype[2].equals("yellow")){
					 mainWindow.addToken(playid,4);}
				 else if(actiontype[2].equals("green")){
					 mainWindow.addToken(playid,5);}
			 }else if(actiontype[3].equals("Lose")){
				 if(actiontype[2].equals("purple")){
					 mainWindow.removeToken(playid,1);
				 }else if(actiontype[2].equals("red")){
					 mainWindow.removeToken(playid,2);}
				 else if(actiontype[2].equals("blue")){
					 mainWindow.removeToken(playid,3);}
				 else if(actiontype[2].equals("yellow")){
					 mainWindow.removeToken(playid,4);}
				 else if(actiontype[2].equals("green")){
					 mainWindow.removeToken(playid,5);}
			 }else{
				 //ERROR MESSAGE
				 System.out.print("Token condition is invalid");
			 }
		 }
		 
		 	/// HANDLES TOURNAMENT COLOR CHANGING
		 else if(actiontype[0].equals("Tournament")){
			 mainWindow.setTournamentColor(mainWindow.getColorStringId(actiontype[1]));
		 }
		 
		 	/// HANDLES PLAYING OF CARDS
		 else if(actiontype[0].equals("Play")){
			 mainWindow.Playscard( mainWindow.getPlayerNumFromID(Integer.parseInt(actiontype[1])),  Integer.parseInt(actiontype[2]));
		 }else{
			 System.out.println("WRONG SERVER INPUT" + actiontype[0]);
			 
		 }
		 
	 }


	private void doDeckAssigning(String actiontype) {
		String deckAddresses[] = actiontype.split("\\|");
		mainWindow.createCardsWithIcons(deckAddresses);
		}
	//Checks every section of the gamestate and if the check returns false it then passes the gamestate update to that section to perform the fix
	public void doStateUpdate(String actiontype){
		if(mainWindow == null){return;}
		String state[] = actiontype.split("\\|");
		 for(int r = 1;r<state.length;r++){
			 if(r < 3){
				 // RETURNS FALSE IF DECK OR DISCARD ARE NOT EQUAL TO STATE
				 if(r == 1){if(!doSectionUpdate(state[r],mainWindow.getDeck())){mainWindow.setDeck(doSectionChange(state[r],"Deck"));}}
				 else if(r == 2){if(!doSectionUpdate(state[r],mainWindow.getDiscard())){mainWindow.setDiscard(doSectionChange(state[r],"Discard"));}}
			 }else{
				 // HANDLE PLAYERS IN ORDER
				 String players[] = state[r].split("\\\\");
				 int x = mainWindow.getPlayerNumFromID(Integer.parseInt(players[0]));
				 String PlayerHandPlay[] = players[1].split("/");
				 if(!doSectionUpdate(PlayerHandPlay[0],mainWindow.getHand(x))) {
					 mainWindow.setHand(x,doSectionChange(PlayerHandPlay[0],"Hand"));
				 }
				 if(!doSectionUpdate(PlayerHandPlay[1],mainWindow.getPlay(x))){
					 mainWindow.setPlay(x,doSectionChange(PlayerHandPlay[1],"Play"));
				 }
			 }
		 }
	 }
	 //Alters a players hand and play field if there is a false return from dosectionupdate
	private ArrayList<Integer> doSectionChange(String actiontype, String s) {
		ArrayList<Integer> temp = new ArrayList<Integer>();
		 //Check IF Deck Matches
		 String state[] = actiontype.split(",");
		 if(state[0].equals("&")){
			 //SET IT TO EMPTY
			 	return temp;
		 }else{//SET EACH
			 for(int r = 0; r<state.length;r++){
				 temp.add(Integer.parseInt(state[r]));
			 }
			 
		 }
		 return temp;
	 }
	//Checks a players hand and play field
	 private boolean doSectionUpdate(String actiontype, ArrayList<Integer> temp){
		 //Check IF Deck Matches
		 String state[] = actiontype.split(",");
		 if(state[0].equals("&")){
			 return temp.isEmpty();
		 }else{//CHECK EACH FOR MATCHING
			 if(state.length != temp.size()){return false;}
			 int x = 0;
			 Boolean tempBool = false;
			 for(int r = 0; r<state.length;r++){
				 tempBool = temp.contains(Integer.parseInt(state[r]));
				 x = tempBool.compareTo(true);
				 if(x !=0){
					 return false;
				 }
			 }
		 }
		return true;
	 }
	 // Adds in the players IDs for parsing later use in game state updates, and also adds the name of the players so that i can display them on the board
	private void doPlayerNumberInsertion(String actiontype, String names) {
		if(mainWindow == null){return ;}
		
		String tempMainPlayerString = String.valueOf(MainUID);
		String tempPlayerNumber = "";
		String tempPlayerName = "";
		
		Deque<String> playerOrder = new ArrayDeque<String>();
		Deque<String> playerNameOrder = new ArrayDeque<String>();
		String state[] = actiontype.split("\\|");
		String namesArray[] = names.split("/");
		for(int r = 3;r<state.length;r++){
			String player[] = state[r].split("\\\\");
			playerOrder.addLast(player[0]);
		}
		for(int r = 0; r<namesArray.length;r++){
			playerNameOrder.addLast(namesArray[r]);
		}
		
		while(!playerOrder.peekFirst().equals(tempMainPlayerString)){
			tempPlayerNumber = playerOrder.removeFirst();
			tempPlayerName   = playerNameOrder.removeFirst();
			playerOrder.addLast(tempPlayerNumber);
			playerNameOrder.addLast(tempPlayerName);
		}
		int p = playerOrder.size();
		namesArray = new String[p];
		for(int x = 0; x<p;){
			namesArray[x] = playerNameOrder.removeFirst();
			mainWindow.addPlayerID(x++, Integer.parseInt(playerOrder.removeFirst()));

		}
		mainWindow.setNames(namesArray);
	}
	//Allows the setting of special cards and will put them to the far right side of a players play field when played.
	public void setSpecials(String actiontype){
		String specials[] = actiontype.split("\\|");
		mainWindow.setSpecialcards(specials);
	}
}



