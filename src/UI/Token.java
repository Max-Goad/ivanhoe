package UI;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
/*
 * Color 1 = Purple
 * Color 2 = Red
 * Color 3 = Blue
 * Color 4 = Yellow
 * Color 5 = Green
 * 
 * */
public class Token  extends JLabel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int tokenID;
	private int color= 0;
	private int pnum = -1;
	private ImageIcon tokenTile; 
	private boolean isTaken = true;


	
	/*
	 * Card Constructor
	 * */
	Token(int tempNum,ImageRef tempIR,int colorID){
			//Position and tokenID setup
		setColor(colorID);
		setTokenID(tempNum);
		tokenTile = new ImageIcon(this.getClass().getResource(tempIR.tokens[tempNum]));
		setIcon(tokenTile);	
		setBounds(0, 0, tempIR.tokenx, tempIR.tokeny);
		setVisible(false);
	}

	/*
	 * Gives possession of the token to a player
	 * */
	public void possesed(int owner){
		if(owner == -1 && isTaken){ // If the card is facing up
			pnum = -1;
			setVisible(false);
			isTaken = false;
		}else{ //If the card is facing down
			pnum = owner;
			setVisible(true);
			isTaken = true;
		}
	}
		/*
		 * 
		 * GETTER'S AND SETTER'S 
		 * 
		 * */
	public int getPlayerID(){
		return pnum;
	}
	public boolean getIsTaken(){
		return isTaken;
	}

	public int getTokenID() {
		return tokenID;
	}
	public void setTokenID(int tokenID) {
		this.tokenID = tokenID;
	}
	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}

}
