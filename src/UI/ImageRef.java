/*
 * Name: 		ImageRef.java
 * Created by:	Peter Zumbach
 * Description:	Stores string references to cards for easy use
 */

package UI;

import java.awt.Color;

public class ImageRef {
	 
	/*
		 * 
		 * LAYERS
		 * 
		 * */
		//layer 1 is the background
	int cardlayer = 2;
	int tokenlayer = cardlayer+1;
	int middle = 50;
	int top = 100;
		/*
		 * 
		 * COLOR SETUP
		 * 
		 * */
	private Color Purple = new Color(142, 0, 142);
	private Color Blue = new Color(0, 75, 255);
	private Color playColor[]= {Purple,Color.RED,Color.GREEN,Blue,Color.YELLOW,Color.WHITE};
	private String playColorString[]= {"purple","red","green","blue","yellow","white"};
		//Array setup
	
		//Background IMAGE!
	String background = "/table.jpg";
		/*
		 * MINI CARDS
		 * */
	/*
	 * Back Card
	 * */
	String b0 = "/simpleCards18R.jpg"; //back of cards
	String b1 = "/simpleCards18RSL.jpg"; //back of cards
	String b2 = "/simpleCards18RSR.jpg"; //back of cards
	String b3 = "/simpleCards18U.jpg"; //back of cards

	// Array that stores the string for each card in the deck
	// 110 cards in the deck of mini cards
	String Deck[];

	
		/*
		 * LARGE CARDS
		 * */
	/*
	 * Back Card
	 * */
	String lb0 = "/simpleCards18.jpg"; //back of cards
	// Array that stores the string for each card in the deck

	/*
	 * Card Size
	 * */
	int cardx = 25;
	int cardy = 35;
	/*
	 * Card Size
	 * */
	int largecardx = 266;
	int largecardy = 375;
	/*
	 * Window Size
	 * */
	int windowx = 1184;
	int windowy = 893;
	/*
	 * Deck Position
	 * */
	int deckx = 800;
	int decky = 350;
	/*
	 * Discard pile Position
	 * */
	int discardx = deckx+50;
	int discardy = decky;
	/*
	 * Name Board Size
	 * */
	int Namesx = 150;
	int Namesy = 30;
	/*
	 * Card Count
	 * */
	int cardcount;
	/*
	 * Player x's Hand Position
	 * */
	int playx = 250;
	int playy = cardy+10;
	int shuffley = 50;
	int Pplayx[] = {((windowx/2) -playx-1),((windowx/2)+1),((windowx/2) -playx -1),((windowx/2)+1),((windowx/2)-(playx/2))};
	int Pplayy[] = {((windowy/2)+shuffley+1),((windowy/2)+shuffley +1),((windowy/2 -1) -playy+shuffley),((windowy/2) -playy+shuffley -1),((windowy/2) +(playy)+shuffley+2)};
	/*
	 * Player x's Play Position
	 * */
	int handx = playx;
	int handy = playy;
	int Phandx[] = {((windowx/8)*3+25),((windowx/8)+70),((windowx/8)*2+45),(windowx/8*4+15),(windowx/8*6+35)};
	int Phandy[] = {(windowy/8*7-45),(windowy/2-125),(windowy/8+20),(windowy/8+20),(windowy/2-125)};
	/*
	 * 
	 * Token Size
	 * 
	 * */
	int tokenx = 20;
	int tokeny = 20;
	/*
	 * Tournament Display Position
	 * */
	int displayx = windowx/2-(handx/2);
	int displayy = windowy/3;
	/*
	 * 
	 * Token Image Refrences
	 * 
	 * */
	String t0 = "/Purple.jpg";
	String t1 = "/Red.jpg";
	String t2 = "/Blue.jpg";
	String t3 = "/Yellow.jpg";
	String t4 = "/Green.jpg";
	String tokens[] = {	t0,t0,t0,t0,t0,//5
						t1,t1,t1,t1,t1,//5
						t2,t2,t2,t2,t2,//5
						t3,t3,t3,t3,t3,//5
						t4,t4,t4,t4,t4,//5
						};
	
	ImageRef(){
	}
	


	public String getPlayColorString(int i) {
		return playColorString[i];
	}

	public void setPlayColorString(String playColorString[]) {
		this.playColorString = playColorString;
	}

	public Color getPlayColor(int i) {
		return playColor[i];
	}

	public void setPlayColor(Color playColor[]) {
		this.playColor = playColor;
	}

	public void setDeck(String Deck[]) {
		this.Deck = Deck;
	}
}
