/*
 * Name: 		UITester.java
 * Created by:	Peter Zumbach
 * Description:	Runs the UI for me to visible denote changes
 */
//
package UI;
//Just for running to make sure there isn't any syntax issues

public class UITester {
	 static UIMain mainWindow;
	 String s = "|42,35,72,21,10,56,33,78,51,48,79,12,41,81,49,2,46,44,73,62,64,24,75,66,65,23,38,68,7,55,36,5,18,30,25,58,1,26,27,20,61,88,87,43,54,59,82,37,8,13,11,45,57,17,14,15,32,47,85,39,77,84,83,67,3,60,76,69,4,63,52,6,86,50,9,71,80,89,40,74,70,34,29,0,28,19,16,22,53|-|1\5,6,7,8,9/-|2\10,11,12,13,14/-|";
    public static void main(String[] args) {
        mainWindow = new UIMain();
        mainWindow.setVisible(true);
    }
    
}
