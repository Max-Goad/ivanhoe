package ai;

import ai.strategy.AIStrategy;
import engine.GameState;
import util.EngineCommandWriter;

public class AIPlayer {
	
	private int UID;
	private String aiName;

	private AIStrategy aiStrategy;
	
	private boolean errorFlag;
	
	//////////////
	//Constructors
	//////////////
	public AIPlayer(int UID, String aiName, AIStrategy aiStrategy) {
		this.UID = UID;
		this.aiName = aiName;
		
		this.aiStrategy = aiStrategy;
		
		this.errorFlag = false;
	}
	
	//////////
	//Main fns
	//////////
	public String getTurnAction() {
		if (errorFlag) {
			errorFlag = false;
			return EngineCommandWriter.endTurnCommand();
		}
		
		return aiStrategy.getNextTurnAction(GameState.getInstance(), this.UID);
	}
	
	///////////////////////////////////////////
	//Getters, Setters, Other state-related fns
	///////////////////////////////////////////
	public int getUID() {
		return this.UID;
	}
	
	public String getName() {
		return this.aiName;
	}
	
	public void setErrorFlag(boolean errorFlag) {
		this.errorFlag = errorFlag;
	}
}
