package ai;

import java.util.Random;

import ai.strategy.*;
import util.UIDGenerator;

public class AIFactory {
	
	public AIPlayer createAIPlayer() {
		Random rng = new Random();
		int strategyNum = rng.nextInt(AIStrategy.AI_STRATEGY_LIST.length);
		String strategyName = AIStrategy.AI_STRATEGY_LIST[strategyNum];
		
		return createAIPlayer(strategyName);
	}
	
	public AIPlayer createAIPlayer(String strategyName) {
		AIStrategy newStrategy;
		String stratShortForm;
		
		switch(strategyName) {
			case "SimpleOnlyStrategy":
				newStrategy = new SimpleOnlyStrategy();
				stratShortForm = "SOS";
				break;
			case "SimpleWinningStrategy":
				newStrategy = new SimpleWinningStrategy();
				stratShortForm = "SWS";
				break;
			default:
				return null;
		}

		int newUID = UIDGenerator.generate();
		String newName = stratShortForm + "AI" +  newUID;
		
		return new AIPlayer(newUID, newName, newStrategy);
	}
}
