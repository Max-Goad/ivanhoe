package ai.strategy;

import java.util.ArrayList;

import cards.Card;
import engine.GameState;
import engine.Player;
import util.EngineCommandWriter;
import util.GameStateUtils;

/*
 * SimpleOnlyStrategy
 * 	-Plays the largest simple card in hand that matches tournament colour
 * 	-Plays supporters if possible
 * 	-Ignores ALL action cards
 * 	-Ignores gamestate and win conditions
 */
public class SimpleOnlyStrategy implements AIStrategy {
	
	public String getNextTurnAction(GameState currentGameState, int myUID) {

		Player myPlayer = currentGameState.getPlayerByID(myUID);
		ArrayList<Card> myHand = myPlayer.getHand();
		
		//First, determine current tournament colour
		String currentTournamentColour = currentGameState.getTournamentColour();
		
		//If no tournament, pick highest simple card number in hand
		if (currentTournamentColour.equals("none")) {
			
			//Get card of largest value
			Card largestCard = GameStateUtils.getHighestValueCard(myHand);
			
			//No simple cards to play? END TURN.
			if (largestCard == null) {
				return EngineCommandWriter.endTurnCommand();
			}
			//If the largest card is a supporter, determine the best colour to choose and PLAY.
			else if (largestCard.getColour().equals("white")) {
				String bestColour = GameStateUtils.getHighestValueColour(myHand);
				
				//If no best colour, pick purple
				if (bestColour == null) {
					bestColour = "purple";
				}
				
				return EngineCommandWriter.playSupporterStartTournamentCommand(largestCard.getID(), bestColour);
			}
			//Largest card isn't white? PLAY it!
			else {
				return EngineCommandWriter.playCardCommand(largestCard.getID());
			}
			
		}
		//Tournament is active already
		else {
			
			//Get card of largest value that is same colour as tournament colour
			Card largestCard = GameStateUtils.getHighestValueCard(GameStateUtils.colourSubset(myHand, currentTournamentColour));
			
			//No coloured cards to play? Try supporters!
			if (largestCard == null) {
				
				largestCard = GameStateUtils.getHighestValueCard(GameStateUtils.colourSubset(myHand, "white"));
				
				//No supporters? End turn.
				if (largestCard == null) {
					return EngineCommandWriter.endTurnCommand();
				}
				//Otherwise, PLAY the supporter!
				else {
					return EngineCommandWriter.playCardCommand(largestCard.getID());
				}
			}
			//Otherwise, PLAY the card!
			else {
				return EngineCommandWriter.playCardCommand(largestCard.getID());
			}
		}
		
		//UNREACHABLE
	}
}
