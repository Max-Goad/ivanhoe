package ai.strategy;

import java.util.ArrayList;

import cards.Card;
import engine.GameState;
import engine.Player;
import util.EngineCommandWriter;
import util.GameStateUtils;

/*
 * SimpleWinningStrategy
 * 	-Determines if hand + display enough to lead
 * 	-Plays highest correct card if yes
 *  -Withdraws otherwise
 * 	-Ignores ALL action cards
 */
public class SimpleWinningStrategy implements AIStrategy {
	
	public String getNextTurnAction(GameState currentGameState, int myUID) {

		Player myPlayer = currentGameState.getPlayerByID(myUID);
		ArrayList<Card> myHand = myPlayer.getHand();
		ArrayList<Card> myDisplay = myPlayer.getDisplay();
		
		//First, determine current tournament colour
		String currentTournamentColour = currentGameState.getTournamentColour();
		
		//No tournament started
		if (currentTournamentColour.equals("none")) {
			
			//Determine colour with most value in hand
			String bestColour = GameStateUtils.getHighestValueColour(myHand);
			
			//If no best colour, pick purple
			if (bestColour == null) {
				bestColour = "purple";
			}
			
			//Get card of largest value given chosen colour
			Card largestCard = GameStateUtils.getHighestValueCard(GameStateUtils.colourSubset(myHand, bestColour));
			
			//No cards of chosen colour? Try supporters!
			if (largestCard == null) {
				largestCard = GameStateUtils.getHighestValueCard(GameStateUtils.colourSubset(myHand, "white"));
				
				//No supporters? End turn
				if (largestCard == null) {
					return EngineCommandWriter.endTurnCommand();
				}
				
				//Play the supporter, starting tournament with bestColour
				return EngineCommandWriter.playSupporterStartTournamentCommand(largestCard.getID(), bestColour);
				
			}
			//Found a card? Play it!
			else {
				return EngineCommandWriter.playCardCommand(largestCard.getID());
			}
		}
		//Tournament is active already
		else {
			
			//Determine total possible score in your hand + display
			int possibleHandScore = GameStateUtils.getTotalCardValue(GameStateUtils.colourSubset(myHand, currentTournamentColour))
								  + GameStateUtils.getTotalCardValue(GameStateUtils.colourSubset(myHand, "white"));
			
			//No possible cards to play? End turn.
			if (possibleHandScore <= 0) {
				return EngineCommandWriter.endTurnCommand();
			}
			
			int possibleScore = possibleHandScore + GameStateUtils.getTotalCardValue(myDisplay);
			
			//Determine current lead score for tournament
			int currentLeadScore = GameStateUtils.getLeadTournamentScore();
			
			
			//If you can lead the tournament using cards in hand, play highest value card in hand
			if (possibleScore > currentLeadScore) {
				//First try tournament colour
				Card largestCard = GameStateUtils.getHighestValueCard(GameStateUtils.colourSubset(myHand, currentTournamentColour));
				
				//No cards matching tournament colour? Try supporters!
				if (largestCard == null) {
					largestCard = GameStateUtils.getHighestValueCard(GameStateUtils.colourSubset(myHand, "white"));
					
					//No supporters? End turn.
					if (largestCard == null) {
						return EngineCommandWriter.endTurnCommand();
					}
					//Otherwise, PLAY the supporter!
					else {
						return EngineCommandWriter.playCardCommand(largestCard.getID());
					}
				}
				//Otherwise, PLAY the card!
				else {
					return EngineCommandWriter.playCardCommand(largestCard.getID());
				}
			}
			//If you can't possibly lead the tournament, end turn (withdraw)
			else {
				return EngineCommandWriter.endTurnCommand();
			}
		}
		
		//UNREACHABLE
	}
}
