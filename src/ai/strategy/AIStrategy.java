package ai.strategy;

import engine.GameState;

public interface AIStrategy {
	public static final String[] AI_STRATEGY_LIST = { SimpleOnlyStrategy.class.getSimpleName(),
													  SimpleWinningStrategy.class.getSimpleName()
													 };
	
	public String getNextTurnAction(GameState currentGameState, int myUID);
}


/* =============================
 * ADDING A NEW AI STRATEGY
 * -----------------------------
 * By: Max Goad
 * =============================
 * Step 1: 	Create new class in the server.ai.strategy package
 * Step 2: 	Make this new class implement AIStrategy
 * Step 3: 	Fill out the above function with the proper logic
 * Step 4: 	Add your new class to AI_STRATEGY_LIST above so it can be picked randomly
 * Step 5:	Add case to AIFactory.createAIPlayer(String strategyName)
 * Step 6: 	Go to test.ai and make a new test file (see SimpleOnlyStrategyTests for example)
 * Step 7:	You're done!
 */