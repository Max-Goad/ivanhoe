package ai;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AIMediator {
	
	private AIFactory aiFactory;
	
	private Map<Integer, AIPlayer> aiMap = null;
	
	//////////////
	//Constructors
	//////////////
	public AIMediator() {
		aiFactory = new AIFactory();
		aiMap = new HashMap<>();
	}
	
	///////////////////
	//Main Mediator Fns
	///////////////////
	public String getTurnAction(int UID) {
		AIPlayer ai = getPlayer(UID);
		
		return (ai != null) ? ai.getTurnAction() : null;
	}	
	
	public void reportError(int UID) {
		AIPlayer ai = getPlayer(UID);
		
		if (ai != null) {
			ai.setErrorFlag(true);
		}
	}
	
	///////////////////////////////////////////
	//Getters, Setters, Other state-related fns
	///////////////////////////////////////////
	
	/*
	 * Getters
	 */
	public boolean isAIPlayer(int UID) {
		return aiMap.containsKey(UID);
	}
	
	public Set<Integer> getUIDs() {
		return aiMap.keySet();
	}
	
	public AIPlayer getPlayer(int UID) {
		return aiMap.get(UID);
	}
	
	public String getNameFromUID(int UID) {
		AIPlayer matchingPlayer = getPlayer(UID);
		
		return (matchingPlayer != null) ? matchingPlayer.getName() : null;
	}
	
	/*
	 * Setters
	 */
	public void initializeAIPlayers(int numAIs) {
		aiMap.clear();
		
		for (int i = 0; i < numAIs; i++) {
			AIPlayer newAI = aiFactory.createAIPlayer();
			aiMap.put(newAI.getUID(), newAI);
		}
	}
	
	//TODO: Determine if unnecessary
	public void setDoLogFlag(boolean doLog) {
		//this.doLog = doLog;
	}
}
