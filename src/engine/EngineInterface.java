/*
 * Name: 		ServerEngineInterface.java
 * Created by:	Max Goad
 * Description:	Interface for all engines meant to be used by IvanhoeServer to implement.
 */

package engine;

public interface EngineInterface {
	
	//Control fns
	public void resetGameState();
	
	//Main fns
	public boolean addPlayer(int UID, String playerName);
	public boolean removePlayer(int UID);
	public String gameStart();
}
