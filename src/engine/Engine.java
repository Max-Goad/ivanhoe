package engine;

import java.util.*;

import cards.*;
import server.*;

public class Engine implements EngineInterface {
	
	public ServerInterface server;
	
	private GameState gameState;
	
	public Engine(ServerInterface server){
		
		this.server = server;

		this.gameState = GameState.getInstance();
		
		gameState.resetGameState(true);
		
	}

	public String getTournamentColour() { return gameState.getTournamentColour(); }
	public void setTournamentColour(String tournamentColour) { gameState.setTournamentColour(tournamentColour); }

	public ArrayList<Card> getDeck() { return gameState.getDeck(); }
	public ArrayList<Card> getDiscardPile() { return gameState.getDiscardPile(); }

	@Override
	public boolean addPlayer(int UID, String playerName){
		return this.addPlayer(new Player(playerName, UID));
	}
	
	public boolean addPlayer(Player p) {
		/* Adds a player to the game 
		 * and returns whether it was successful
		 * */
		
		gameState.addPlayer(p);
		return true;
	}
	
	@Override
	public boolean removePlayer(int ID){
		/* Removes the player with the given ID
		 * and returns whether it was successful
		 *  */
		
		if (gameState.getPlayerByID(ID) == null) return false;
		
		if (gameState.isGameRunning()){
			// This means a player dropped in the middle of the game
			// We need to smoothly handle this by advancing the turn and discarding their cards
			if (gameState.getTurn() == gameState.getPlayers().size()){
				gameState.setTurn(0);
			} else {
				gameState.setTurn( (gameState.getTurn()+1) % gameState.getPlayers().size() );
				for (Card c : gameState.getPlayerByID(ID).getHand()){
					gameState.addCardToDiscardPile(c);
				}
				for (Card c : gameState.getPlayerByID(ID).getDisplay()){
					gameState.addCardToDiscardPile(c);
				}
				for (Card c : gameState.getPlayerByID(ID).getSpecialZone()){
					gameState.addCardToDiscardPile(c);
				}
			}
		}

		Player p = gameState.removePlayer(ID);
		
		return (p != null);
	}
	
	@Override
	public void resetGameState() {
		this.resetGameState(false);
	}
	
	public void resetGameState(boolean resetPlayers){
		/* Resets the game state to nothing.
		 * Players will remain in the game,
		 * but everything else is reset.
		 * 
		 * Called when a game has finished.
		 */
		
		gameState.resetGameState(resetPlayers);
	}
	
	@Override
	public String gameStart() {
		return this.gameStart(false);
	}
	
	public String gameStart(boolean testMode){
		/* Sets up the initial game state for the game
		 * to start. Then runs the main game loop. 
		 * 
		 * testMode: running with special constraints necessary for JUnit tests
		 * 		test mode should never be used otherwise
		 */
		
		String deckString = makeCardImageList(gameState.getDeck());
		
		int shieldID = gameState.getShieldID();
		int stunnedID = gameState.getStunnedID();
		
		if (!testMode){
			gameState.shuffleDeck();
			gameState.setTurn((int)Math.floor(Math.random()*gameState.getPlayers().size())-1); // randomly choose a starting player
		} else {
			gameState.setTurn(gameState.getPlayers().size()-1);
		}
		// each player draw 5 cards for their starting hand
		ArrayList<Player> players = gameState.getPlayers();
		for (int i=0; i<8; i++){
			for (Player p : players) {
				drawCard(p);
			}
		}

		server.updateAllStates("NewGame "+createGameStateMessage()+" "+deckString+" "+createPlayerNameList()+" "+shieldID+"|"+stunnedID);
		
		return gameLoop(testMode);
	}
	
	public String makeCardList(ArrayList<Card> cards){
		/* Takes a list of cards and returns a string
		 * consisting of a list of the card ID's 
		 * separated by commas
		 */
		String msg = "";
		if (cards.size() == 0) return "&";
		for (int i=0; i<cards.size(); i++){
			if (i != 0) msg += ",";
			if (cards.get(i) != null) msg += cards.get(i).getID();
		}
		return msg;
	}
	
	public String makeCardImageList(ArrayList<Card> cards){
		/* Takes a list of cards and returns a string
		 * consisting of a list of the card image paths 
		 * separated by commas
		 */
		String msg = "";
		for (int i=0; i<cards.size(); i++){
			if (i != 0) msg += "|";
			if (cards.get(i) != null) msg += cards.get(i).getImagePath();
		}
		return msg;
	}
	
	public String createActionMessage(String action, int playerID, int cardID){
		/* Creates an action message for the UI
		 * 
		 * For example:
		 * "draw 1 15" means player 1 drew the card with ID 15
		 */
		
		return action+" "+playerID+" "+cardID;
		
	}
	
	public String createGameStateMessage(){
		/* Generates a string representation of the
		 * current game state.
		 */
		
		String msg = "";
		
		msg += "|"+makeCardList(gameState.getDeck());
		msg += "|"+makeCardList(gameState.getDiscardPile());
		
		ArrayList<Player> players = gameState.getPlayers();
		
		for (Player p : players) {
			msg += "|"+p.getID();
			msg += "\\"+makeCardList(p.getHand());
			ArrayList<Card> display = p.getDisplay();
			display.addAll(p.getSpecialZone());
			msg += "/"+makeCardList(display);
		}
		
		msg += "|";
		
		return msg;
	}
	
	public String createPlayerNameList(){
		String msg = "";
		for (int i=0; i<gameState.getPlayers().size(); i++){
			if (i != 0) msg += "/";
			if (gameState.getPlayers().get(i) != null) msg += gameState.getPlayers().get(i).getName();
		}
		return msg;
	}
	
	private String gameLoop(boolean testMode){
		
		ArrayList<Player> players = gameState.getPlayers();
		String playerAction; // the message indicating the player's action
		boolean playedThisTurn = false; // indicates if the player has played a card this turn.
		boolean tournamentOver = false; // when the tournament ends it should not advance the turn
		boolean isPurpleDisallowed = false; // if the last tournament was purple, a purple tournament can't be started
		
		while (true){
			
			//Stop game if less than 2 players exist!
			if (gameState.getPlayers().size() < 2) break;
			
			// Each iteration of this loop is one turn.
			
			// Advance the turn, unless a player has just won a tournament
			// (in which case it is that player's turn)
			if (!tournamentOver) {
				int currentTurn = gameState.getTurn();
				gameState.setTurn((currentTurn+1) % players.size());
			} else {
				tournamentOver = false;
			}
			
			// Update the state at the start of every round (after every player's turn)
			if (gameState.getTurn() == 0) server.updateAllStates("GameState "+createGameStateMessage());
			
			Player activePlayer = players.get(gameState.getTurn());
			
			// Skip the player's turn if they are withdrawn
			if (activePlayer.isWithdrawn()) continue;
			
			// The player draws a card
			Card cardDrawn = drawCard(activePlayer);
			server.updateAllStates("Draw "+activePlayer.getID()+" "+cardDrawn.getID());
			
			if (canPlayerStart(activePlayer, isPurpleDisallowed) == false && gameState.getTournamentColour().equals("none")){
				// If there is no tournament and the active player can't start, skip his turn.
				continue;
			}
			
			playedThisTurn = false;
			activePlayer.setAddedToDisplay(false);
			
			// Continue to get turn actions from the player until
			// they end their turn
			boolean endedTurn = false;
			do {
				playerAction = server.getTurnAction(activePlayer.getID());
				//If we receive a null playerAction, end turn
				if (playerAction == null) {
					endedTurn = true;
				}
				else {
					if (handleTurnAction(playerAction, activePlayer, isPurpleDisallowed)) playedThisTurn = true;
					if (testMode) break;
					
					endedTurn = playerAction.equals("Withdraw");
					if (endedTurn && gameState.getTournamentColour().equals("none") && canPlayerStart(activePlayer, isPurpleDisallowed)){
						// You have to start a tournament if you are able to
						server.reportErrorToUser(activePlayer.getID(), "You must start a tournament if you are able");
						endedTurn = false;
					}
				}
			} while (  !(endedTurn)  );
			
			if ((isPlayerWinning(activePlayer) == false || playedThisTurn == false)
					&& !(gameState.getTournamentColour().equals("none"))){
				// If the player doesn't have the highest valued display, then
				// he/she is withdrawn from the tournament if one is active
				activePlayer.setWithdrawn(true);
				server.updateAllStates("Withdraw "+activePlayer.getID());
				
				if (activePlayer.hasMaiden()){					
					// If the player withdraws while they have a maiden, they lose a token of their choice
					if (activePlayer.getTokens().get("purple") ||
						activePlayer.getTokens().get("red")    ||
						activePlayer.getTokens().get("blue")   ||
						activePlayer.getTokens().get("yellow") ||
						activePlayer.getTokens().get("green")  ){
						// Check if the player has a token to lose
						
						String colour = server.promptUserForString(activePlayer.getID(), "Choose a colour of token to lose");
						while (activePlayer.getTokens().get(colour) == false){
							colour = server.promptUserForString(activePlayer.getID(), "Choose a colour of token to lose");
						}
						
						activePlayer.setToken(colour, false);
						server.updateAllStates("Token "+activePlayer.getID()+" "+colour+" Lose");
					}
				}
				
				cleanDisplay(activePlayer);
				
				if (playersLeft() == 1){
					// If there's only one player left after, he/she gets a token
					
					// Determine who the winning player is
					Player winningPlayer = null;
					for (Player p : players) {
						if (p.isWithdrawn() == false){
							winningPlayer = p;
							break;
						}
					}
					
					// Award the token and exit if the player has won the game
					Player gameWinner = awardToken(winningPlayer);
					if (gameWinner != null) return gameWinner.getName(); 
					cleanDisplay(winningPlayer);
					server.updateAllStates("GameState "+createGameStateMessage());
					
					if (testMode) break;
					
					// Reset the tournament
					if (gameState.getTournamentColour().equals("purple")){
						// If the tournament colour was purple, the next tournament colour cannot be purple
						isPurpleDisallowed = true;
					} else {
						isPurpleDisallowed = false;
					}
					gameState.setTournamentColour("none");
					server.updateAllStates("Tournament white");
					tournamentOver = true;
					gameState.setTurn(players.indexOf(winningPlayer));
					for (Player p : players) {
						p.setWithdrawn(false);
					}
					
				}
			}
			
		}
		return null;
	}
	
	public boolean handleTurnAction(String playerAction, Player activePlayer, boolean disallowPurple){
		/* Handles the turn action of the specified player
		 * Processes the card played (if it's valid)
		 * and sends a game state message towards the UI
		 */
		if(playerAction.equals("Withdraw")) return false;
		
		/*	New Code
		 *	Moved the splitting out here so we could parse the new "additionalInfo" field
		 *	This field is needed so the AI can send ALL card info in one message, as opposed
		 *	to the multiple messages (via promptUserForString) usually done for the player.
		 * 	-Max 
		 */
		
		String[] playerActionSplit = playerAction.split(" ");
		String playCmd = playerActionSplit[0];
		int cardID = -1;
		String additionalInfo = null;
		
		if (playerActionSplit.length >= 2) {
			try {
				cardID = Integer.parseInt(playerActionSplit[1]);
			} catch (NumberFormatException e) {
				server.reportErrorToUser(activePlayer.getID(), 
										 "Could not parse 1st arg (cardID) from playerAction: " + playerAction);
				return false;
			}
		}
		//If additionalInfo exists, join together remaining split elements
		if (playerActionSplit.length >= 3) {
			additionalInfo = String.join(" ", Arrays.copyOfRange(playerActionSplit, 2, playerActionSplit.length));
		}
		
		/*	End new code
		 */
		
		if(playCmd.equals("play")){
			Card playedCard = activePlayer.getCardFromHand(cardID);
			
			if (playedCard == null) {
				server.reportErrorToUser(activePlayer.getID(), "Cannot find card of ID " + cardID + " in your hand!");
				return false;
			}
			
			if (gameState.getTournamentColour().equals("none")){				
				if (playedCard.getType().equals("action")){
					server.reportErrorToUser(activePlayer.getID(), 
							playedCard.getID()+" You can't play action cards if no tournament is active");
					return false;
				} else {
					activePlayer.playCard(playedCard.getID());
					activePlayer.setAddedToDisplay(true);
					server.updateAllStates("Play "+activePlayer.getID()+" "+playedCard.getID());
					if (playedCard.getColour().equals("white")){
						// then the player must choose the tournament colour
						String tc = null;
						
						/*	New Code
						 *	For computer players, this new tournament colour is already passed in!
						 *	So, use additionalInfo instead of prompting user for String
						 * 	-Max
						 */
						if (additionalInfo != null) {
							tc = additionalInfo;
						}
						else {
							do {
								tc = server.promptUserForString(activePlayer.getID(), "Choose a tournament colour");
							} while (disallowPurple && tc.equals("purple"));
						}
						/*	End new/modified code
						 */
						
						gameState.setTournamentColour(tc);
					} else {
						if (playedCard.getColour().equals("purple") && disallowPurple){
							server.reportErrorToUser(activePlayer.getID(), 
									"You can't start a purple tournament directly after another purple tournament.");
							return false;
						}
						gameState.setTournamentColour(playedCard.getColour());
					}
					server.updateAllStates("Tournament "+gameState.getTournamentColour());
					return true;
				}
			} else {
				if (playedCard.getType().equals("action")){
					
					boolean success = false;
					String ivanhoePrompt;
					
					GameState oldState = GameState.getCopy();
					
					switch(playedCard.getName()) {
						case "outmaneuver":
						case "charge":
						case "counterCharge":
						case "disgrace":
						{
							//1) ID of player who played card
							success = playedCard.performCardStrategy(gameState, String.valueOf(activePlayer.getID()));
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+".\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
								}
							}
							break;
						}
						case "breakLance":
						{
							//1) ID of the target player
							String targetName="";
							if (additionalInfo == null) {
								String prompt = "Please enter in the name of the player you wish to target with your " + playedCard.getName() + " action!\n";
								prompt += "Valid player names:";
								for (Player p : gameState.getPlayers()) {
									if (p.getID() != activePlayer.getID())
										prompt += " " + p.getName();
								}
							
								targetName = server.promptUserForString(activePlayer.getID(), prompt);
								int targetID = gameState.getPlayerByName(targetName).getID();
								additionalInfo = "" + targetID;
							}
							
							success = playedCard.performCardStrategy(gameState, additionalInfo);
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+" targeting "+targetName+".\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
								}
							}
							
							break;
						}
						case "knockDown":
						case "riposte":
						{
							//1) ID of player who played card
							//2) ID of target player
							String targetName = "";
							
							if (additionalInfo == null) {
								String prompt = "Please enter in the name of the player you wish to target with your " + playedCard.getName() + " action!\n";
								prompt += "Valid player names:";
								for (Player p : gameState.getPlayers()) {
									if (p.getID() != activePlayer.getID())
										prompt += " " + p.getName();
								}
								
								if (playedCard.getName().equals("riposte") && 
										activePlayer.hasAddedToDisplay() && 
										activePlayer.hasStunned()){
									server.reportErrorToUser(activePlayer.getID(), 
											"You can't add more than one card to your display while you are Stunned.");
									return false;
								}
								
								int playerID = activePlayer.getID();
								targetName = server.promptUserForString(playerID, prompt);
								int targetID = gameState.getPlayerByName(targetName).getID();
								additionalInfo = playerID + " " + targetID;
							}
							
							success = playedCard.performCardStrategy(gameState, additionalInfo);
							
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+" targeting "+targetName+".\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
								}
							}
							
							break;
						}
						case "dodge":
						{
							//1) ID of target player
							//2) ID of card to discard
							String targetName = "";
							int discardID = -1;
							
							if (additionalInfo == null) {
								String prompt = "Please enter in the name of the player you wish to target with your " + playedCard.getName() + " action!\n";
								prompt += "Valid player names:";
								for (Player p : gameState.getPlayers()) {
									if (p.getID() != activePlayer.getID())
										prompt += " " + p.getName();
								}
								
								targetName = server.promptUserForString(activePlayer.getID(), prompt);
								int targetID = gameState.getPlayerByName(targetName).getID();
								
								prompt = "Please enter the card number you wish to discard from " + targetName + "'s display!\n";
								prompt += "Valid inputs:";
								for (int i = 0; i < gameState.getPlayerByID(targetID).getDisplay().size(); ++i) {
									prompt += " " + i;
								}
								
								try {
									discardID = Integer.parseInt(server.promptUserForString(activePlayer.getID(), prompt));
								} catch (NumberFormatException e) {
									break;
								}
								
								additionalInfo = targetID + " " + discardID;
							}
							
							success = playedCard.performCardStrategy(gameState, additionalInfo);
							
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+" "
										+ "to discard card number "+discardID+" from "+targetName+"'s display.\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
								}
							}
							
							break;
						}
						case "retreat":
						{
							//1) ID of player who played card
							//2) ID of the card to return from display
							String cardString="";
							if (additionalInfo == null) {
								int playerID = activePlayer.getID();
								String prompt = "Please enter the card number you wish to return from your display to your hand!\n";
								prompt += "Valid inputs:";
								for (int i = 0; i < activePlayer.getDisplay().size(); ++i) {
									prompt += " " + i;
								}
								
								cardString = server.promptUserForString(playerID, prompt);
								additionalInfo = playerID + " " + cardString;
							}
							
							success = playedCard.performCardStrategy(gameState, additionalInfo);
							
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+" "
										+ "to return card number "+cardString+" to his hand.\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
								}
							}
							
							break;
						}
						case "changeWeapon":
						case "unhorse":
						{
							//1) new tournament colour (red/blue/yellow)
							
							if (additionalInfo == null) {
								String prompt = "Please enter the new tournament colour!\n";
								prompt += "Valid inputs: red yellow blue";
								
								additionalInfo = server.promptUserForString(activePlayer.getID(), prompt);
							}
							
							success = playedCard.performCardStrategy(gameState, additionalInfo);
							
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+" "
										+ "to change the tournament colour to "+additionalInfo+".\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
									break;
								}
							}
							
							if (success) server.updateAllStates("Tournament "+gameState.getTournamentColour());
							break;
						}
						case "dropWeapon":
						{
							success = playedCard.performCardStrategy(gameState, null);
							
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+".\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
									break;
								}
							}
							
							if (success) server.updateAllStates("Tournament "+gameState.getTournamentColour());
							break;
						}
						case "outwit":
						{
							//1) ID of the player's card
							//2) ID of target player
							//3) ID of target's card
							int playerCardID = -1;
							String targetName = "";
							int targetCardID = -1;
							if (additionalInfo == null) {
								String prompt = "Please enter the card number you wish to swap from your display!\n";
								prompt += "Valid inputs:";
								for (int i = 0; i < activePlayer.getDisplay().size(); ++i) {
									prompt += " " + i;
								}
								
								try {
									playerCardID = Integer.parseInt(server.promptUserForString(activePlayer.getID(), prompt));
								} catch (NumberFormatException e) {
									break;
								}
								
								prompt = "Please enter in the name of the player you wish to swap cards with!\n";
								prompt += "Valid player names:";
								for (Player p : gameState.getPlayers()) {
									if (p.getID() != activePlayer.getID())
										prompt += " " + p.getName();
								}
								
								targetName = server.promptUserForString(activePlayer.getID(), prompt);
								int targetID = gameState.getPlayerByName(targetName).getID();
								
								prompt = "Please enter the card number you wish to swap from " + targetName + "'s display!\n";
								prompt += "Valid inputs:";
								for (int i = 0; i < gameState.getPlayerByID(targetID).getDisplay().size(); ++i) {
									prompt += " " + i;
								}
								
								try {
									targetCardID = Integer.parseInt(server.promptUserForString(activePlayer.getID(), prompt));
								} catch (NumberFormatException e) {
									break;
								}
								
								additionalInfo = activePlayer.getID() + " " + playerCardID + " " + targetID + " " + targetCardID;
							}
							
							success = playedCard.performCardStrategy(gameState, additionalInfo);
							
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+" "
										+ "to swap card number "+playerCardID+" with "+targetName+"'s "
										+ "card number "+targetCardID+".\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
								}
							}
							
							break;
						}
						case "adapt":
						{
							String prompt = "Player "+activePlayer.getName()+" is playing Adapt."
									+ "Choose the index of the card you want to keep for card value ";
							additionalInfo = "";
							
							for (Player p : gameState.getPlayers()){
								for (int i=1; i<=7; i++){
									ArrayList<Integer> tempList = new ArrayList<Integer>();
									for (int c=0; c<p.getDisplay().size(); c++){
										if (p.getDisplay().get(c).getValue() == i){
											tempList.add(c);
										}
									}
									
									if (tempList.size() == 0){
										continue;
									} else if (tempList.size() == 1){
										additionalInfo += p.getDisplay().get(tempList.get(0)).getID()+",";
									} else {
										boolean validInput = false;
										while (!validInput){
											String tempPrompt = prompt+i+": ";
											for (Integer x : tempList){
												tempPrompt += x+", ";
											}
											String response = server.promptUserForString(p.getID(), 
													tempPrompt);
											if (response != null){
												try {
													if (tempList.contains(Integer.parseInt(response))){
														validInput = true;
														additionalInfo += response+",";
													}
												} catch (NumberFormatException e){
													validInput = false;
												}
											}
										}
									}
									
								}
								
								additionalInfo = additionalInfo.substring(0, additionalInfo.length()-2)+" ";
								
							}
							
							success = playedCard.performCardStrategy(gameState, additionalInfo);
							
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+".\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
								}
							}
							
							break;
						}
						case "shield":
						{
							success = playedCard.performCardStrategy(gameState, null);
							
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+".\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
								}
							}
							
							if (success){
								activePlayer.getSpecialZone().add(playedCard);
								activePlayer.getHand().remove(playedCard);
								server.updateAllStates("GameState "+createGameStateMessage());
								return true;
							}
						}
						case "stunned":
						{
							String targetName = "";
							if (additionalInfo == null) {
								String prompt = "Please enter in the name of the player you wish to target with your " + playedCard.getName() + " action!\n";
								prompt += "Valid player names:";
								for (Player p : gameState.getPlayers()) {
									if (p.getID() != activePlayer.getID() && p.isWithdrawn()==false)
										prompt += " " + p.getName();
								}
								
								targetName = server.promptUserForString(activePlayer.getID(), prompt);
								int targetID = gameState.getPlayerByName(targetName).getID();
								
								additionalInfo = ""+targetID;
							}
							
							success = playedCard.performCardStrategy(gameState, additionalInfo);
							
							if (success){
								ivanhoePrompt = "Player "+activePlayer.getName()+" is playing "+playedCard.getName()+" "
										+ "targeting player "+targetName+".\n"
										+ "Would you like to play Ivanhoe to counter it?";
								if (handleIvanhoePlaying(activePlayer, playedCard, ivanhoePrompt, oldState)){
									success = false;
								}
							}
							
							if (success){
								gameState.getPlayerByID(Integer.parseInt(additionalInfo)).getSpecialZone().add(playedCard);
								activePlayer.getHand().remove(playedCard);
								server.updateAllStates("GameState "+createGameStateMessage());
								return true;
							}
							
						}
						case "ivanhoe":
						case "simple":
						default:
						{
							//We shouldn't get this!
							break;
						}
					}
					
					if (success){
						gameState.addCardToDiscardPile(playedCard);
						activePlayer.getHand().remove(playedCard);
						server.updateAllStates("GameState "+createGameStateMessage());
					}
					
					return success;
					
				} else {
					if (playedCard.getColour().equals(gameState.getTournamentColour()) || playedCard.getColour().equals("white")){
						if (playedCard.getColour().equals("white") && playedCard.getValue()==6 && activePlayer.hasMaiden()){
							server.reportErrorToUser(activePlayer.getID(), "You can only have one maiden in play at a time.");
							return false;
						} else {
							if ( !(activePlayer.hasStunned()) || !(activePlayer.hasAddedToDisplay()) ){
								activePlayer.playCard(playedCard.getID());
								server.updateAllStates("Play "+activePlayer.getID()+" "+playedCard.getID());
								activePlayer.setAddedToDisplay(true);
								return true;
							} else {
								server.reportErrorToUser(activePlayer.getID(), 
										"You can't add more than one card to your display while you are Stunned.");
								return false;
							}
						}
					} else {
						server.reportErrorToUser(activePlayer.getID(), 
								playedCard.getID()+" Card doesn't match tournament colour");
						return false;
					}
				}
			}
		}
		return false;
	}

	public Card drawCard (Player p){
		/* Draws a card from the deck into the player's hand */
		
		Card card = gameState.getDeck().remove(0);
		p.getHand().add(card);
		
		if (gameState.getDeck().size() == 0){
			// when the deck runs out, shuffle the discard pile into the deck
			gameState.setDeck(gameState.getDiscardPile());
			gameState.shuffleDeck();
			gameState.setDiscardPile(new ArrayList<Card>());
			
			server.updateAllStates("GameState "+createGameStateMessage());
		}
		
		return card;
	}
	
	public Player awardToken(Player player){
		/* Awards the player a token of the tournament colour.
		 * If the tournament is purple, the player gets to choose the colour.
		 * 
		 * If the player wins the game as a result, return its Player object
		 * Otherwise, return null
		 */
		
		if (gameState.getTournamentColour().equals("purple")){
			String tc = server.promptUserForString(player.getID(), "Choose a token colour");
			//AI workaround. Give it purple, unless it already has purple in which case just return false.	 
			if (tc == null) {
				tc = "purple";
				if (player.getTokens().get(tc) == true) {
					return null;
				}
			}
			
			while (player.getTokens().get(tc) == true){
				tc = server.promptUserForString(player.getID(), "You already have that colour. Choose a different token colour");
			}
			player.setToken(tc, true);
			server.updateAllStates("Token "+player.getID()+" "+tc+" Won");
		} else {
			if (player.getTokens().get(gameState.getTournamentColour()) == false){
				player.setToken(gameState.getTournamentColour(), true);
				server.updateAllStates("Token "+player.getID()+" "+gameState.getTournamentColour()+" Won");
			}
		}
		
		if (player.getTokens().get("purple") &&
			player.getTokens().get("red")    &&
			player.getTokens().get("blue")   &&
			player.getTokens().get("yellow") &&
			player.getTokens().get("green")  ){
			// The player wins!
			return player;
		}
		return null;
	}
	
	private int playersLeft(){
		int playersLeft = 0;
		for (Player p : gameState.getPlayers()) {
			if (!(p.isWithdrawn())) playersLeft++;
		}
		
		return playersLeft;
	}
	
	private void cleanDisplay(Player p){
		// Discards all cards from the specified player's display
		ArrayList<Card> cards = p.getDisplay();
		
		for (Card c : cards){
			gameState.addCardToDiscardPile(c);
		}
		
		cards = p.getSpecialZone();
		
		for (Card c : cards){
			gameState.addCardToDiscardPile(c);
		}
		
		p.getDisplay().clear();
		p.getSpecialZone().clear();
		
	}

	public boolean isPlayerWinning(Player player){
		// Checks if a player has the highest value among all displays
		int playersValue = 0;
		int otherValue;
		
		for (int i=0; i<player.getDisplay().size(); i++){
			if (gameState.getTournamentColour().equals("green")){ 
				// cards only count as 1 in a green tournament
				playersValue++;
			} else {
				playersValue += player.getDisplay().get(i).getValue();
			}
		}
		
		ArrayList<Player> players = gameState.getPlayers();
		
		for (Player p : players){
			if (p == player) continue;
			otherValue = 0;
			for (Card c : p.getDisplay()){
				if (gameState.getTournamentColour().equals("green")){ 
					// cards only count as 1 in a green tournament
					otherValue++;
				} else {
					otherValue += c.getValue();
				}
			}
			if (otherValue >= playersValue) return false;
		}
		
		return true;
	}
	
	public boolean canPlayerStart(Player p, boolean disallowPurple){
		/* Returns true if the player is able to start a tournament
		 * If their hand is entirely action cards, they are unable to start
		 * otherwise, they are able to
		 */
		for (Card c : p.getHand()){
			if (c.getType().equals("simple")){
				if (c.getColour().equals("purple") && disallowPurple){
					// You cannot have two purple tournaments in a row.
					// Therefore a purple card doesn't count as a valid
					// starting play if the last tournament was purple
					return false;
				} else {
					return true;
				}
			}
		}
		return false;
	}
	
	public Player playerHasIvanhoe(){
		/* Returns the player object which has the Ivanhoe card in its hand.
		 * If no player has it or the player that has it is withdrawn, returns null
		 */
		for (Player p : gameState.getPlayers()){
			for (Card c : p.getHand()){
				if (c.getName().equals("ivanhoe")){
					if (p.isWithdrawn() == false){
						return p;
					} else {
						return null;
					}
				}
			}
		}
		return null;
	}
	
	public Card getIvanhoeCard(){
		/* Finds the Ivanhoe card out of a player's hand and returns it
		 * if it's not in someone's hand, return null
		 */
		for (Player p : gameState.getPlayers()){
			for (Card c : p.getHand()){
				if (c.getName().equals("ivanhoe")){
					return c;
				}
			}
		}
		return null;
	}
	
	public boolean handleIvanhoePlaying(Player activePlayer, Card playedCard, String prompt, GameState oldGameState){
		/* Handle the playing of Ivanhoe to counter
		 * Returns true if a players uses Ivanhoe to cancel
		 * Returns false if either no player has Ivanhoe or the player who has it chooses not use it
		 */
		
		Player ivanhoePlayer = playerHasIvanhoe();
		if (ivanhoePlayer != null && ivanhoePlayer != activePlayer){
			String response = server.promptUserForString(ivanhoePlayer.getID(), prompt);
			if (response != null && response.toLowerCase().equals("yes")){
				gameState = oldGameState;
				
				gameState.addCardToDiscardPile(playedCard);
				activePlayer.getHand().remove(playedCard);

				gameState.addCardToDiscardPile(getIvanhoeCard());
				ivanhoePlayer.getHand().remove(getIvanhoeCard());
				
				server.reportErrorToUser(activePlayer.getID(), ivanhoePlayer.getName()+" countered your card with Ivanhoe!");
				server.updateAllStates("GameState "+createGameStateMessage());
				
				return true;
			}
		}
		return false;
	}

	/////////////////////////////////////////////////
	// FROM HERE DOWN IS STRICTLY FOR TESTING
	/////////////////////////////////////////////////

	public void setDeck(ArrayList<Card> deck) { gameState.setDeck(deck); }
	
	public void buildTestDeck(){
		ArrayList<Card> testDeck = new ArrayList<Card>();
		int id = 0;
		
		String[] colours = {"red", "purple", "blue", "yellow", "green", "white", "white", "white", "white", "white", "white"};
		
		for (int i=0; i<colours.length; i++){
			testDeck.add(new Card(colours[i], 1, id++, ""));
			testDeck.add(new Card(colours[i], 2, id++, ""));
			testDeck.add(new Card(colours[i], 3, id++, ""));
			testDeck.add(new Card(colours[i], 4, id++, ""));
		}
		
		gameState.setDeck(testDeck);
	}
	

}