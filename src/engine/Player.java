package engine;

import java.util.*;

import cards.*;

public class Player {
	private String name;
	private int ID;
	
	private HashMap<String, Boolean> tokens; // keeps track of which tokens the player has
	private ArrayList<Card> hand; // the play's hand
	private ArrayList<Card> display; // the player's display
	private ArrayList<Card> specialZone; // this is where Shield and Stunned go
	private boolean withdrawn; // whether the player has withdrawn from the current tournament
	private boolean addedToDisplay;
	
	public static Player getCopy(Player oldPlayer) {
		Player newPlayer = new Player(oldPlayer.getName(), oldPlayer.getID());
		
		HashMap<String, Boolean> newTokens = newPlayer.getTokens();
		for (Map.Entry<String, Boolean> e : oldPlayer.getTokens().entrySet()) {
			newTokens.put(e.getKey(), e.getValue());
		}
		
		ArrayList<Card> newHand = newPlayer.getHand();
		for (Card c : oldPlayer.getHand()) {
			newHand.add(c); //Don't need to deep copy cards because they don't change
		}
		
		ArrayList<Card> newDisplay = newPlayer.getDisplay();
		for (Card c : oldPlayer.getDisplay()) {
			newDisplay.add(c); //Don't need to deep copy cards because they don't change
		}
		
		ArrayList<Card> newSpecial = newPlayer.getSpecialZone();
		for (Card c : oldPlayer.getSpecialZone()) {
			newSpecial.add(c); //Don't need to deep copy cards because they don't change
		}
		
		newPlayer.setHand(newHand);
		newPlayer.setDisplay(newDisplay);
		newPlayer.setSpecialZone(newSpecial);
		newPlayer.setWithdrawn(oldPlayer.isWithdrawn());
		newPlayer.setAddedToDisplay(oldPlayer.hasAddedToDisplay());
		
		return newPlayer;
	}
	
	
	public Player(String name, int ID){
		this.name = name;
		this.ID = ID;
		// set the player to have no tokens
		this.tokens = new HashMap<String, Boolean>();
		this.tokens.put("purple", false);
		this.tokens.put("red", false);
		this.tokens.put("blue", false);
		this.tokens.put("yellow", false);
		this.tokens.put("green", false);
		
		this.hand = new ArrayList<Card>();
		this.display = new ArrayList<Card>();
		this.specialZone = new ArrayList<Card>();
		
		this.withdrawn = false;
		this.addedToDisplay = false;
		
	}

	public boolean playCard(int id){
		// plays a card from hand to display
		Card c = getCardFromHand(id);
		
		if (c == null) return false;
		
		hand.remove(c);
		display.add(c);
		
		return true;
	}
	
	public Card getCardFromHand(int id){
		Card c = null;
		for (int i=0; i<hand.size(); i++){
			if (hand.get(i).getID() == id) c = hand.get(i);
		}
		return c;
	}
	
	public void resetState(){
		/* Cleans the player's hand and display 
		 * and gives up his/her tokens
		 * */
		
		this.hand.clear();
		this.display.clear();
		this.specialZone.clear();
		
		this.tokens.put("purple", false);
		this.tokens.put("red", false);
		this.tokens.put("blue", false);
		this.tokens.put("yellow", false);
		this.tokens.put("green", false);
		
		this.withdrawn = false;
	}
	
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	public HashMap<String, Boolean> getTokens() { return tokens; }
	public boolean setToken(String colour, boolean state) {
		/* Gives the player the specified colour of token 
		 * state is whether the player is winning or losing the token
		 * true: winning, false: losing
		 */
		if (Arrays.asList(Card.card_colours).contains(colour)){
			this.tokens.put(colour, state); 
			return true;
		}
		return false;
	}
	
	public boolean hasShield(){
		for (Card c : specialZone){
			if (c.getName().equals("shield")) return true;
		}
		return false;
	}
	public boolean hasStunned(){
		for (Card c : specialZone){
			if (c.getName().equals("stunned")) return true;
		}
		return false;
	}
	public boolean hasMaiden(){
		// Returns true if the player has a Maiden in his display
		for (Card c : display){
			if (c.getColour().equals("white") && c.getValue() == 6) return true;
		}
		return false;
	}
	
	public ArrayList<Card> getHand() { return hand; }
	public void setHand(ArrayList<Card> hand) { this.hand = hand; }
	
	public ArrayList<Card> getDisplay() { return display; }
	public void setDisplay(ArrayList<Card> display) { this.display = display; }
	
	public ArrayList<Card> getSpecialZone() { return specialZone; }
	public void setSpecialZone(ArrayList<Card> specialZone) { this.specialZone = specialZone; }

	public boolean isWithdrawn() { return withdrawn; }
	public void setWithdrawn(boolean withdrawn) { this.withdrawn = withdrawn; }
	
	public boolean hasAddedToDisplay() { return addedToDisplay; }
	public void setAddedToDisplay(boolean addedToDisplay) { this.addedToDisplay = addedToDisplay; }

	public int getID() { return ID; }

	
	
}
