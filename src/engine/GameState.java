package engine;

import java.util.ArrayList;
import java.util.Collections;

import cards.*;

public class GameState {
	private ArrayList<Player> players;
	private ArrayList<Card> deck; // the deck of cards
	private ArrayList<Card> discardPile; // the discard pile
	boolean gameRunning;
	
	private int turn; // saves the index of the player whose turn it is
	private String tournamentColour;
	
	////////////////
	//Singleton Code
	////////////////
	private static GameState instance = new GameState();
	
	public static GameState getInstance() {
		return instance;
	}
	
	public static GameState getCopy() {
		GameState oldGS = GameState.getInstance();
		GameState newGS = new GameState();
		
		for (Player oldPlayer : oldGS.getPlayers()) {
			Player newPlayer = Player.getCopy(oldPlayer);
			newGS.addPlayer(newPlayer);
		}
		
		newGS.getDeck().clear();
		ArrayList<Card> newDeck = newGS.getDeck();
		for (Card oldCard : oldGS.getDeck()) {
			newDeck.add(oldCard); //Don't need to deep copy cards because they don't change
		}
		
		ArrayList<Card> newDiscard = newGS.getDiscardPile();
		for (Card oldCard : oldGS.getDiscardPile()) {
			newDiscard.add(oldCard); //Don't need to deep copy cards because they don't change
		}
		
		newGS.setDeck(newDeck);
		newGS.setDiscardPile(newDiscard);
		newGS.setTurn(oldGS.getTurn());
		newGS.setTournamentColour(oldGS.getTournamentColour());
		
		return newGS;
	}

	//////////////
	//Constructors
	//////////////
	private GameState() {
		this.players = new ArrayList<Player>();
		this.deck = new ArrayList<Card>();
		this.discardPile = new ArrayList<Card>();
		this.gameRunning = false;
		
		this.tournamentColour = "none";
		
		buildDeck();
		
	}
	
	
	//////////
	//Main fns
	//////////
	public void resetGameState(boolean removePlayers) {
		if (removePlayers) {
			this.players.clear();
		}
		else {
			for (Player p : players) {
				p.resetState();
			}
		}
		
		this.deck.clear();
		this.discardPile.clear();
		this.tournamentColour = "none";
		
		buildDeck();
	}
	
	public void buildDeck() {
		
		CardFactory factory = new CardFactory();
		// TODO: Remember the UI needs to account for new ID's for cards
		
		// All cards that appear once in the deck
		deck.add(factory.makeCard("dodge"));
		deck.add(factory.makeCard("disgrace"));
		deck.add(factory.makeCard("retreat"));
		deck.add(factory.makeCard("outmaneuver"));
		deck.add(factory.makeCard("counterCharge"));
		deck.add(factory.makeCard("charge"));
		deck.add(factory.makeCard("breakLance"));
		deck.add(factory.makeCard("adapt"));
		deck.add(factory.makeCard("dropWeapon"));
		deck.add(factory.makeCard("changeWeapon"));
		deck.add(factory.makeCard("unhorse"));
		deck.add(factory.makeCard("outwit"));
		deck.add(factory.makeCard("shield"));
		deck.add(factory.makeCard("stunned"));
		deck.add(factory.makeCard("ivanhoe"));
		
		// All cards that appear twice in the deck
		for (int i=0; i<2; i++){
			deck.add(factory.makeCard("red 5"));
			deck.add(factory.makeCard("blue 5"));
			deck.add(factory.makeCard("yellow 4"));
			deck.add(factory.makeCard("purple 7"));
			deck.add(factory.makeCard("knockDown"));
		}
		
		// All cards that appear three times in the deck
		for (int i=0; i<3; i++){
			deck.add(factory.makeCard("riposte"));
		}
		
		// All cards that appear four times in the deck
		for (int i=0; i<4; i++){
			deck.add(factory.makeCard("purple 3"));
			deck.add(factory.makeCard("purple 4"));
			deck.add(factory.makeCard("purple 5"));
			deck.add(factory.makeCard("blue 2"));
			deck.add(factory.makeCard("blue 3"));
			deck.add(factory.makeCard("blue 4"));
			deck.add(factory.makeCard("yellow 2"));
			deck.add(factory.makeCard("white 6"));
		}
		
		// All cards that appear six times in the deck
		for (int i=0; i<6; i++){
			deck.add(factory.makeCard("red 3"));
			deck.add(factory.makeCard("red 4"));
		}
		
		// All cards that appear eight times in the deck
		for (int i=0; i<8; i++){
			deck.add(factory.makeCard("white 2"));
			deck.add(factory.makeCard("white 3"));
			deck.add(factory.makeCard("yellow 3"));
		}
		
		// All cards that appear 14 times in the deck
		for (int i=0; i<14; i++){
			deck.add(factory.makeCard("green 1"));
		}
	}
	
	public void shuffleDeck() {
		Collections.shuffle(deck);
	}
	
	public int getShieldID(){
		// Returns the ID of the Shield card
		for (Card c : deck){
			if (c.getName().equals("shield")) return c.getID();
		}
		return -1;
	}
	
	public int getStunnedID(){
		// Returns the ID of the Shield card
		for (Card c : deck){
			if (c.getName().equals("stunned")) return c.getID();
		}
		return -1;
	}
	
	///////////////////////////////////////////
	//Getters, Setters, Other state-related fns
	///////////////////////////////////////////
	public ArrayList<Player> getPlayers() {
		return players;
	}
	
	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}
	
	public void addPlayer(Player p) {
		players.add(p);
	}
	
	public Player removePlayer(int ID) {
		for (Player p : players) {
			if (p.getID() == ID) {
				players.remove(p);
				return p;
			}
		}
		
		return null;
	}
	
	public Player getPlayerByName(String name){
		for (Player p : players) {
			if (p.getName().equals(name)) {
				return p;
			}
		}
		return null;
	}
	
	public Player getPlayerByID(int ID){
		for (int i=0; i<players.size(); i++){
			if (players.get(i).getID() == ID) return players.get(i);
		}
		return null;
	}
	
	public ArrayList<Card> getDeck() {
		return deck;
	}
	
	public void setDeck(ArrayList<Card> deck) {
		this.deck = deck;
	}
	
	public void addCardToDeck(Card card) {
		this.deck.add(card);
	}

	
	
	
	public ArrayList<Card> getDiscardPile() {
		return discardPile;
	}
	
	public void setDiscardPile(ArrayList<Card> discardPile) {
		this.discardPile = discardPile;
	}
	
	public void addCardToDiscardPile(Card card) {
		this.discardPile.add(card);
	}
	
	public int getTurn() {
		return turn;
	}
	
	public void setTurn(int turn) {
		this.turn = turn;
	}
	
	public String getTournamentColour() {
		return tournamentColour;
	}
	
	public void setTournamentColour(String tournamentColour) {
		this.tournamentColour = tournamentColour;
	}

	public boolean isGameRunning() {
		return gameRunning;
	}

	public void setGameRunning(boolean gameRunning) {
		this.gameRunning = gameRunning;
	}
	
	
	
}
